// Distributed under GNU General Public License (see license.txt for details).
//
//  Copyright (c) 2007 Shai Shalev-Shwartz.
//  All Rights Reserved.
//=============================================================================
// File Name: pegasos_optimize.cc
// implements the main optimization function of pegasos
//=============================================================================

#include "pegasos_optimize.h"

// help function for getting runtime
long get_runtime(void)
{
  clock_t start;
  start = clock();
  return((long)((double)start/(double)CLOCKS_PER_SEC));
}


// ------------------------------------------------------------//
// ---------------- OPTIMIZING --------------------------------//
// ------------------------------------------------------------//
void Learn(// Input variables
	   std::vector<simple_sparse_vector>& Dataset,
	   std::vector<int>& Labels,
	   uint dimension,
	   std::vector<simple_sparse_vector>& testDataset,
	   std::vector<int>& testLabels,
	   double lambda,int max_iter,int exam_per_iter,int num_iter_to_avg,
	   std::string& model_filename,
	   // Output variables
	   long& train_time,long& calc_obj_time,double& obj_value,
	   double& norm_value,double& loss_value,double& zero_one_error,
	   double& test_loss,double& test_error,
	   // additional parameters
	   int eta_rule_type , double eta_constant ,
	   int projection_rule, double projection_constant) {

  uint num_examples = Labels.size();

  long startTime = get_runtime();
  long endTime;
  

  // Initialization of classification vector
  WeightVector W(dimension);
  WeightVector AvgW(dimension);
  double avgScale = (num_iter_to_avg > max_iter) ? max_iter : num_iter_to_avg; 

  // ---------------- Main Loop -------------------
  for (int i = 0; i < max_iter; ++i) {

    // learning rate
    double eta;
    if (eta_rule_type == 0) { // Pegasos eta rule
      eta = 1 / (lambda * (i+2)); 
    } else if (eta_rule_type == 1) { // Norma rule
      eta = eta_constant / sqrt(i+2);
      // solve numerical problems
      W.make_my_a_one();
    } else {
      eta = eta_constant;
    }

    // gradient indices and losses
    std::vector<uint> grad_index;
    std::vector<double> grad_weights;

    // calc sub-gradients
    for (int j=0; j < exam_per_iter; ++j) {

      // choose random example
      uint r = ((int)rand()) % num_examples;

      // calculate prediction
      double prediction = W*Dataset[r];

      // calculate loss
      double cur_loss = 1 - Labels[r]*prediction;
      if (cur_loss < 0.0) cur_loss = 0.0;

      // and add to the gradient
      if (cur_loss > 0.0) {
	grad_index.push_back(r);
	grad_weights.push_back(eta*Labels[r]/exam_per_iter);
      }
    }
 
    // scale w 
    W.scale(1.0 - eta*lambda);

    // and add sub-gradients
    for (uint j=0; j<grad_index.size(); ++j) {
      W.add(Dataset[grad_index[j]],grad_weights[j]);
    }

    // Project if needed
    if (projection_rule == 0) { // Pegasos projection rule
      double norm2 = W.snorm();
      if (norm2 > 1.0/lambda) {
	W.scale(sqrt(1.0/(lambda*norm2)));
      }
    } else if (projection_rule == 1) { // other projection
      double norm2 = W.snorm();
      if (norm2 > (projection_constant*projection_constant)) {
	W.scale(projection_constant/sqrt(norm2));
      }
    } // else -- no projection


    // and update AvgW
    if (max_iter <= num_iter_to_avg + i)
      AvgW.add(W, 1.0/avgScale);
  }


  // update timeline
  endTime = get_runtime();
  train_time = endTime - startTime;
  startTime = get_runtime();

  // Calculate objective value
  norm_value = AvgW.snorm();
  obj_value = norm_value * lambda / 2.0;
  loss_value = 0.0;
  zero_one_error = 0.0;
  for (uint i=0; i < Dataset.size(); ++i) {
    double cur_loss = 1 - Labels[i]*(AvgW * Dataset[i]); 
    if (cur_loss < 0.0) cur_loss = 0.0;
    loss_value += cur_loss/num_examples;
    obj_value += cur_loss/num_examples;
    if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
  }

  endTime = get_runtime();
  calc_obj_time = endTime - startTime;

  // Calculate test_loss and test_error
  test_loss = 0.0;
  test_error = 0.0;
  for (uint i=0; i < testDataset.size(); ++i) {
    double cur_loss = 1 - testLabels[i]*(AvgW * testDataset[i]); 
    if (cur_loss < 0.0) cur_loss = 0.0;
    test_loss += cur_loss;
    if (cur_loss >= 1.0) test_error += 1.0;
  }
  if (testDataset.size() != 0) {
    test_loss /= testDataset.size();
    test_error /= testDataset.size();
  }
  


  // finally, print the model to the model_file
  if (model_filename != "noModelFile") {
    std::ofstream model_file(model_filename.c_str());
    if (!model_file.good()) {
      std::cerr << "error w/ " << model_filename << std::endl;
      exit(EXIT_FAILURE);
    }
    AvgW.print(model_file);
    model_file.close();
  }

}




void LearnAndValidate(// Input variables
		      std::vector<simple_sparse_vector>& Dataset,
		      std::vector<int>& Labels,
		      uint dimension,
		      std::vector<simple_sparse_vector>& testDataset,
		      std::vector<int>& testLabels,
		      double lambda,int max_iter,
		      int exam_per_iter,int num_example_to_validate,
		      std::string& model_filename,
		      // Output variables
		      long& train_time,long& calc_obj_time,
		      double& obj_value,double& norm_value,
		      double& loss_value,double& zero_one_error,
		      double& test_loss,double& test_error,
		      // additional parameters
		      int eta_rule_type , double eta_constant ,
		      int projection_rule, double projection_constant) {

  uint num_examples = Labels.size();

  long startTime = get_runtime();
  long endTime;
  

  // Initialization of classification vector
  WeightVector W(dimension);
  WeightVector BestW(dimension);
  double best_obj = 1.0; // the zero solution

  // create validation indices
  std::vector<uint> validate_indices(num_example_to_validate);
  for (uint i=0; i < validate_indices.size(); ++i)
    validate_indices[i] = ((int)rand()) % num_examples;

  // Choose s random indices 
  int s = 5; // corresponds to confidence of 0.9933
  int block_size = max_iter/s;
  std::vector<int> candidates(s);
  for (int i=0; i<s; ++i) {
    candidates[i] = block_size*i + ((int)rand()) % block_size;
  }
  candidates[s-1] = max_iter-1; // make sure we need all iterations
  int cur_block = 0;

  
  

  // ---------------- Main Loop -------------------
  for (int i = 0; i < max_iter; ++i) {

    // learning rate
    double eta;
    if (eta_rule_type == 0) { // Pegasos eta rule
      eta = 1 / (lambda * (i+2)); 
    } else if (eta_rule_type == 1) { // Norma rule
      eta = eta_constant / sqrt(i+2);
      // solve numerical problems
      if (projection_rule != 2)
	W.make_my_a_one();
    } else {
      eta = eta_constant;
    }

    // gradient indices and losses
    std::vector<uint> grad_index;
    std::vector<double> grad_weights;

    // calc sub-gradients
    for (int j=0; j < exam_per_iter; ++j) {

      // choose random example
      uint r = ((int)rand()) % num_examples;

      // calculate prediction
      double prediction = W*Dataset[r];

      // calculate loss
      double cur_loss = 1 - Labels[r]*prediction;
      if (cur_loss < 0.0) cur_loss = 0.0;

      // and add to the gradient
      if (cur_loss > 0.0) {
	grad_index.push_back(r);
	grad_weights.push_back(eta*Labels[r]/exam_per_iter);
      }
    }
 
    // scale w 
    W.scale(1.0 - eta*lambda);

    // and add sub-gradients
    for (uint j=0; j<grad_index.size(); ++j) {
      W.add(Dataset[grad_index[j]],grad_weights[j]);
    }

    // Project if needed
    if (projection_rule == 0) { // Pegasos projection rule
      double norm2 = W.snorm();
      if (norm2 > 1.0/lambda) {
	W.scale(sqrt(1.0/(lambda*norm2)));
      }
    } else if (projection_rule == 1) { // other projection
      double norm2 = W.snorm();
      if (norm2 > (projection_constant*projection_constant)) {
	W.scale(projection_constant/sqrt(norm2));
      }
    } // else -- no projection


    // and validate
    if (i == candidates[cur_block]) {
      double obj = 0.0;
      for (uint j=0; j < validate_indices.size(); ++j) {
	uint ind = validate_indices[j];
	double cur_loss = 1 - Labels[ind]*(W * Dataset[ind]); 
	if (cur_loss < 0.0) cur_loss = 0.0;
	obj += cur_loss;
      }
      obj /= validate_indices.size();
      obj += lambda/2.0*W.snorm();
      if (obj <= best_obj) {
	//	std::cerr << "obj of " << i << " (candidates[" 
	//		  << cur_block << "]) = " << obj << std::endl;
	BestW = W;
	best_obj = obj;
      }
      cur_block++;
    }
  }


  // update timeline
  endTime = get_runtime();
  train_time = endTime - startTime;
  startTime = get_runtime();

  // Calculate objective value
  norm_value = BestW.snorm();
  obj_value = norm_value * lambda / 2.0;
  loss_value = 0.0;
  zero_one_error = 0.0;
  for (uint i=0; i < Dataset.size(); ++i) {
    double cur_loss = 1 - Labels[i]*(BestW * Dataset[i]); 
    if (cur_loss < 0.0) cur_loss = 0.0;
    loss_value += cur_loss/num_examples;
    obj_value += cur_loss/num_examples;
    if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
  }

  endTime = get_runtime();
  calc_obj_time = endTime - startTime;

  // Calculate test_loss and test_error
  test_loss = 0.0;
  test_error = 0.0;
  for (uint i=0; i < testDataset.size(); ++i) {
    double cur_loss = 1 - testLabels[i]*(BestW * testDataset[i]); 
    if (cur_loss < 0.0) cur_loss = 0.0;
    test_loss += cur_loss;
    if (cur_loss >= 1.0) test_error += 1.0;
  }
  if (testDataset.size() != 0) {
    test_loss /= testDataset.size();
    test_error /= testDataset.size();
  }
  


  // finally, print the model to the model_file
  if (model_filename != "noModelFile") {
    std::ofstream model_file(model_filename.c_str());
    if (!model_file.good()) {
      std::cerr << "error w/ " << model_filename << std::endl;
      exit(EXIT_FAILURE);
    }
    BestW.print(model_file);
    model_file.close();
  }

}





void LearnReturnLast(// Input variables
		      std::vector<simple_sparse_vector>& Dataset,
		      std::vector<int>& Labels,
		      uint dimension,
		      std::vector<simple_sparse_vector>& testDataset,
		      std::vector<int>& testLabels,
		      double lambda,int max_iter,
		      int exam_per_iter,
		      std::string& model_filename,
		      // Output variables
		      long& train_time,long& calc_obj_time,
		      double& obj_value,double& norm_value,
		      double& loss_value,double& zero_one_error,
		      double& test_loss,double& test_error,
		      // additional parameters
		      int eta_rule_type , double eta_constant ,
		      int projection_rule, double projection_constant) {

  uint num_examples = Labels.size();

  long startTime = get_runtime();
  long endTime;
  

  // Initialization of classification vector
  WeightVector W(dimension);

  // ---------------- Main Loop -------------------
  for (int i = 0; i < max_iter; ++i) {

    // learning rate
    double eta;
    if (eta_rule_type == 0) { // Pegasos eta rule
      eta = 1 / (lambda * (i+2)); 
    } else if (eta_rule_type == 1) { // Norma rule
      eta = eta_constant / sqrt(i+2);
      // solve numerical problems
      //if (projection_rule != 2)
	W.make_my_a_one();
    } else {
      eta = eta_constant;
    }

    // gradient indices and losses
    std::vector<uint> grad_index;
    std::vector<double> grad_weights;

    // calc sub-gradients
    for (int j=0; j < exam_per_iter; ++j) {

      // choose random example
      uint r = ((int)rand()) % num_examples;

      // calculate prediction
      double prediction = W*Dataset[r];

      // calculate loss
      double cur_loss = 1 - Labels[r]*prediction;
      if (cur_loss < 0.0) cur_loss = 0.0;

      // and add to the gradient
      if (cur_loss > 0.0) {
	grad_index.push_back(r);
	grad_weights.push_back(eta*Labels[r]/exam_per_iter);
      }
    }
 
    // scale w 
    W.scale(1.0 - eta*lambda);

    // and add sub-gradients
    for (uint j=0; j<grad_index.size(); ++j) {
      W.add(Dataset[grad_index[j]],grad_weights[j]);
    }

    // Project if needed
    if (projection_rule == 0) { // Pegasos projection rule
      double norm2 = W.snorm();
      if (norm2 > 1.0/lambda) {
	W.scale(sqrt(1.0/(lambda*norm2)));
      }
    } else if (projection_rule == 1) { // other projection
      double norm2 = W.snorm();
      if (norm2 > (projection_constant*projection_constant)) {
	W.scale(projection_constant/sqrt(norm2));
      }
    } // else -- no projection

  }


  // update timeline
  endTime = get_runtime();
  train_time = endTime - startTime;
  startTime = get_runtime();

  // Calculate objective value
  norm_value = W.snorm();
  obj_value = norm_value * lambda / 2.0;
  loss_value = 0.0;
  zero_one_error = 0.0;
  for (uint i=0; i < Dataset.size(); ++i) {
    double cur_loss = 1 - Labels[i]*(W * Dataset[i]); 
    if (cur_loss < 0.0) cur_loss = 0.0;
    loss_value += cur_loss/num_examples;
    obj_value += cur_loss/num_examples;
    if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
  }

  endTime = get_runtime();
  calc_obj_time = endTime - startTime;

  // Calculate test_loss and test_error
  test_loss = 0.0;
  test_error = 0.0;
  for (uint i=0; i < testDataset.size(); ++i) {
    double cur_loss = 1 - testLabels[i]*(W * testDataset[i]); 
    if (cur_loss < 0.0) cur_loss = 0.0;
    test_loss += cur_loss;
    if (cur_loss >= 1.0) test_error += 1.0;
  }
  if (testDataset.size() != 0) {
    test_loss /= testDataset.size();
    test_error /= testDataset.size();
  }
  


  // finally, print the model to the model_file
  if (model_filename != "noModelFile") {
    std::ofstream model_file(model_filename.c_str());
    if (!model_file.good()) {
      std::cerr << "error w/ " << model_filename << std::endl;
      exit(EXIT_FAILURE);
    }
    W.print(model_file);
    model_file.close();
  }

}
void LearnBCRH2(// Input variables
	      std::vector<simple_sparse_vector>& Datasetx,
	      std::vector<int>& Labelsx,
	      std::vector<simple_sparse_vector>& Datasety,
	      std::vector<int>& Labelsy,
	      std::vector<uint>& pairs_idx_x,
		  std::vector<uint>& pairs_idx_y,
		  std::vector<double>& pairs_label,
	      uint dimensionx,
	      uint dimensiony,
	      uint code_length,
	      double lambda_x,double lambda_y, double gamma_cross,
	      int max_iter_x, int max_iter_y,
	      int exam_per_iter, int exam_per_iter_pair,
	      std::string& model_filename,
	      // Output variables
	      long& train_time,long& calc_obj_time,
	      double& obj_value,double& norm_value,
//		      double& loss_value,double& zero_one_error,
//		      double& test_loss,double& test_error,
	      // additional parameters
	      double scisd_a, double scisd_lambda,
	      int eta_rule_type , double eta_constant,
	      int projection_rule, double projection_constant) {
	///*
			  uint num_examples_x = Labelsx.size();
			  uint num_examples_y = Labelsy.size();
			  uint num_pairs_xy = pairs_idx_x.size();
			  // std::cerr << scisd_a <<"\t"<<scisd_lambda<< std::endl;
			  if (num_pairs_xy != pairs_idx_y.size()) {
				  	  std::cerr << "checking y" << std::endl;
					  std::cerr << "error w/ point pairs!"<< std::endl;
					  std::cerr << pairs_idx_y.size() << std::endl;
					  std::cerr << num_pairs_xy << std::endl;
					  exit(EXIT_FAILURE);
					}
			  if (num_pairs_xy != pairs_label.size()) {
				  std::cerr << "checking label" << std::endl;
						std::cerr << "error w/ point pairs!"<< std::endl;
						std::cerr << pairs_label.size() << std::endl;
						std::cerr << num_pairs_xy << std::endl;
						exit(EXIT_FAILURE);
					  }

			  long startTime = get_runtime();
			  long endTime;

			  bool converge_wxwy, converge_wx, converge_wy;


//			  // Initialization of classification vector
//			  WeightVector Wx(dimensionx);
//			  WeightVector Wx_curr(dimensionx);
//			  WeightVector Wy(dimensiony);
//			  WeightVector Wy_curr(dimensiony);

			  // Initialization of weightings for cross-modal pairs for boosting
			  double* pweights = new double [num_pairs_xy];
			  for (uint i = 0; i<num_pairs_xy; ++i) {
				  pweights[i] = 1.0/(double) num_pairs_xy;
			  }


			  bool do_init_cvh = false;
			  std::vector<simple_sparse_vector> WeightMatx;
			  std::vector<simple_sparse_vector> WeightMaty;
			  if (do_init_cvh) {
				  // std::cerr << "I am here!"<< std::endl;
				  char model_file[100];
				  sprintf(model_file, "../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT10_TR%d_%dbModel_x.dat", num_examples_x, code_length);
//				  std::cerr << model_file<< std::endl;
				  std::string model_file_x(model_file);
//				  std::cerr << model_file_x<< std::endl;
				  sprintf(model_file, "../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT10_TR%d_%dbModel_y.dat", num_examples_x, code_length);
//				  std::cerr << model_file<< std::endl;
				  std::string model_file_y(model_file);
				  long readingTime;
				  std::vector<int> dummylabel;
				  uint dummydim;
				  ReadData(model_file_x, WeightMatx,dummylabel,dummydim,readingTime);
				  ReadData(model_file_y, WeightMaty,dummylabel,dummydim,readingTime);
			  }
			  // std::cerr << "I am here!"<< std::endl;
			  // exit(EXIT_FAILURE);
			  for (uint k = 0; k < code_length; ++k) {
				  // for each bit
				  // Initialization of classification vector

				  WeightVector Wx(dimensionx);
				  WeightVector Wy(dimensiony);
				  if (do_init_cvh) {
					  Wx.add(WeightMatx[k],1.0);
					  Wy.add(WeightMaty[k],1.0);
				  }
				  WeightVector Wx_curr(dimensionx);
				  WeightVector Wx_tmp(dimensionx);
				  WeightVector Wy_curr(dimensiony);
				  WeightVector Wy_tmp(dimensiony);

				  startTime = get_runtime();

				  // while for updating wx and wy
				  double obj_value_old = 10000000; // total objective
				  converge_wxwy = false;
				  int iters_xy = 0;
				  ///*
				  while (converge_wxwy == false) {// update wx and wy iteratively until convergence
					  std::cerr << "Objective for total:"<< "\t" << obj_value_old << std::endl;

					  // ----------------- start of updating wx ---------------------------------
					  // for updating wx
					  double obj_wx = 0.0;
					  double obj_wx_old = 1000000;
					  converge_wx = false;
					  int iters_x = 0;
					  while (converge_wx == false) {
						  //std::cerr << "Objective for x:"<< "\t" << obj_wx_old << std::endl;
						  // CCCP iterations
						  Wx_curr = Wx;// for current CCCP iteration
						  // ---------------- Inner Pegasos Loop -------------------
							for (int i = 0; i < max_iter_x; ++i) {

							  // learning rate
							  double eta;
							  if (eta_rule_type == 0) { // Pegasos eta rule
								eta = 1 / (lambda_x * (i+2));
							  } else if (eta_rule_type == 1) { // Norma rule
								eta = eta_constant / sqrt(i+2);
								// solve numerical problems
								//if (projection_rule != 2)
								Wx.make_my_a_one();
							  } else {
								eta = eta_constant;
							  }
							  //std::cerr << eta << "\t" << "learning rate!"<< std::endl;

							  // gradient indices and losses
							  std::vector<uint> grad_index;
							  std::vector<double> grad_weights;
							  // gradient indices and losses for cross
							  std::vector<uint> cross_grad_index;
							  std::vector<double> cross_grad_weights;

							  // calc sub-gradients for (at most) exam_per_iter points
							  for (int j=0; j < exam_per_iter; ++j) {

								// choose random example
								uint r = ((int)rand()) % num_examples_x;

								// calculate prediction
								double prediction = Wx*Datasetx[r];
								double Labelr = (prediction >=0)?+1.0:-1.0;

								// calculate loss
								double cur_loss = 1 - Labelr*prediction;
								if (cur_loss < 0.0) cur_loss = 0.0;

								// and add to the gradient
								if (cur_loss > 0.0) {
									grad_index.push_back(r);
									grad_weights.push_back(eta*Labelr/exam_per_iter);
								}
							  }

							  // calc sub-gradients2 for cross-modal similarity for all pairs
							  if (exam_per_iter_pair > (int)num_pairs_xy) {
								  exam_per_iter_pair = (int)num_pairs_xy;
							  }
							  for (int j=0; j < exam_per_iter_pair; ++j) {

								// choose random example
								uint r = ((int)rand()) % num_pairs_xy;

								// calculate prediction
								double d_curr = Wx_curr*Datasetx[pairs_idx_x[r]] - Wy*Datasety[pairs_idx_y[r]];
								double d_i = Wx*Datasetx[pairs_idx_x[r]] - Wy*Datasety[pairs_idx_y[r]];
								double gtau_one_i = 0;
								if (fabs(d_i) > scisd_a*scisd_lambda) {
									gtau_one_i = d_i;
								}
								else if (fabs(d_i) > scisd_lambda) {
									if (d_i >= 0) {
										gtau_one_i = (scisd_a*d_i - scisd_a*scisd_lambda)/(scisd_a-1);
									}
									else {
										gtau_one_i = (scisd_a*d_i + scisd_a*scisd_lambda)/(scisd_a-1);
									}
								}

								cross_grad_index.push_back(r);
								cross_grad_weights.push_back(-eta*pweights[r]*gamma_cross*(2*pairs_label[r]*d_i + (1-pairs_label[r])*(gtau_one_i-d_curr)));///num_pairs_xy
							  }

							  // scale w
							  Wx.scale(1.0 - eta*lambda_x);

							  // and add sub-gradients
							  for (uint j=0; j<grad_index.size(); ++j) {
								Wx.add(Datasetx[grad_index[j]], grad_weights[j]);
							  }

							  // and add sub-gradients 2 for cross-modal similarity
							  for (uint j=0; j<cross_grad_index.size(); ++j) {
								Wx.add(Datasetx[pairs_idx_x[cross_grad_index[j]]], cross_grad_weights[j]);
							  }

							  // Project if needed
							  if (projection_rule == 0) { // Pegasos projection rule
								double norm2 = Wx.snorm();
								if (norm2 > 1.0/lambda_x) {
									Wx.scale(sqrt(1.0/(lambda_x*norm2)));
								}
							  } else if (projection_rule == 1) { // other projection
								double norm2 = Wx.snorm();
								if (norm2 > (projection_constant*projection_constant)) {
									Wx.scale(projection_constant/sqrt(norm2));
								}
							  } // else -- no projection
							}// ---------------- End of Inner Pegasos Loop -------------------

							  // Calculate objective value for x
//							  startTime = get_runtime();
							  norm_value = Wx.snorm();
							  obj_wx = norm_value * lambda_x / 2.0;
							  // std::cerr << "Objective for x:"<< "\t" << obj_wx << std::endl;
							  for (uint i=0; i < Datasetx.size(); ++i) {
								double cur_loss = 1 - fabs(Wx * Datasetx[i]);
								if (cur_loss < 0.0) cur_loss = 0.0;
								obj_wx += cur_loss/num_examples_x;
							  }
//							  std::cerr << "Objective for x:"<< "\t" << obj_wx << std::endl;
							  for (uint i=0; i < num_pairs_xy; ++i) {
								  double d_i = Wx*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
								  double tau_i = 0;
								  if (fabs(d_i) <= scisd_lambda) {
									  tau_i = -0.5*d_i*d_i + 0.5*scisd_a*scisd_lambda*scisd_lambda;
								  }
								  else if (fabs(d_i) <= scisd_a*scisd_lambda) {
									  tau_i = (d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
								  }
								  obj_wx += pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i+(1-pairs_label[i])*tau_i);///num_pairs_xy
								  // std::cerr << pweights[i] <<"\t"<<gamma_cross <<"\t"<<pairs_label[i] <<"\t"<<tau_i<<"\t"<<d_i<<"\t"<<scisd_a*scisd_lambda<<"\t"<<pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i+(1-pairs_label[i])*tau_i)<<"\t"<<"Objective for x:"<< "\t" << obj_wx << std::endl;
							  }
							  // std::cerr << "Objective for x:"<< "\t" << obj_wx << std::endl;
//							  endTime = get_runtime();
//							  calc_obj_time = endTime - startTime;

							  // calc convex upper bound for wx
//							  norm_value = Wx.snorm();
//							  double obj_wx_upper = norm_value * lambda_x / 2.0;
////							  std::cerr << "Upper for x:"<< "\t" << obj_wx_upper << std::endl;
//							  //loss_wx = 0.0;
//							  //zero_one_error = 0.0;
//							  for (uint i=0; i < Datasetx.size(); ++i) {
//								double cur_loss = 1 - fabs(Wx * Datasetx[i]);
//								if (cur_loss < 0.0) cur_loss = 0.0;
//								//loss_value += cur_loss/num_examples;
//								obj_wx_upper += cur_loss/num_examples_x;
//								//if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
//							  }
////							  std::cerr << "Upper for x:"<< "\t" << obj_wx_upper << std::endl;
//							  Wx_tmp = Wx;
//							  Wx_tmp.add(Wx_curr,-1.0);
//							  for (uint i=0; i < num_pairs_xy; ++i) {
//								  double d_i = Wx*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
//								  double d_i_curr = Wx_curr*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
//								  double tau_i_1 = 0;
//								  if (fabs(d_i) > scisd_a*scisd_lambda) {
//									  tau_i_1 = 0.5*d_i*d_i - 0.5*scisd_a*scisd_lambda*scisd_lambda;
//								  }
//								  else if (fabs(d_i) > scisd_lambda) {
//									  tau_i_1 = (scisd_a*d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
//								  }
//								  obj_wx_upper += pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i + (1-pairs_label[i])*(tau_i_1 - (0.5*d_i_curr*d_i_curr-0.5*scisd_a*scisd_lambda*scisd_lambda) - d_i_curr*(Wx_tmp*Datasetx[pairs_idx_x[i]])));///num_pairs_xy
//								  // std::cerr << pweights[i] <<"\t"<<gamma_cross <<"\t"<<pairs_label[i] <<"\t"<<d_i<<"\t"<<"Upper for x:"<< "\t" << obj_wx_upper << std::endl;
//							  }
////							  for (int i = 0; i < num_pairs_xy; ++i) {
////								  std::cerr << "pweights["<< i << "]="<< pweights[i] << std::endl;
////							  }
//							  std::cerr << obj_wx << "\t"<< obj_wx_upper << ";"<<std::endl;
////							  std::cerr << "X: Objective: "<< obj_wx << "\t UpperBound: "<< obj_wx_upper << std::endl;

							  // check convergence
							  iters_x++;
							  if ((fabs(obj_wx - obj_wx_old) < 0.0001)||(iters_x>20)) {
								  converge_wx = true;
							  } else {
								  obj_wx_old = obj_wx;
							  }

					  }// end of while for updating wx
					  //std::cerr << "End of updating Wx!"<< std::endl;//exit(EXIT_FAILURE);
					  // ----------------- end of updating wx ---------------------------------

					  // ----------------- start of updating wy ---------------------------------
					  // for updating wy
					  double obj_wy = 0.0;
					  double obj_wy_old = 1000000;
					  converge_wy = false;
					  int iters_y = 0;
					  while (converge_wy == false) {
						  //std::cerr << "Objective for y:"<< "\t" << obj_wy_old << std::endl;
						  // CCCP iterations
						  Wy_curr = Wy;// for current CCCP iteration
						  // ---------------- Inner Pegasos Loop -------------------
						  for (int i = 0; i < max_iter_y; ++i) {
							  // learning rate
							  double eta;
							  if (eta_rule_type == 0) { // Pegasos eta rule
								eta = 1 / (lambda_y * (i+2));
							  } else if (eta_rule_type == 1) { // Norma rule
								eta = eta_constant / sqrt(i+2);
								// solve numerical problems
								//if (projection_rule != 2)
								Wy.make_my_a_one();
							  } else {
								eta = eta_constant;
							  }
							  //std::cerr << eta << "\t" << "learning rate!"<< std::endl;

							  // gradient indices and losses
							  std::vector<uint> grad_index;
							  std::vector<double> grad_weights;
							  // gradient indices and losses for cross
							  std::vector<uint> cross_grad_index;
							  std::vector<double> cross_grad_weights;

							  // calc sub-gradients
							  for (int j=0; j < exam_per_iter; ++j) {

								// choose random example
								uint r = ((int)rand()) % num_examples_y;

								// calculate prediction
								double prediction = Wy*Datasety[r];
								double Labelr = (prediction >=0)?+1.0:-1.0;

								// calculate loss
								double cur_loss = 1 - Labelr*prediction;
								if (cur_loss < 0.0) cur_loss = 0.0;

								// and add to the gradient
								if (cur_loss > 0.0) {
									grad_index.push_back(r);
									grad_weights.push_back(eta*Labelr/exam_per_iter);//-
								}
							  }

							  // calc sub-gradients2 for cross-modal similarity
							  for (int j=0; j < exam_per_iter_pair; ++j) {//num_pairs_xy

								  // choose random example
								uint r = ((int)rand()) % num_pairs_xy;

								// calculate prediction
								double d_curr = Wx*Datasetx[pairs_idx_x[r]] - Wy_curr*Datasety[pairs_idx_y[r]];
								double d_i = Wx*Datasetx[pairs_idx_x[r]] - Wy*Datasety[pairs_idx_y[r]];
								double gtau_one_i = 0;
								if (fabs(d_i) > scisd_a*scisd_lambda)
										{gtau_one_i = d_i;}
								else if (fabs(d_i) > scisd_lambda) {
									if (d_i >= 0) {
										gtau_one_i = (scisd_a*d_i - scisd_a*scisd_lambda)/(scisd_a-1);
									}
									else {
										gtau_one_i = (scisd_a*d_i + scisd_a*scisd_lambda)/(scisd_a-1);
									}
								}

								cross_grad_index.push_back(r);
								cross_grad_weights.push_back(-eta*pweights[r]*gamma_cross*(-2*pairs_label[r]*d_i + (1-pairs_label[r])*(-gtau_one_i+d_curr)));///num_pairs_xy
							  }

							  // scale w
							  Wy.scale(1.0 - eta*lambda_y);

							  // and add sub-gradients
							  for (uint j=0; j<grad_index.size(); ++j) {
								Wy.add(Datasety[grad_index[j]], grad_weights[j]);
							  }

							  // and add sub-gradients 2 for cross-modal similarity
							  for (uint j=0; j<cross_grad_index.size(); ++j) {
								Wy.add(Datasety[pairs_idx_y[cross_grad_index[j]]], cross_grad_weights[j]);
							  }

							  // Project if needed
							  if (projection_rule == 0) { // Pegasos projection rule
								double norm2 = Wy.snorm();
								if (norm2 > 1.0/lambda_y) {
									Wy.scale(sqrt(1.0/(lambda_y*norm2)));
								}
							  } else if (projection_rule == 1) { // other projection
								double norm2 = Wy.snorm();
								if (norm2 > (projection_constant*projection_constant)) {
									Wy.scale(projection_constant/sqrt(norm2));
								}
							  } // else -- no projection
							}// ---------------- End of Inner Pegasos Loop -------------------

							  // Calculate objective value
//							  startTime = get_runtime();
							  norm_value = Wy.snorm();
							  obj_wy = norm_value * lambda_y / 2.0;
							  //loss_wx = 0.0;
							  //zero_one_error = 0.0;
							  for (uint i=0; i < Datasety.size(); ++i) {
								double cur_loss = 1 - fabs(Wy * Datasety[i]);
								if (cur_loss < 0.0) cur_loss = 0.0;
								//loss_value += cur_loss/num_examples;
								obj_wy += cur_loss/num_examples_y;
								//if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
							  }
							  for (uint i=0; i < num_pairs_xy; ++i) {
								  double d_i = Wx*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
								  double tau_i = 0;
								  if (fabs(d_i) <= scisd_lambda)
										{tau_i = -0.5*d_i*d_i + 0.5*scisd_a*scisd_lambda*scisd_lambda;}
								  else if (fabs(d_i) <= scisd_a*scisd_lambda) {
									  tau_i = (d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
								  }
								  obj_wy += pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i+(1-pairs_label[i])*tau_i);///num_pairs_xy
							  }
//							  endTime = get_runtime();
//							  calc_obj_time = endTime - startTime;

//							  // calc convex upper bound for wy
//							  norm_value = Wy.snorm();
//							  double obj_wy_upper = norm_value * lambda_y / 2.0;
//  //							  std::cerr << "Upper for x:"<< "\t" << obj_wx_upper << std::endl;
//							  for (uint i=0; i < Datasety.size(); ++i) {
//								double cur_loss = 1 - fabs(Wy * Datasety[i]);
//								if (cur_loss < 0.0) cur_loss = 0.0;
//								//loss_value += cur_loss/num_examples;
//								obj_wy_upper += cur_loss/num_examples_y;
//								//if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
//							  }
//  //							  std::cerr << "Upper for x:"<< "\t" << obj_wx_upper << std::endl;
//							  Wy_tmp = Wy;
//							  Wy_tmp.add(Wy_curr,-1.0);
//							  for (uint i=0; i < num_pairs_xy; ++i) {
//								  double d_i = Wx*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
//								  double d_i_curr = Wx*Datasetx[pairs_idx_x[i]] - Wy_curr*Datasety[pairs_idx_y[i]];
//								  double tau_i_1 = 0;
//								  if (fabs(d_i) > scisd_a*scisd_lambda) {
//									  tau_i_1 = 0.5*d_i*d_i - 0.5*scisd_a*scisd_lambda*scisd_lambda;
//								  }
//								  else if (fabs(d_i) > scisd_lambda) {
//									  tau_i_1 = (scisd_a*d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
//								  }
//								  obj_wy_upper += pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i + (1-pairs_label[i])*(tau_i_1 - (0.5*d_i_curr*d_i_curr-0.5*scisd_a*scisd_lambda*scisd_lambda) + d_i_curr*(Wy_tmp*Datasety[pairs_idx_y[i]])));///num_pairs_xy
//								  // std::cerr << pweights[i] <<"\t"<<gamma_cross <<"\t"<<pairs_label[i] <<"\t"<<d_i<<"\t"<<"Upper for x:"<< "\t" << obj_wx_upper << std::endl;
//							  }
//  //							  for (int i = 0; i < num_pairs_xy; ++i) {
//  //								  std::cerr << "pweights["<< i << "]="<< pweights[i] << std::endl;
//  //							  }
//							  std::cerr << obj_wy << "\t"<< obj_wy_upper << ";"<<std::endl;

							  // check convergence
							  iters_y++;
							  if ((fabs(obj_wy - obj_wy_old) < 0.0001)||(iters_y>20)) {
								  converge_wy = true;
							  } else {
								  obj_wy_old = obj_wy;
							  }
					  }// end of while for updating wy
					  //std::cerr << "End of updating Wy!"<< std::endl;//exit(EXIT_FAILURE);
					  // ----------------- end of updating wy ---------------------------------

					  // here we check the convergence after updating wx and wy
					  // Calculate objective value
//					  startTime = get_runtime();
					  norm_value = Wx.snorm();
					  obj_value = norm_value * lambda_x / 2.0;
					  norm_value = Wy.snorm();
					  obj_value += norm_value * lambda_y / 2.0;

					  for (uint i=0; i < Datasetx.size(); ++i) {
						double cur_loss = 1 - fabs(Wx * Datasetx[i]);
						if (cur_loss < 0.0) cur_loss = 0.0;
						obj_value += cur_loss/num_examples_x;
					  }
					  for (uint i=0; i < Datasety.size(); ++i) {
						double cur_loss = 1 - fabs(Wy * Datasety[i]);
						if (cur_loss < 0.0) cur_loss = 0.0;
						obj_value += cur_loss/num_examples_y;
					  }
					  for (uint i=0; i < num_pairs_xy; ++i) {
						  double d_i = Wx*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
						  double tau_i = 0;
						  if (fabs(d_i) <= scisd_lambda)
								{tau_i = -0.5*d_i*d_i + 0.5*scisd_a*scisd_lambda*scisd_lambda;}
						  else if (fabs(d_i) <= scisd_a*scisd_lambda) {
							  tau_i = (d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
						  }
						  obj_value += pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i+(1-pairs_label[i])*tau_i);///num_pairs_xy
					  }


					  // check convergence
					  iters_xy++;
					  if ((fabs(obj_value - obj_value_old) < 0.0001)||(iters_xy>20)) {
						  converge_wxwy = true;
					  } else {
						  obj_value_old = obj_value;
					  }
				  }//end of updating wx and wy for one bit*/
				  endTime = get_runtime();
				  calc_obj_time = endTime - startTime;

				  std::cerr << "End of updating "<< k << " bit using "<< calc_obj_time << "seconds." << std::endl;//exit(EXIT_FAILURE);

				  //std::cerr << "I am here!"<< std::endl;
				  // now wx and wy for k-th bit are printed out in files
					char buffer_name [50];
					if (model_filename != "noModelFile") {
						//std::cerr << "I am here!"<< std::endl;
						sprintf(buffer_name, "%s_x_%d.model", model_filename.c_str(), k);
						std::ofstream model_filex(buffer_name);
						if (!model_filex.good()) {
							std::cerr << "error w/ " << model_filename << std::endl;
							exit(EXIT_FAILURE);
						}
						Wx.print2(model_filex);
						model_filex.close();

						sprintf(buffer_name, "%s_y_%d.model", model_filename.c_str(), k);
						std::ofstream model_filey(buffer_name);
						if (!model_filey.good()) {
							std::cerr << "error w/ " << model_filename << std::endl;
							exit(EXIT_FAILURE);
						}
						Wy.print2(model_filey);
						model_filey.close();
					  }

					// now we update the pair weights
					double boostloss = 0.0;
					double* plabels = new double [num_pairs_xy];
					int xlabel, ylabel;
					for (uint i=0; i < num_pairs_xy; ++i) {

						if ((Wx*Datasetx[pairs_idx_x[i]])>=0) {
							xlabel = 1;
						} else {
							xlabel = -1;
						}
						if ((Wy*Datasety[pairs_idx_y[i]])>=0) {
							ylabel = 1;
						} else {
							ylabel = -1;
						}
						if (( xlabel * ylabel) >0) {
							plabels[i] = 1;
						} else {
							plabels[i] = 0;
						}
						if (plabels[i] != pairs_label[i]) {
							boostloss += pweights[i];
						}
						//std::cerr << i << "\t" << pweights[i] << "\t" << "I am here!"<< std::endl;
					  }

					//std::cerr << k << "I am here!"<< std::endl;
					if (boostloss >= 1) {
						std::cerr << "Abnormal boosting loss: "<< boostloss << std::endl; exit(EXIT_FAILURE);
					}
					double decayrate = boostloss / (1-boostloss);
					double totalweights = 0.0;
					for (uint i=0; i < num_pairs_xy; ++i) {
						if (plabels[i] == pairs_label[i]) {
							pweights[i] = pweights[i] * decayrate;
						}
						totalweights += pweights[i];
					}
					// normalize weights by sum
					for (uint i=0; i < num_pairs_xy; ++i) {
						pweights[i] /= totalweights;
					}
					// end for one bit
			  }// end for all bits
			   //*/
}

//void LearnBCRH2andValidate(// Input variables
//	      std::vector<simple_sparse_vector>& Datasetx,
//	      std::vector<int>& Labelsx,
//	      std::vector<simple_sparse_vector>& Datasety,
//	      std::vector<int>& Labelsy,
//	      double& best_lambda_x, double& best_lambda_y, double& best_gamma_cross) {
//	// first give some candiates
//	// lambda = [0.01, 0.1,1,10,100,1000]
//	// gamma = [0.01, 0.1,1,10,100,1000]
//
//	for (int i = 0; i<6; i++) {
//		double lambdax = 0.01*pow(10,i);
//		double lambday = 0.01*pow(10,i);
//		for (int j = 0; j<6; j++) {
//			double gamma_cr = 0.01*pow(10,j);
//			// now we conduct 5-fold cv
//			double totalerror = 0.0;
//			for (int ifold = 0; ifold < 5; ifold++) {
//				// read the pairs
//				std::vector<uint> train_pairs_idx_x;
//				std::vector<uint> train_pairs_idx_y;
//				std::vector<double> train_pairs_label;
//				ReadCrossData(train_data_filename_cross,train_pairs_idx_x,train_pairs_idx_y,pairs_label,readingTime);
//			}
//		}
//
//	}
//}

void LearnBCRH2andValidate(// Input variables
	      std::vector<simple_sparse_vector>& Datasetx,
	      std::vector<int>& Labelsx,
	      std::vector<simple_sparse_vector>& Datasety,
	      std::vector<int>& Labelsy,
	      std::vector<uint>& train_pairs_idx_x,
		  std::vector<uint>& train_pairs_idx_y,
		  std::vector<double>& train_pairs_label,
		  std::vector<uint>& test_pairs_idx_x,
		  std::vector<uint>& test_pairs_idx_y,
		  std::vector<double>& test_pairs_label,
	      uint dimensionx,
	      uint dimensiony,
	      uint code_length,
	      double lambda_x,double lambda_y, double gamma_cross,
	      int max_iter_x, int max_iter_y,
	      int exam_per_iter,
	      std::string& model_filename,
	      // Output variables
	      long& train_time,long& calc_obj_time,
	      double& obj_value,double& norm_value,
	      double& test_error,
//		      double& loss_value,double& zero_one_error,
//		      double& test_loss,double& test_error,
	      // additional parameters
	      double scisd_a, double scisd_lambda,
	      int eta_rule_type , double eta_constant,
	      int projection_rule, double projection_constant) {

	  uint num_examples_x = Labelsx.size();
	  uint num_examples_y = Labelsy.size();
	  uint train_num_pairs_xy = train_pairs_idx_x.size();
	  uint test_num_pairs_xy = test_pairs_idx_x.size();
	  // std::cerr << scisd_a <<"\t"<<scisd_lambda<< std::endl;
	  if (train_num_pairs_xy != train_pairs_idx_y.size()) {
			  std::cerr << "checking y" << std::endl;
			  std::cerr << "error w/ point pairs!"<< std::endl;
			  std::cerr << train_pairs_idx_y.size() << std::endl;
			  std::cerr << train_num_pairs_xy << std::endl;
			  exit(EXIT_FAILURE);
			}
	  if (train_num_pairs_xy != train_pairs_label.size()) {
		  std::cerr << "checking label" << std::endl;
				std::cerr << "error w/ point pairs!"<< std::endl;
				std::cerr << train_pairs_label.size() << std::endl;
				std::cerr << train_num_pairs_xy << std::endl;
				exit(EXIT_FAILURE);
			  }

	  long startTime = get_runtime();
	  long endTime;

	  bool converge_wxwy, converge_wx, converge_wy;

	  // Initialization of weightings for cross-modal pairs for boosting
	  double* pweights = new double [train_num_pairs_xy];
	  for (uint i = 0; i<train_num_pairs_xy; ++i) {
		  pweights[i] = 1.0/(double) train_num_pairs_xy;
	  }

	// we first learn a model
	// Initialization of classification vector
	  WeightVector Wx(dimensionx);
	  WeightVector Wx_curr(dimensionx);
	  WeightVector Wx_tmp(dimensionx);
	  WeightVector Wy(dimensiony);
	  WeightVector Wy_curr(dimensiony);
	  WeightVector Wy_tmp(dimensiony);

//	  startTime = get_runtime();

	  // while for updating wx and wy
	  double obj_value_old = 10000000; // total objective
	  converge_wxwy = false;
	  int iters_xy = 0;
	  ///*
	  while (converge_wxwy == false) {// update wx and wy iteratively until convergence
		  std::cerr << "Objective for total:"<< "\t" << obj_value_old << std::endl;

		  // ----------------- start of updating wx ---------------------------------
		  // for updating wx
		  double obj_wx = 0.0;
		  double obj_wx_old = 1000000;
		  converge_wx = false;
		  int iters_x = 0;
		  while (converge_wx == false) {
			  //std::cerr << "Objective for x:"<< "\t" << obj_wx_old << std::endl;
			  // CCCP iterations
			  Wx_curr = Wx;// for current CCCP iteration
			  // ---------------- Inner Pegasos Loop -------------------
				for (int i = 0; i < max_iter_x; ++i) {

				  // learning rate
				  double eta;
				  if (eta_rule_type == 0) { // Pegasos eta rule
					eta = 1 / (lambda_x * (i+2));
				  } else if (eta_rule_type == 1) { // Norma rule
					eta = eta_constant / sqrt(i+2);
					// solve numerical problems
					//if (projection_rule != 2)
					Wx.make_my_a_one();
				  } else {
					eta = eta_constant;
				  }
				  //std::cerr << eta << "\t" << "learning rate!"<< std::endl;

				  // gradient indices and losses
				  std::vector<uint> grad_index;
				  std::vector<double> grad_weights;
				  // gradient indices and losses for cross
				  std::vector<uint> cross_grad_index;
				  std::vector<double> cross_grad_weights;

				  // calc sub-gradients for (at most) exam_per_iter points
				  for (int j=0; j < exam_per_iter; ++j) {

					// choose random example
					uint r = ((int)rand()) % num_examples_x;

					// calculate prediction
					double prediction = Wx*Datasetx[r];
					double Labelr = (prediction >=0)?+1.0:-1.0;

					// calculate loss
					double cur_loss = 1 - Labelr*prediction;
					if (cur_loss < 0.0) cur_loss = 0.0;

					// and add to the gradient
					if (cur_loss > 0.0) {
						grad_index.push_back(r);
						grad_weights.push_back(eta*Labelr/exam_per_iter);
					}
				  }

				  // calc sub-gradients2 for cross-modal similarity for all pairs
				  for (uint j=0; j < train_num_pairs_xy; ++j) {

					// calculate prediction
					double d_curr = Wx_curr*Datasetx[train_pairs_idx_x[j]] - Wy*Datasety[train_pairs_idx_y[j]];
					double d_i = Wx*Datasetx[train_pairs_idx_x[j]] - Wy*Datasety[train_pairs_idx_y[j]];
					double gtau_one_i = 0;
					if (fabs(d_i) > scisd_a*scisd_lambda) {
						gtau_one_i = d_i;
					}
					else if (fabs(d_i) > scisd_lambda) {
						if (d_i >= 0) {
							gtau_one_i = (scisd_a*d_i - scisd_a*scisd_lambda)/(scisd_a-1);
						}
						else {
							gtau_one_i = (scisd_a*d_i + scisd_a*scisd_lambda)/(scisd_a-1);
						}
					}

					cross_grad_index.push_back(j);
					cross_grad_weights.push_back(-eta*pweights[j]*gamma_cross*(2*train_pairs_label[j]*d_i + (1-train_pairs_label[j])*(gtau_one_i-d_curr)));///num_pairs_xy
				  }

				  // scale w
				  Wx.scale(1.0 - eta*lambda_x);

				  // and add sub-gradients
				  for (uint j=0; j<grad_index.size(); ++j) {
					Wx.add(Datasetx[grad_index[j]], grad_weights[j]);
				  }

				  // and add sub-gradients 2 for cross-modal similarity
				  for (uint j=0; j<cross_grad_index.size(); ++j) {
					Wx.add(Datasetx[train_pairs_idx_x[cross_grad_index[j]]], cross_grad_weights[j]);
				  }

				  // Project if needed
				  if (projection_rule == 0) { // Pegasos projection rule
					double norm2 = Wx.snorm();
					if (norm2 > 1.0/lambda_x) {
						Wx.scale(sqrt(1.0/(lambda_x*norm2)));
					}
				  } else if (projection_rule == 1) { // other projection
					double norm2 = Wx.snorm();
					if (norm2 > (projection_constant*projection_constant)) {
						Wx.scale(projection_constant/sqrt(norm2));
					}
				  } // else -- no projection
				}// ---------------- End of Inner Pegasos Loop -------------------

				  // Calculate objective value for x
	//							  startTime = get_runtime();
				  norm_value = Wx.snorm();
				  obj_wx = norm_value * lambda_x / 2.0;
				  // std::cerr << "Objective for x:"<< "\t" << obj_wx << std::endl;
				  for (uint i=0; i < Datasetx.size(); ++i) {
					double cur_loss = 1 - fabs(Wx * Datasetx[i]);
					if (cur_loss < 0.0) cur_loss = 0.0;
					obj_wx += cur_loss/num_examples_x;
				  }
	//							  std::cerr << "Objective for x:"<< "\t" << obj_wx << std::endl;
				  for (uint i=0; i < train_num_pairs_xy; ++i) {
					  double d_i = Wx*Datasetx[train_pairs_idx_x[i]] - Wy*Datasety[train_pairs_idx_y[i]];
					  double tau_i = 0;
					  if (fabs(d_i) <= scisd_lambda) {
						  tau_i = -0.5*d_i*d_i + 0.5*scisd_a*scisd_lambda*scisd_lambda;
					  }
					  else if (fabs(d_i) <= scisd_a*scisd_lambda) {
						  tau_i = (d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
					  }
					  obj_wx += pweights[i]*gamma_cross*(train_pairs_label[i]*d_i*d_i+(1-train_pairs_label[i])*tau_i);///num_pairs_xy
					  // std::cerr << pweights[i] <<"\t"<<gamma_cross <<"\t"<<pairs_label[i] <<"\t"<<tau_i<<"\t"<<d_i<<"\t"<<scisd_a*scisd_lambda<<"\t"<<pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i+(1-pairs_label[i])*tau_i)<<"\t"<<"Objective for x:"<< "\t" << obj_wx << std::endl;
				  }
				  // std::cerr << "Objective for x:"<< "\t" << obj_wx << std::endl;
	//							  endTime = get_runtime();
	//							  calc_obj_time = endTime - startTime;

				  // calc convex upper bound for wx
	//							  norm_value = Wx.snorm();
	//							  double obj_wx_upper = norm_value * lambda_x / 2.0;
	////							  std::cerr << "Upper for x:"<< "\t" << obj_wx_upper << std::endl;
	//							  //loss_wx = 0.0;
	//							  //zero_one_error = 0.0;
	//							  for (uint i=0; i < Datasetx.size(); ++i) {
	//								double cur_loss = 1 - fabs(Wx * Datasetx[i]);
	//								if (cur_loss < 0.0) cur_loss = 0.0;
	//								//loss_value += cur_loss/num_examples;
	//								obj_wx_upper += cur_loss/num_examples_x;
	//								//if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
	//							  }
	////							  std::cerr << "Upper for x:"<< "\t" << obj_wx_upper << std::endl;
	//							  Wx_tmp = Wx;
	//							  Wx_tmp.add(Wx_curr,-1.0);
	//							  for (uint i=0; i < num_pairs_xy; ++i) {
	//								  double d_i = Wx*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
	//								  double d_i_curr = Wx_curr*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
	//								  double tau_i_1 = 0;
	//								  if (fabs(d_i) > scisd_a*scisd_lambda) {
	//									  tau_i_1 = 0.5*d_i*d_i - 0.5*scisd_a*scisd_lambda*scisd_lambda;
	//								  }
	//								  else if (fabs(d_i) > scisd_lambda) {
	//									  tau_i_1 = (scisd_a*d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
	//								  }
	//								  obj_wx_upper += pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i + (1-pairs_label[i])*(tau_i_1 - (0.5*d_i_curr*d_i_curr-0.5*scisd_a*scisd_lambda*scisd_lambda) - d_i_curr*(Wx_tmp*Datasetx[pairs_idx_x[i]])));///num_pairs_xy
	//								  // std::cerr << pweights[i] <<"\t"<<gamma_cross <<"\t"<<pairs_label[i] <<"\t"<<d_i<<"\t"<<"Upper for x:"<< "\t" << obj_wx_upper << std::endl;
	//							  }
	////							  for (int i = 0; i < num_pairs_xy; ++i) {
	////								  std::cerr << "pweights["<< i << "]="<< pweights[i] << std::endl;
	////							  }
	//							  std::cerr << obj_wx << "\t"<< obj_wx_upper << ";"<<std::endl;
	////							  std::cerr << "X: Objective: "<< obj_wx << "\t UpperBound: "<< obj_wx_upper << std::endl;

				  // check convergence
				  iters_x++;
				  if ((fabs(obj_wx - obj_wx_old) < 0.0001)||(iters_x>20)) {
					  converge_wx = true;
				  } else {
					  obj_wx_old = obj_wx;
				  }

		  }// end of while for updating wx
		  //std::cerr << "End of updating Wx!"<< std::endl;//exit(EXIT_FAILURE);
		  // ----------------- end of updating wx ---------------------------------

		  // ----------------- start of updating wy ---------------------------------
		  // for updating wy
		  double obj_wy = 0.0;
		  double obj_wy_old = 1000000;
		  converge_wy = false;
		  int iters_y = 0;
		  while (converge_wy == false) {
			  //std::cerr << "Objective for y:"<< "\t" << obj_wy_old << std::endl;
			  // CCCP iterations
			  Wy_curr = Wy;// for current CCCP iteration
			  // ---------------- Inner Pegasos Loop -------------------
			  for (int i = 0; i < max_iter_y; ++i) {
				  // learning rate
				  double eta;
				  if (eta_rule_type == 0) { // Pegasos eta rule
					eta = 1 / (lambda_y * (i+2));
				  } else if (eta_rule_type == 1) { // Norma rule
					eta = eta_constant / sqrt(i+2);
					// solve numerical problems
					//if (projection_rule != 2)
					Wy.make_my_a_one();
				  } else {
					eta = eta_constant;
				  }
				  //std::cerr << eta << "\t" << "learning rate!"<< std::endl;

				  // gradient indices and losses
				  std::vector<uint> grad_index;
				  std::vector<double> grad_weights;
				  // gradient indices and losses for cross
				  std::vector<uint> cross_grad_index;
				  std::vector<double> cross_grad_weights;

				  // calc sub-gradients
				  for (int j=0; j < exam_per_iter; ++j) {

					// choose random example
					uint r = ((int)rand()) % num_examples_y;

					// calculate prediction
					double prediction = Wy*Datasety[r];
					double Labelr = (prediction >=0)?+1.0:-1.0;

					// calculate loss
					double cur_loss = 1 - Labelr*prediction;
					if (cur_loss < 0.0) cur_loss = 0.0;

					// and add to the gradient
					if (cur_loss > 0.0) {
						grad_index.push_back(r);
						grad_weights.push_back(-eta*Labelr/exam_per_iter);
					}
				  }

				  // calc sub-gradients2 for cross-modal similarity
				  for (uint j=0; j < train_num_pairs_xy; ++j) {

					// calculate prediction
					double d_curr = Wx*Datasetx[train_pairs_idx_x[j]] - Wy_curr*Datasety[train_pairs_idx_y[j]];
					double d_i = Wx*Datasetx[train_pairs_idx_x[j]] - Wy*Datasety[train_pairs_idx_y[j]];
					double gtau_one_i = 0;
					if (fabs(d_i) > scisd_a*scisd_lambda)
							{gtau_one_i = d_i;}
					else if (fabs(d_i) > scisd_lambda) {
						if (d_i >= 0) {
							gtau_one_i = (scisd_a*d_i - scisd_a*scisd_lambda)/(scisd_a-1);
						}
						else {
							gtau_one_i = (scisd_a*d_i + scisd_a*scisd_lambda)/(scisd_a-1);
						}
					}

					cross_grad_index.push_back(j);
					cross_grad_weights.push_back(-eta*pweights[j]*gamma_cross*(-2*train_pairs_label[j]*d_i + (1-train_pairs_label[j])*(-gtau_one_i+d_curr)));///num_pairs_xy
				  }

				  // scale w
				  Wy.scale(1.0 - eta*lambda_y);

				  // and add sub-gradients
				  for (uint j=0; j<grad_index.size(); ++j) {
					Wy.add(Datasety[grad_index[j]], grad_weights[j]);
				  }

				  // and add sub-gradients 2 for cross-modal similarity
				  for (uint j=0; j<cross_grad_index.size(); ++j) {
					Wy.add(Datasety[train_pairs_idx_y[cross_grad_index[j]]], cross_grad_weights[j]);
				  }

				  // Project if needed
				  if (projection_rule == 0) { // Pegasos projection rule
					double norm2 = Wy.snorm();
					if (norm2 > 1.0/lambda_y) {
						Wy.scale(sqrt(1.0/(lambda_y*norm2)));
					}
				  } else if (projection_rule == 1) { // other projection
					double norm2 = Wy.snorm();
					if (norm2 > (projection_constant*projection_constant)) {
						Wy.scale(projection_constant/sqrt(norm2));
					}
				  } // else -- no projection
				}// ---------------- End of Inner Pegasos Loop -------------------

				  // Calculate objective value
	//							  startTime = get_runtime();
				  norm_value = Wy.snorm();
				  obj_wy = norm_value * lambda_y / 2.0;
				  //loss_wx = 0.0;
				  //zero_one_error = 0.0;
				  for (uint i=0; i < Datasety.size(); ++i) {
					double cur_loss = 1 - fabs(Wy * Datasety[i]);
					if (cur_loss < 0.0) cur_loss = 0.0;
					//loss_value += cur_loss/num_examples;
					obj_wy += cur_loss/num_examples_y;
					//if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
				  }
				  for (uint i=0; i < train_num_pairs_xy; ++i) {
					  double d_i = Wx*Datasetx[train_pairs_idx_x[i]] - Wy*Datasety[train_pairs_idx_y[i]];
					  double tau_i = 0;
					  if (fabs(d_i) <= scisd_lambda)
							{tau_i = -0.5*d_i*d_i + 0.5*scisd_a*scisd_lambda*scisd_lambda;}
					  else if (fabs(d_i) <= scisd_a*scisd_lambda) {
						  tau_i = (d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
					  }
					  obj_wy += pweights[i]*gamma_cross*(train_pairs_label[i]*d_i*d_i+(1-train_pairs_label[i])*tau_i);///num_pairs_xy
				  }
	//							  endTime = get_runtime();
	//							  calc_obj_time = endTime - startTime;

	//							  // calc convex upper bound for wy
	//							  norm_value = Wy.snorm();
	//							  double obj_wy_upper = norm_value * lambda_y / 2.0;
	//  //							  std::cerr << "Upper for x:"<< "\t" << obj_wx_upper << std::endl;
	//							  for (uint i=0; i < Datasety.size(); ++i) {
	//								double cur_loss = 1 - fabs(Wy * Datasety[i]);
	//								if (cur_loss < 0.0) cur_loss = 0.0;
	//								//loss_value += cur_loss/num_examples;
	//								obj_wy_upper += cur_loss/num_examples_y;
	//								//if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
	//							  }
	//  //							  std::cerr << "Upper for x:"<< "\t" << obj_wx_upper << std::endl;
	//							  Wy_tmp = Wy;
	//							  Wy_tmp.add(Wy_curr,-1.0);
	//							  for (uint i=0; i < num_pairs_xy; ++i) {
	//								  double d_i = Wx*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
	//								  double d_i_curr = Wx*Datasetx[pairs_idx_x[i]] - Wy_curr*Datasety[pairs_idx_y[i]];
	//								  double tau_i_1 = 0;
	//								  if (fabs(d_i) > scisd_a*scisd_lambda) {
	//									  tau_i_1 = 0.5*d_i*d_i - 0.5*scisd_a*scisd_lambda*scisd_lambda;
	//								  }
	//								  else if (fabs(d_i) > scisd_lambda) {
	//									  tau_i_1 = (scisd_a*d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
	//								  }
	//								  obj_wy_upper += pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i + (1-pairs_label[i])*(tau_i_1 - (0.5*d_i_curr*d_i_curr-0.5*scisd_a*scisd_lambda*scisd_lambda) + d_i_curr*(Wy_tmp*Datasety[pairs_idx_y[i]])));///num_pairs_xy
	//								  // std::cerr << pweights[i] <<"\t"<<gamma_cross <<"\t"<<pairs_label[i] <<"\t"<<d_i<<"\t"<<"Upper for x:"<< "\t" << obj_wx_upper << std::endl;
	//							  }
	//  //							  for (int i = 0; i < num_pairs_xy; ++i) {
	//  //								  std::cerr << "pweights["<< i << "]="<< pweights[i] << std::endl;
	//  //							  }
	//							  std::cerr << obj_wy << "\t"<< obj_wy_upper << ";"<<std::endl;

				  // check convergence
				  iters_y++;
				  if ((fabs(obj_wy - obj_wy_old) < 0.0001)||(iters_y>20)) {
					  converge_wy = true;
				  } else {
					  obj_wy_old = obj_wy;
				  }
		  }// end of while for updating wy
		  //std::cerr << "End of updating Wy!"<< std::endl;//exit(EXIT_FAILURE);
		  // ----------------- end of updating wy ---------------------------------

		  // here we check the convergence after updating wx and wy
		  // Calculate objective value
	//					  startTime = get_runtime();
		  norm_value = Wx.snorm();
		  obj_value = norm_value * lambda_x / 2.0;
		  norm_value = Wy.snorm();
		  obj_value += norm_value * lambda_y / 2.0;

		  for (uint i=0; i < Datasetx.size(); ++i) {
			double cur_loss = 1 - fabs(Wx * Datasetx[i]);
			if (cur_loss < 0.0) cur_loss = 0.0;
			obj_value += cur_loss/num_examples_x;
		  }
		  for (uint i=0; i < Datasety.size(); ++i) {
			double cur_loss = 1 - fabs(Wy * Datasety[i]);
			if (cur_loss < 0.0) cur_loss = 0.0;
			obj_value += cur_loss/num_examples_y;
		  }
		  for (uint i=0; i < train_num_pairs_xy; ++i) {
			  double d_i = Wx*Datasetx[train_pairs_idx_x[i]] - Wy*Datasety[train_pairs_idx_y[i]];
			  double tau_i = 0;
			  if (fabs(d_i) <= scisd_lambda)
					{tau_i = -0.5*d_i*d_i + 0.5*scisd_a*scisd_lambda*scisd_lambda;}
			  else if (fabs(d_i) <= scisd_a*scisd_lambda) {
				  tau_i = (d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
			  }
			  obj_value += pweights[i]*gamma_cross*(train_pairs_label[i]*d_i*d_i+(1-train_pairs_label[i])*tau_i);///num_pairs_xy
		  }


		  // check convergence
		  iters_xy++;
		  if ((fabs(obj_value - obj_value_old) < 0.0001)||(iters_xy>20)) {
			  converge_wxwy = true;
		  } else {
			  obj_value_old = obj_value;
		  }
	  }//end of updating wx and wy for one bit*/
	  endTime = get_runtime();;
	  calc_obj_time = endTime-startTime;
	  std::cerr << "End of learning one bit using "<< calc_obj_time << "seconds." << std::endl;

	  // now we have Wx and Wy, and test their error on test pairs
	  test_error = 0.0;
	  int xlabel, ylabel;
	  double plabel = 0.0;
	  for (uint i = 0; i<test_num_pairs_xy; ++i) {
		  if ((Wx*Datasetx[test_pairs_idx_x[i]])>=0) {
				xlabel = 1;
			} else {
				xlabel = -1;
			}
			if ((Wy*Datasety[test_pairs_idx_y[i]])>=0) {
				ylabel = 1;
			} else {
				ylabel = -1;
			}
			if (( xlabel * ylabel) >0) {
				plabel = 1.0;
			} else {
				plabel = 0.0;
			}
			if (plabel != test_pairs_label[i]) {
				test_error++;
			}
	  }
	  test_error = test_error/(double)test_num_pairs_xy;
}

void LearnBCRH(// Input variables
	      std::vector<simple_sparse_vector>& Datasetx,
	      std::vector<int>& Labelsx,
	      std::vector<simple_sparse_vector>& Datasety,
	      std::vector<int>& Labelsy,
	      std::vector<uint>& pairs_idx_x,
	      std::vector<uint>& pairs_idx_y,
	      std::vector<double>& pairs_label,
	      uint dimensionx,
	      uint dimensiony,
	      int code_length,
	      double lambda_x,double lambda_y, double gamma_cross,
	      int max_iter_x, int max_iter_y,
	      int exam_per_iter,
	      std::string& model_filename,
	      // Output variables
	      long& train_time, long& calc_obj_time,
	      double& obj_value, double& norm_value,
//	      double& loss_value, double& zero_one_error,
//	      double& test_loss, double& test_error,
	      // additional parameters
	      double scisd_a, double scisd_lambda,
	      int eta_rule_type , double eta_constant,
	      int projection_rule, double projection_constant) {
	/*
		  uint num_examples_x = Labelsx.size();
		  uint num_examples_y = Labelsy.size();
		  uint num_pairs_xy = pairs_idx_x.size();
		  if (num_pairs_xy != pairs_idx_y.size()) {
				  std::cerr << "error w/ point pairs!"<< std::endl;
				  exit(EXIT_FAILURE);
				}
		  if (num_pairs_xy != pairs_label.size()) {
					std::cerr << "error w/ point pairs!"<< std::endl;
					exit(EXIT_FAILURE);
				  }

		  long startTime = get_runtime();
		  long endTime;

		  bool converge_wxwy, converge_wx, converge_wy;


		  // Initialization of classification vector
		  WeightVector Wx(dimensionx);
		  WeightVector Wx_curr(dimensionx);
		  WeightVector Wy(dimensiony);
		  WeightVector Wy_curr(dimensiony);

		  // Initialization of weightings for cross-modal pairs
		  double* pweights = new double [num_pairs_xy];
		  for (uint i = 0; i<num_pairs_xy; ++i) {
			  pweights[i] = 1.0/(double) num_pairs_xy;
		  }


		  for (int k = 0; k < code_length; ++k) {
			  // for each bit
			  // while for updating wx and wy
			  double obj_value_old = 10000000;
			  while (converge_wxwy == false) {
				  // for updating wx
				  double obj_wx = 0.0;
				  double obj_wx_old = 1000000;
				  converge_wx = false;
				  while (converge_wx == false) {
					  // CCCP iterations
					  Wx_curr = Wx;// for current CCCP iteration
					  // ---------------- Inner Pegasos Loop -------------------
						for (int i = 0; i < max_iter_x; ++i) {

						  // learning rate
						  double eta;
						  if (eta_rule_type == 0) { // Pegasos eta rule
							eta = 1 / (lambda_x * (i+2));
						  } else if (eta_rule_type == 1) { // Norma rule
							eta = eta_constant / sqrt(i+2);
							// solve numerical problems
							//if (projection_rule != 2)
							Wx.make_my_a_one();
						  } else {
							eta = eta_constant;
						  }

						  // gradient indices and losses
						  std::vector<uint> grad_index;
						  std::vector<double> grad_weights;
						  // gradient indices and losses for cross
						  std::vector<uint> cross_grad_index;
						  std::vector<double> cross_grad_weights;

						  // calc sub-gradients
						  for (int j=0; j < exam_per_iter; ++j) {

							// choose random example
							uint r = ((int)rand()) % num_examples_x;

							// calculate prediction
							double prediction = Wx*Datasetx[r];
							double Labelr = (prediction >=0)?+1.0:-1.0;

							// calculate loss
							double cur_loss = 1 - Labelr*prediction;
							if (cur_loss < 0.0) cur_loss = 0.0;

							// and add to the gradient
							if (cur_loss > 0.0) {
								grad_index.push_back(r);
								grad_weights.push_back(eta*Labelr/exam_per_iter);
							}
						  }

						  // calc sub-gradients2 for cross-modal similarity
						  for (uint j=0; j < num_pairs_xy; ++j) {

							// calculate prediction
							double d_curr = Wx_curr*Datasetx[pairs_idx_x[j]] - Wy*Datasety[pairs_idx_y[j]];
							double d_i = Wx*Datasetx[pairs_idx_x[j]] - Wy*Datasety[pairs_idx_y[j]];
							double gtau_one_i = 0;
							if (fabs(d_i) >= scisd_a*scisd_lambda)
									{gtau_one_i = d_i;}
							else if (fabs(d_i) >= scisd_lambda) {
								if (d_i >= 0) {
									gtau_one_i = (scisd_a*d_i-scisd_a*scisd_lambda)/(scisd_a-1);
								}
								else {
									gtau_one_i = (scisd_a*d_i-scisd_a*scisd_lambda)/(scisd_a-1);
								}
							}

							cross_grad_index.push_back(j);
							cross_grad_weights.push_back(0.5*pweights[j]*gamma_cross*(2*pairs_label[j]*d_i+(1-pairs_label[j])*(gtau_one_i-d_curr))/num_pairs_xy);
						  }

						  // scale w
						  Wx.scale(1.0 - eta*lambda_x);

						  // and add sub-gradients
						  for (uint j=0; j<grad_index.size(); ++j) {
							Wx.add(Datasetx[grad_index[j]], grad_weights[j]);
						  }

						  // and add sub-gradients 2 for cross-modal similarity
						  for (uint j=0; j<cross_grad_index.size(); ++j) {
							Wx.add(Datasetx[pairs_idx_x[cross_grad_index[j]]], cross_grad_weights[j]);
						  }

						  // Project if needed
						  if (projection_rule == 0) { // Pegasos projection rule
							double norm2 = Wx.snorm();
							if (norm2 > 1.0/lambda_x) {
								Wx.scale(sqrt(1.0/(lambda_x*norm2)));
							}
						  } else if (projection_rule == 1) { // other projection
							double norm2 = Wx.snorm();
							if (norm2 > (projection_constant*projection_constant)) {
								Wx.scale(projection_constant/sqrt(norm2));
							}
						  } // else -- no projection
						}// ---------------- End of Inner Pegasos Loop -------------------

						  // Calculate objective value
						  startTime = get_runtime();
						  norm_value = Wx.snorm();
						  obj_wx = norm_value * lambda_x / 2.0;
						  //loss_wx = 0.0;
						  //zero_one_error = 0.0;
						  for (uint i=0; i < Datasetx.size(); ++i) {
							double cur_loss = 1 - fabs(Wx * Datasetx[i]);
							if (cur_loss < 0.0) cur_loss = 0.0;
							//loss_value += cur_loss/num_examples;
							obj_wx += cur_loss/num_examples_x;
							//if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
						  }
						  for (uint i=0; i < num_pairs_xy; ++i) {
							  double d_i = Wx*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
							  double tau_i = 0;
							  if (fabs(d_i) <= scisd_lambda)
									{tau_i = -0.5*d_i*d_i + 0.5*scisd_a*scisd_lambda*scisd_lambda;}
							  else if (fabs(d_i) <= scisd_a*scisd_lambda) {
								  tau_i = (d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
							  }
							  obj_wx += 0.5*pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i+(1-pairs_label[i])*tau_i)/num_pairs_xy;
						  }
						  endTime = get_runtime();
						  calc_obj_time = endTime - startTime;

						  // check convergence
						  if (fabs(obj_wx - obj_wx_old) < 0.0001) {
							  converge_wx = true;
						  } else {
							  obj_wx_old = obj_wx;
						  }
				  }// end of while for updating wx

				  // for updating wy
				  double obj_wy = 0.0;
				  double obj_wy_old = 1000000;
				  converge_wy = false;
				  while (converge_wy == false) {
					  // CCCP iterations
					  Wy_curr = Wy;// for current CCCP iteration
					  // ---------------- Inner Pegasos Loop -------------------
					  for (int i = 0; i < max_iter_y; ++i) {

						  // learning rate
						  double eta;
						  if (eta_rule_type == 0) { // Pegasos eta rule
							eta = 1 / (lambda_y * (i+2));
						  } else if (eta_rule_type == 1) { // Norma rule
							eta = eta_constant / sqrt(i+2);
							// solve numerical problems
							//if (projection_rule != 2)
							Wy.make_my_a_one();
						  } else {
							eta = eta_constant;
						  }

						  // gradient indices and losses
						  std::vector<uint> grad_index;
						  std::vector<double> grad_weights;
						  // gradient indices and losses for cross
						  std::vector<uint> cross_grad_index;
						  std::vector<double> cross_grad_weights;

						  // calc sub-gradients
						  for (int j=0; j < exam_per_iter; ++j) {

							// choose random example
							uint r = ((int)rand()) % num_examples_y;

							// calculate prediction
							double prediction = Wy*Datasety[r];
							double Labelr = (prediction >=0)?+1.0:-1.0;

							// calculate loss
							double cur_loss = 1 - Labelr*prediction;
							if (cur_loss < 0.0) cur_loss = 0.0;

							// and add to the gradient
							if (cur_loss > 0.0) {
								grad_index.push_back(r);
								grad_weights.push_back(-eta*Labelr/exam_per_iter);
							}
						  }

						  // calc sub-gradients2 for cross-modal similarity
						  for (uint j=0; j < num_pairs_xy; ++j) {

							// calculate prediction
							double d_curr = Wx*Datasetx[pairs_idx_x[j]] - Wy_curr*Datasety[pairs_idx_y[j]];
							double d_i = Wx*Datasetx[pairs_idx_x[j]] - Wy*Datasety[pairs_idx_y[j]];
							double gtau_one_i = 0;
							if (fabs(d_i) >= scisd_a*scisd_lambda)
									{gtau_one_i = d_i;}
							else if (fabs(d_i) >= scisd_lambda) {
								if (d_i >= 0) {
									gtau_one_i = (scisd_a*d_i-scisd_a*scisd_lambda)/(scisd_a-1);
								}
								else {
									gtau_one_i = (scisd_a*d_i-scisd_a*scisd_lambda)/(scisd_a-1);
								}
							}

							cross_grad_index.push_back(j);
							cross_grad_weights.push_back(-0.5*pweights[j]*gamma_cross*(2*pairs_label[j]*d_i + (1-pairs_label[j])*(gtau_one_i-d_curr))/num_pairs_xy);
						  }

						  // scale w
						  Wy.scale(1.0 - eta*lambda_y);

						  // and add sub-gradients
						  for (uint j=0; j<grad_index.size(); ++j) {
							Wy.add(Datasety[grad_index[j]], grad_weights[j]);
						  }

						  // and add sub-gradients 2 for cross-modal similarity
						  for (uint j=0; j<cross_grad_index.size(); ++j) {
							Wy.add(Datasety[pairs_idx_y[cross_grad_index[j]]], cross_grad_weights[j]);
						  }

						  // Project if needed
						  if (projection_rule == 0) { // Pegasos projection rule
							double norm2 = Wy.snorm();
							if (norm2 > 1.0/lambda_y) {
								Wy.scale(sqrt(1.0/(lambda_y*norm2)));
							}
						  } else if (projection_rule == 1) { // other projection
							double norm2 = Wy.snorm();
							if (norm2 > (projection_constant*projection_constant)) {
								Wy.scale(projection_constant/sqrt(norm2));
							}
						  } // else -- no projection
						}// ---------------- End of Inner Pegasos Loop -------------------

						  // Calculate objective value
						  startTime = get_runtime();
						  norm_value = Wy.snorm();
						  obj_wy = norm_value * lambda_y / 2.0;
						  //loss_wx = 0.0;
						  //zero_one_error = 0.0;
						  for (uint i=0; i < Datasety.size(); ++i) {
							double cur_loss = 1 - fabs(Wy * Datasety[i]);
							if (cur_loss < 0.0) cur_loss = 0.0;
							//loss_value += cur_loss/num_examples;
							obj_wy += cur_loss/num_examples_y;
							//if (cur_loss >= 1.0) zero_one_error += 1.0/num_examples;
						  }
						  for (uint i=0; i < num_pairs_xy; ++i) {
							  double d_i = Wx*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
							  double tau_i = 0;
							  if (fabs(d_i) <= scisd_lambda)
									{tau_i = -0.5*d_i*d_i + 0.5*scisd_a*scisd_lambda*scisd_lambda;}
							  else if (fabs(d_i) <= scisd_a*scisd_lambda) {
								  tau_i = (d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
							  }
							  obj_wy += 0.5*pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i+(1-pairs_label[i])*tau_i)/num_pairs_xy;
						  }
						  endTime = get_runtime();
						  calc_obj_time = endTime - startTime;

						  // check convergence
						  if (fabs(obj_wy - obj_wy_old) < 0.0001) {
							  converge_wx = true;
						  } else {
							  obj_wy_old = obj_wy;
						  }
				  }// end of while for updating wy

				  // here we check the convergence after updating wx and wy
				  // Calculate objective value
				  startTime = get_runtime();
				  norm_value = Wx.snorm();
				  obj_value = norm_value * lambda_x / 2.0;
				  norm_value = Wy.snorm();
				  obj_value += norm_value * lambda_y / 2.0;

				  for (uint i=0; i < Datasetx.size(); ++i) {
					double cur_loss = 1 - fabs(Wx * Datasetx[i]);
					if (cur_loss < 0.0) cur_loss = 0.0;
					obj_value += cur_loss/num_examples_x;
				  }
				  for (uint i=0; i < Datasety.size(); ++i) {
					double cur_loss = 1 - fabs(Wy * Datasety[i]);
					if (cur_loss < 0.0) cur_loss = 0.0;
					obj_value += cur_loss/num_examples_y;
				  }
				  for (uint i=0; i < num_pairs_xy; ++i) {
					  double d_i = Wx*Datasetx[pairs_idx_x[i]] - Wy*Datasety[pairs_idx_y[i]];
					  double tau_i = 0;
					  if (fabs(d_i) <= scisd_lambda)
							{tau_i = -0.5*d_i*d_i + 0.5*scisd_a*scisd_lambda*scisd_lambda;}
					  else if (fabs(d_i) <= scisd_a*scisd_lambda) {
						  tau_i = (d_i*d_i - 2*scisd_a*scisd_lambda*fabs(d_i) + scisd_a*scisd_a*scisd_lambda*scisd_lambda)/(2*scisd_a-2);
					  }
					  obj_value += 0.5*pweights[i]*gamma_cross*(pairs_label[i]*d_i*d_i+(1-pairs_label[i])*tau_i)/num_pairs_xy;
				  }
				  endTime = get_runtime();
				  calc_obj_time = endTime - startTime;

				  // check convergence
				  if (fabs(obj_value - obj_value_old) < 0.0001) {
					  converge_wx = true;
				  } else {
					  obj_value_old = obj_value;
				  }
			  }

			  // now wx and wy for k-th bit are printed out in files
				char buffer_name [50];
				if (model_filename != "noModelFile") {
					sprintf(buffer_name, "%s_x_%d.model", model_filename.c_str(), k);
					std::ofstream model_filex(buffer_name);
					if (!model_filex.good()) {
						std::cerr << "error w/ " << model_filename << std::endl;
						exit(EXIT_FAILURE);
					}
					Wx.print(model_filex);
					model_filex.close();

					sprintf(buffer_name, "%s_y_%d.model", model_filename.c_str(), k);
					std::ofstream model_filey(buffer_name);
					if (!model_filey.good()) {
						std::cerr << "error w/ " << model_filename << std::endl;
						exit(EXIT_FAILURE);
					}
					Wy.print(model_filey);
					model_filey.close();
				  }
				//}
				// now we update the pair weights
				double boostloss = 0.0;
				double* plabels = new double [num_pairs_xy];
				int xlabel, ylabel;
				for (uint i=0; i < num_pairs_xy; ++i) {
					if ((Wx*Datasetx[pairs_idx_x[i]])>=0) {
						xlabel = 1;
					} else {
						xlabel = -1;
					}
					if ((Wy*Datasety[pairs_idx_y[i]])>=0) {
						ylabel = 1;
					} else {
						ylabel = -1;
					}
					if (( xlabel * ylabel) >0) {
						plabels[i] = 1;
					} else {
						plabels[i] = 0;
					}
					if (plabels[i] == pairs_label[i]) {
						boostloss += pweights[i];
					}
				  }
				double decayrate = boostloss / (1-boostloss);
				double totalweights = 0.0;
				for (uint i=0; i < num_pairs_xy; ++i) {
					if (plabels[i] == pairs_label[i]) {
						pweights[i] = pweights[i] * decayrate;
					}
					totalweights += pweights[i];
				}
				// normalize weights by sum
				for (uint i=0; i < num_pairs_xy; ++i) {
					pweights[i] /= totalweights;
				}
		  }// end for one bit
		   //*/
}

// ------------------------------------------------------------//
// ---------------- READING DATA ------------------------------//
// ------------------------------------------------------------//
void ReadData(// input
	      std::string& data_filename,
	      // output
	      std::vector<simple_sparse_vector> & Dataset,
	      std::vector<int> & Labels,
	      uint& dimension,
	      long& readingTime) {
  
  dimension = 0;

  // Start a timer
  long startTime = get_runtime();

  // OPEN DATA FILE
  // =========================
  std::ifstream data_file(data_filename.c_str());
  if (!data_file.good()) {
    std::cerr << "error w/ " << data_filename << std::endl;
    exit(EXIT_FAILURE);
  }

  
  // Read SVM-Light data file
  // ========================
  int num_examples = 0;
  std::string buf;
  while (getline(data_file,buf)) {
    // ignore lines which begin with #
    if (buf[0] == '#') continue;
    // Erase what comes after #
    size_t pos = buf.find('#');
    if (pos < buf.size()) {
      buf.erase(pos);
    }
    // replace ':' with white space
    int n=0;
    for (size_t pos=0; pos < buf.size(); ++pos)
      if (buf[pos] == ':') {
	n++; buf[pos] = ' ';
      }
    // read from the string
    std::istringstream is(buf);
    int label = 0;
    is >> label;
    if (label != 1 && label != -1 && label != 0) {
      std::cerr << "Error reading SVM-light format. Abort." << std::endl;
      exit(EXIT_FAILURE);
    }
    Labels.push_back(label);
    simple_sparse_vector instance(is,n);
    Dataset.push_back(instance);
    num_examples++;
    uint cur_max_ind = instance.max_index() + 1;
    if (cur_max_ind > dimension) dimension = cur_max_ind;
  }

  data_file.close();


#ifdef nodef
  std::cerr << "num_examples = " << num_examples 
	    << " dimension = " << dimension
	    << " Dataset.size = " << Dataset.size() 
	    << " Labels.size = " << Labels.size() << std::endl;
#endif
    
  
  // update timeline
  readingTime = get_runtime() - startTime;
  
}

// ------------------------------------------------------------//
// ----------- READING CROSS-MODAL DATA -----------------------//
// ------------------------------------------------------------//
void ReadCrossData(// input
	      std::string& data_filename,
	      // output
	      std::vector<uint> & pairs_idx_x,
	      std::vector<uint> & pairs_idx_y,
	      std::vector<double> & pairs_label,
	      long& readingTime) {

  // dimension = 0;

  // Start a timer
  long startTime = get_runtime();

  // OPEN DATA FILE
  // =========================
  std::ifstream data_file(data_filename.c_str());
  if (!data_file.good()) {
    std::cerr << "error w/ " << data_filename << std::endl;
    exit(EXIT_FAILURE);
  }


  // Read sparse format data file
  // ========================
  // int num_examples = 0;
  std::string buf;
  while (getline(data_file,buf)) {
    // ignore lines which begin with #
    if (buf[0] == '#') continue;
    // Erase what comes after #
    size_t pos = buf.find('#');
    if (pos < buf.size()) {
      buf.erase(pos);
    }
//    // replace ':' with white space
//    int n=0;
//    for (size_t pos=0; pos < buf.size(); ++pos)
//      if (buf[pos] == ':') {
//	n++; buf[pos] = ' ';
//      }
    // read from the string
    std::istringstream is(buf);
    uint idtemp = 0;
    is >> idtemp;
    if (idtemp < 1 ) {
      std::cerr << "Error reading sparse format. Abort." << std::endl;
      exit(EXIT_FAILURE);
    }
    pairs_idx_x.push_back(idtemp-1);

    is >> idtemp;
	if (idtemp < 1 ) {
	  std::cerr << "Error reading sparse format. Abort." << std::endl;
	  exit(EXIT_FAILURE);
	}
	pairs_idx_y.push_back(idtemp-1);

	double label = 0.0;
	is >> label;
	if (label > 0 ) {
		pairs_label.push_back(label);
	} else if (label < 0) {
		pairs_label.push_back(0);
	} else {
	  std::cerr << "Error reading sparse format. Abort." << std::endl;
	  exit(EXIT_FAILURE);
	}
	//pairs_label.push_back(label);
  }

  data_file.close();

//#ifdef nodef
//  std::cerr << "num_examples = " << num_examples
//	    << " dimension = " << dimension
//	    << " Dataset.size = " << Dataset.size()
//	    << " Labels.size = " << Labels.size() << std::endl;
//#endif

  // update timeline
  readingTime = get_runtime() - startTime;

}



// -------------------------------------------------------------//
// ---------------------- Experiments mode ---------------------//
// -------------------------------------------------------------//

  class ExperimentStruct {
  public:
    ExperimentStruct() { }
    void Load(std::istringstream& is) {
      is >> lambda >> max_iter >> exam_per_iter >> num_iter_to_avg
	 >> eta_rule >> eta_constant >> projection_rule 
	 >> projection_constant;
    }
    void Print() {
      std::cout << lambda << "\t\t" << max_iter << "\t\t" << exam_per_iter << "\t\t" 
		<< num_iter_to_avg << "\t\t" << eta_rule << "\t\t" << eta_constant 
		<< "\t\t" << projection_rule << "\t\t" << projection_constant << "\t\t";
    }
    void PrintHead() {
      std::cerr << "lambda\t\tT\t\tk\t\tnumValid\t\te_r\t\te_c\t\tp_r\t\tp_c\t\t";
    }
    double lambda, eta_constant, projection_constant;
    uint max_iter,exam_per_iter,num_iter_to_avg;
    int eta_rule,projection_rule;
  };

  class ResultStruct {
  public:
    ResultStruct() : trainTime(0), calc_obj_time(0),
		     norm_value(0.0),loss_value(0.0),
		     zero_one_error(0.0),obj_value(0.0),
		     test_loss(0.0), test_error(0.0) { }
    void Print() {
      std::cout << trainTime << "\t\t" 
		<< calc_obj_time << "\t\t" 
		<< norm_value  << "\t\t" 
		<< loss_value << "\t\t"  
		<< zero_one_error  << "\t\t" 
		<< obj_value << "\t\t"
		<< test_loss << "\t\t"
		<< test_error << "\t\t";

    }
    void PrintHead() {
      std::cerr << "tTime\t\tcoTime\t\t||w||\t\tL\t\tL0-1\t\tobj_value\t\ttest_L\t\ttestL0-1\t\t";
    }

    long trainTime, calc_obj_time;
    double norm_value,loss_value,zero_one_error,obj_value,test_loss,test_error;
  };


void run_experiments(std::string& experiments_filename,
		     std::vector<simple_sparse_vector>& Dataset,
		     std::vector<int>& Labels,
		     uint dimension,
		     std::vector<simple_sparse_vector>& testDataset,
		     std::vector<int>& testLabels) {

  // open the experiments file
  std::ifstream exp_file(experiments_filename.c_str());
  if (!exp_file.good()) {
    std::cerr << "error w/ " << experiments_filename << std::endl;
    exit(EXIT_FAILURE);
  }

  // read the entire experiment specification

  
  uint num_experiments = 0;
  std::vector<ExperimentStruct> v;
  std::string buf;
  while (getline(exp_file,buf)) {
    // read from the string
    std::istringstream is(buf);
    ExperimentStruct tmp; tmp.Load(is);
    v.push_back(tmp);
    num_experiments++;
  }
  exp_file.close();

  // run all the experiments
  std::vector<ResultStruct> res(num_experiments);
  std::string lala = "noModelFile";
  for (uint i=0; i<num_experiments; ++i) {
    LearnReturnLast(Dataset,Labels,dimension,testDataset,testLabels,
		     v[i].lambda,v[i].max_iter,v[i].exam_per_iter,
		     //v[i].num_iter_to_avg,
		     lala,
		     res[i].trainTime,res[i].calc_obj_time,res[i].obj_value,
		     res[i].norm_value,
		     res[i].loss_value,res[i].zero_one_error,
		     res[i].test_loss,res[i].test_error,
		     v[i].eta_rule,v[i].eta_constant,
		     v[i].projection_rule,v[i].projection_constant);
  }

  // print results
  v[0].PrintHead(); res[0].PrintHead(); 
  std::cout << std::endl;
  for (uint i=0; i<num_experiments; ++i) {
    v[i].Print();
    res[i].Print();
    std::cout << std::endl;
  }

  std::cerr << std::endl;

}
