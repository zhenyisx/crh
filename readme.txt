The whole program can be divied into TWO parts.

1 The model learning part. All the related files are in the "pegasos" directory. We adapted the code of pegasos and learn the projection vectors (a.k.a. the models).

2 The model evaluation part. All the related files are in the "datatool" directory. We convert the images into hash codes and perform evaluation. 

