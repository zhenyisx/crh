function make_data_flickr


addpath utils;

nclass = 10;
indatafile = sprintf('../../MH_TMM11/Python-1/HashExp/data/nus/NUS_TT%d',nclass);


load(indatafile);

% mean-center the features
x_mean = mean(feax_tr,2);
y_mean = mean(feay_tr,2);
feax_tr = double(bsxfun(@minus, feax_tr, x_mean)); %feax_tr = feax_tr-repmat(x_mean,1,Ntrain);
% feax_te = double(bsxfun(@minus, feax_te, x_mean)); %feax_te = feax_te-repmat(x_mean,1,Ntest);
feay_tr = double(bsxfun(@minus, feay_tr, y_mean)); %feay_tr = feay_tr-repmat(y_mean,1,Ntrain);
% feay_te = double(bsxfun(@minus, feay_te, y_mean)); %feay_te = feay_te-repmat(y_mean,1,Ntest);



trsize = [300 500 1000 1500 2000];
obratio = 0.001;

for r = 1:10
    rdids = randperm(size(feax_tr,2));
    for i=1:length(trsize)
        itrsize = trsize(i);
        trids = rdids(1:itrsize);
        sfeax = feax_tr(:,trids);
        sfeay = feay_tr(:,trids);
        sSXY = (gnd_tr(:,trids)'*gnd_tr(:,trids))>0;
        sSXY = 2*sSXY-1;

        sOXY = getobservations(itrsize, itrsize, obratio);

        % save data for CVH and CMSSH use
        save(sprintf('../../MH_TMM11/Python-1/HashExp/data/nus/NUS_TT%d_TR%d_rep%d',nclass, itrsize, r),'sfeax','sfeay','sSXY','sOXY');

        % save data for CRH use
        outdatafile_x = sprintf('../pegasos/data/NUS_TT%d_TR%d_feax_rep%d.dat',nclass, itrsize, r);
        outdatafile_y = sprintf('../pegasos/data/NUS_TT%d_TR%d_feay_rep%d.dat',nclass, itrsize, r);
        outdatafile_cross = sprintf('../pegasos/data/NUS_TT%d_TR%d_crosssim_rep%d.dat',nclass, itrsize, r);
        
        label_vector = zeros(itrsize,1);
        libsvmwrite(outdatafile_x, label_vector, sparse(sfeax'));
        libsvmwrite(outdatafile_y, label_vector, sparse(sfeay'));
        sparseWrite(sparse(sSXY.*sOXY),outdatafile_cross);
    end
end