function [code,T] = compressSMH(data, W, threshname, T)
% input:
%   data : data matrix (Dim x N), each point is a column
%   W : projection matrix [Dim x Codelength]
%   threshname : name of thresholding method
%   T : threshould vector [Dim x 1]
% output:
%   code : binary code matrix, N x M (compacted in 8 bits words), each
%   point is a column

U = W'*data; % codelength x N, each point is a column
if nargin < 4
    % find the optimal thresholds vectors for X and Y
    if strcmp(threshname,'zero')
        T = 0;
    elseif strcmp(threshname,'mean')
        T = mean(W'*data,2);
    elseif strcmp(threshname,'median')
        T = median(W'*data,2);
    elseif strcmp(threshname,'quater-mean')
        quatervec = range(U,2)/4;
        minvec = min(U,[],2);
        maxvec = max(U,[],2);
        T.low = double(bsxfun(@plus,minvec, quatervec));
        T.high = double(bsxfun(@minus,maxvec, quatervec));
    elseif strcmp(threshname,'quater-median')
        Us = sort(U,2,'ascend');
        T.low = double(Us(:,floor(size(data,2)/4)));
        T.high = double(Us(:,floor(size(data,2)*3/4)));
    elseif strcmp(threshname,'learn')
        nBin = 20;
        kappa = [1,1,1];
        T = zeros(size(U,1),1);
        for m = 1:size(U,1)
            [nP, cP] = hist(U(m,:),nBin);
            leftnP = zeros(nBin,1);
            rightnP = zeros(nBin,1);
            for npos = 1:nBin
                leftnP(npos) = sum(nP(1:npos-1));
                rightnP(npos) = sum(nP(npos+1:end));
            end
            costP = kappa(1)*leftnP.^2+kappa(2)*rightnP.^2+kappa(3)*nP'*size(U,2);
            [~, i] = min(costP);
            T(m) = cP(i);
        end
    end
end


if strcmp(threshname,'quater-median') || strcmp(threshname,'quater-mean') 
    Ulow = bsxfun(@lt,U,T.low);
    Uhigh = bsxfun(@gt,U,T.high);
    U1 = double(Ulow | Uhigh);
    U2 = double(bsxfun(@gt,U,0));
    r = floor(size(W,2)/2);
    U = [U1(1:r,:);U2(1:r,:)];
elseif strcmp(threshname,'learn')
    U1 = double(bsxfun(@gt,U,0));
    U2 = double(bsxfun(@gt,U,T));
    r = floor(size(W,2)/2);
    U = [U1(1:r,:);U2(1:r,:)];
%     U = U2;
else
    % threshold each column of U by vector T
    U = double(bsxfun(@gt,U,T));
    % U = double(U>0);
end

code = compactbit(U);
