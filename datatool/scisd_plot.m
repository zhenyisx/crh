function scisd_plot


% avalues = [1,3.7,10];

% for a = avalues
    
fvalues = [];
f1values = [];
f2values = [];
xvalues = [-4:0.1:4];
for i=xvalues
    a = 1; fvalues(:,end+1) = scisd(a,1/a,i);
    a = 3.7; f1values(:,end+1) = scisd(a,1/a,i);
    a = 10; f2values(:,end+1) = scisd(a,1/a,i);
end
    
% end
figure;hold on;ylim([0,0.8]);
plot(xvalues, fvalues, 'b-', 'LineWidth',3);
plot(xvalues, f1values, 'r:', 'LineWidth',3);
plot(xvalues, f2values, 'k-.', 'LineWidth',3);
legend('a=1,\lambda=1/a','a=3.7,\lambda=1/a', 'a=10,\lambda=1/a');
xlabel('d');h = get(gca, 'xlabel');set(h, 'FontSize', 20);
ylabel('\tau');h = get(gca, 'ylabel');set(h, 'FontSize', 20);


fvalues = [];
f1values = [];
f2values = [];
xvalues = [-4:0.1:4];
for i=xvalues
    a = 1; fvalues(:,end+1) = scisd_one(a,1/a,i);
    a = 3.7; f1values(:,end+1) = scisd_one(a,1/a,i);
    a = 10; f2values(:,end+1) = scisd_one(a,1/a,i);
end
    
% end
figure;hold on;%ylim([0,0.8]);
plot(xvalues, fvalues, 'b-', 'LineWidth',3);
plot(xvalues, f1values, 'r:', 'LineWidth',3);
plot(xvalues, f2values, 'k-.', 'LineWidth',3);
legend('a=1,\lambda=1/a','a=3.7,\lambda=1/a', 'a=10,\lambda=1/a');
xlabel('d');h = get(gca, 'xlabel');set(h, 'FontSize', 20);
ylabel('\tau_1');h = get(gca, 'ylabel');set(h, 'FontSize', 20);

fvalues = [];
f1values = [];
f2values = [];
xvalues = [-4:0.1:4];
for i=xvalues
    a = 1; fvalues(:,end+1) = scisd_two(a,1/a,i);
    a = 3.7; f1values(:,end+1) = scisd_two(a,1/a,i);
    a = 10; f2values(:,end+1) = scisd_two(a,1/a,i);
end
    
% end
figure;hold on;%ylim([0,0.8]);
plot(xvalues, fvalues, 'b-', 'LineWidth',3);
plot(xvalues, f1values, 'r:', 'LineWidth',3);
plot(xvalues, f2values, 'k-.', 'LineWidth',3);
legend('a=1,\lambda=1/a','a=3.7,\lambda=1/a', 'a=10,\lambda=1/a');
xlabel('d');h = get(gca, 'xlabel');set(h, 'FontSize', 20);
ylabel('\tau_2');h = get(gca, 'ylabel');set(h, 'FontSize', 20);


end

function [f] = scisd(a,lambda,x)
    f= scisd_one(a,lambda,x) - scisd_two(a,lambda,x);
end

function [f] = scisd_one(a,lambda,x)
    
    if abs(x)<= lambda
        f = 0;
    elseif abs(x) <= a*lambda
        f = (a*x^2-2*a*lambda*abs(x)+a*lambda^2)/(2*a-2);
    else
        f = 0.5*x^2-0.5*a*lambda^2;
    end
end

function [f] = scisd_two(a, lambda,x)
    f = 0.5*x^2-0.5*a*lambda^2;
end