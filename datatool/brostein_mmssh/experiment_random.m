randn('seed', 0);
rand('seed', 0);



if 1
    
Labels = repmat([1:40],[200 1]); 
Labels = Labels(:);

I = generate_random_set(Labels,2,8,10,128);    
T = generate_random_set(Labels,2,7,15,80);    

I_tr = I(:,1:2:end);
I_te = I(:,2:2:end);

T_tr = T(:,1:2:end);
T_te = T(:,2:2:end);

Labels_tr = Labels(1:2:end);
Labels_te = Labels(2:2:end);



[Labels_te,i] = sort(Labels_te);
I_te = I_te(:,i);
T_te = T_te(:,i);

pos_te = generate_pairs(Labels_te,Inf,1);
pos_tr = generate_pairs(Labels_tr,100,1);

neg_te = generate_negatives(Labels_te,Inf,1);
neg_tr = generate_negatives(Labels_tr,100,1);

% scramble
pos_te = pos_te(randperm(size(pos_te,1)),:);
pos_tr = pos_tr(randperm(size(pos_tr,1)),:);
neg_te = neg_te(randperm(size(neg_te,1)),:);
neg_tr = neg_tr(randperm(size(neg_tr,1)),:);

% mask
MASK = zeros(length(Labels_te)) - 1;
ul = unique(Labels_te);
for k = 1:length(ul)
    ll = find(Labels_te==ul(k));
    MASK(ll,ll)=1;
end

end


return

%X1 = normalize(X1,2,1);
%X2 = normalize(X2,2,1);


% Training settings (equal for all algorithms)
M = 32;
%K = [1000 15 50];
K = [1000 15 50];
Np_train = 1e4; %1e4;
Nn_train = 1e5; %2e5;
Np_test  = 5e3; %1e4;
Nn_test  = 5e4; %2e5;
alpha = 0.2;


% Similarity on X
Xp_train = I_tr(:,pos_tr(1:Np_train,1));
Yp_train = I_tr(:,pos_tr(1:Np_train,2));
Xp_test  = I_te(:,pos_te(1:Np_test,1));
Yp_test  = I_te(:,pos_te(1:Np_test,2));
Xn_train = I_tr(:,neg_tr(1:Nn_train,1));
Yn_train = I_tr(:,neg_tr(1:Nn_train,2));
Xn_test  = I_te(:,neg_te(1:Nn_test,1));
Yn_test  = I_te(:,neg_te(1:Nn_test,2));

rand('seed',0);
randn('seed',0);
[P1, v] = run_adaboost(Xp_train,Yp_train,Xn_train,Yn_train, Xp_test,Yp_test,Xn_test,Yn_test, K,M,alpha);


% Similarity on Y
Xp_train = T_tr(:,pos_tr(1:Np_train,1));
Yp_train = T_tr(:,pos_tr(1:Np_train,2));
Xp_test  = T_te(:,pos_te(1:Np_test,1));
Yp_test  = T_te(:,pos_te(1:Np_test,2));
Xn_train = T_tr(:,neg_tr(1:Nn_train,1));
Yn_train = T_tr(:,neg_tr(1:Nn_train,2));
Xn_test  = T_te(:,neg_te(1:Nn_test,1));
Yn_test  = T_te(:,neg_te(1:Nn_test,2));

rand('seed',0);
randn('seed',0);
[P2, v] = run_adaboost(Xp_train,Yp_train,Xn_train,Yn_train, Xp_test,Yp_test,Xn_test,Yn_test, K,M,alpha);


% Similarity on X-Y
Xp_train = I_tr(:,pos_tr(1:Np_train,1));
Yp_train = T_tr(:,pos_tr(1:Np_train,2));
Xp_test  = I_te(:,pos_te(1:Np_test,1));
Yp_test  = T_te(:,pos_te(1:Np_test,2));
Xn_train = I_tr(:,neg_tr(1:Nn_train,1));
Yn_train = T_tr(:,neg_tr(1:Nn_train,2));
Xn_test  = I_te(:,neg_te(1:Nn_test,1));
Yn_test  = T_te(:,neg_te(1:Nn_test,2));

rand('seed',0);
randn('seed',0);    
[PX,PY,v,history] = run_adaboost_cross(Xp_train,Yp_train,Xn_train,Yn_train, Xp_test,Yp_test,Xn_test,Yn_test, K(2:end),M,alpha);





%m = M;
%[m calculate_perf(X1,X1, P1(1:m,:),P1(1:m,:), shapeid, strength, MASK)]
%[m calculate_perf(X2,X2, P2(1:m,:),P2(1:m,:), shapeid, strength, MASK)]
%[m calculate_perf(X2,X1, PY(1:m,:),PX(1:m,:), shapeid, strength, MASK)]
%[0 calculate_perf(X2,X2, [],[], shapeid, strength, MASK)]
%[0 calculate_perf(X1,X1, [],[], shapeid, strength, MASK)]


% Performance tests
% bits, map, eer, fpr1, fpr01
PERFX  = [];
PERFY  = [];
PERFXY = [];
for m=1:M,
    m
    PERFX(end+1,:)  = [m calculate_perf(I_te,I_te, P1(1:m,:),P1(1:m,:), Labels_te, zeros(size(Labels_te)), MASK)];
    PERFY(end+1,:)  = [m calculate_perf(T_te,T_te, P2(1:m,:),P2(1:m,:), Labels_te, zeros(size(Labels_te)), MASK)];
    PERFXY(end+1,:) = [m calculate_perf(I_te,T_te, PX(1:m,:),PY(1:m,:), Labels_te, zeros(size(Labels_te)), MASK)];
end
PERFX(end+1,:) = [0 calculate_perf(I_te,I_te, [],[], Labels_te, zeros(size(Labels_te)), MASK)];
PERFY(end+1,:) = [0 calculate_perf(T_te,T_te, [],[], Labels_te, zeros(size(Labels_te)), MASK)];



h = plot([1 M], [1 1]*PERFX(end,2), ':r', 1:M,PERFX(1:end-1,2), 'r',  ...
     [1 M], [1 1]*PERFY(end,2), ':b', 1:M,PERFY(1:end-1,2), 'b', ...
     1:M,PERFXY(:,2), 'g' );
set(h(end),'LineWidth', 1.5); 
axis([1 M 0 1.01]);
xlabel('Number of bits (n)');
ylabel('Mean average precision (mAP)');
%1:M, PERFY(:,1),PERFY(:,2), PERFX(:,1),PERFX(:,2))
legend('Images (Euclidean)', 'Images (Optimal)', 'Text (Euclidean)', 'Text (Optimal)', 'Cross-modality', 'Location', 'SouthEast');
%set(gca,'YTickLabel',['0.50';'0.55';'0.60';'0.65';'0.70';'0.75';'0.80';'0.85';'0.90';'0.95';'1.00']);

