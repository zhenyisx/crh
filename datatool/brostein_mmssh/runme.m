load dataset_random
% data contains:
% X_tr: training data X, each col is a point
% Y_tr: training data Y, each col is a point
% X_te:
% Y_te:
% pos_tr: 100,000 candididate training positive pairs, each row is a pair index
% pos_te: 100,000 candididate test positive pairs, each row is a pair index
% neg_tr: 211,212 candididate training negative pairs, each row is a pair index
% neg_te: 210,862 candididate test negative pairs, each row is a pair index
% MASK:
% Labels_tr:
% Labels_te:

% Training settings (equal for all algorithms)
M = 32;         % Hamming space dimensionality
K = [1000 15 50];
Np_train = 1e4; % # of positive training pairs
Nn_train = 1e5; % # of negative training pairs
Np_test  = 5e3; 
Nn_test  = 5e4; 
alpha = 0.2;


% % Similarity on X
% X1p_train = X_tr(:,pos_tr(1:Np_train,1));
% X2p_train = X_tr(:,pos_tr(1:Np_train,2));
% X1p_test  = X_te(:,pos_te(1:Np_test,1));
% X2p_test  = X_te(:,pos_te(1:Np_test,2));
% X1n_train = X_tr(:,neg_tr(1:Nn_train,1));
% X2n_train = X_tr(:,neg_tr(1:Nn_train,2));
% X1n_test  = X_te(:,neg_te(1:Nn_test,1));
% X2n_test  = X_te(:,neg_te(1:Nn_test,2));
% 
% rand('seed',0);
% randn('seed',0);
% [P1, v] = run_adaboost(X1p_train,X2p_train,X1n_train,X2n_train, X1p_test,X2p_test,X1n_test,X2n_test, K,M,alpha);
% 
% 
% % Similarity on Y
% Y1p_train = Y_tr(:,pos_tr(1:Np_train,1));
% Y2p_train = Y_tr(:,pos_tr(1:Np_train,2));
% Y1p_test  = Y_te(:,pos_te(1:Np_test,1));
% Y2p_test  = Y_te(:,pos_te(1:Np_test,2));
% Y1n_train = Y_tr(:,neg_tr(1:Nn_train,1));
% Y2n_train = Y_tr(:,neg_tr(1:Nn_train,2));
% Y1n_test  = Y_te(:,neg_te(1:Nn_test,1));
% Y2n_test  = Y_te(:,neg_te(1:Nn_test,2));
% 
% rand('seed',0);
% randn('seed',0);
% [P2, v] = run_adaboost(Y1p_train,Y2p_train,Y1n_train,Y2n_train, Y1p_test,Y2p_test,Y1n_test,Y2n_test, K,M,alpha);


% Similarity on X-Y
X1p_train = X_tr(:,pos_tr(1:Np_train,1));
Y2p_train = Y_tr(:,pos_tr(1:Np_train,2));
X1p_test  = X_te(:,pos_te(1:Np_test,1));
Y2p_test  = Y_te(:,pos_te(1:Np_test,2));
X1n_train = X_tr(:,neg_tr(1:Nn_train,1));
Y2n_train = Y_tr(:,neg_tr(1:Nn_train,2));
X1n_test  = X_te(:,neg_te(1:Nn_test,1));
Y2n_test  = Y_te(:,neg_te(1:Nn_test,2));

rand('seed',0);
randn('seed',0);    
[PX,PY,v,history] = run_adaboost_cross(X1p_train,Y2p_train,X1n_train,Y2n_train, X1p_test,Y2p_test,X1n_test,Y2n_test, K(2:end),M,alpha);





% Performance tests
% bits, map, eer, fpr1, fpr01
PERFX  = [];
PERFY  = [];
PERFXY = [];
for m=1:M,
    %m
    PERFX(end+1,:)  = [m calculate_perf(X_te,X_te, P1(1:m,:),P1(1:m,:), Labels_te, zeros(size(Labels_te)), MASK)];
    PERFY(end+1,:)  = [m calculate_perf(Y_te,Y_te, P2(1:m,:),P2(1:m,:), Labels_te, zeros(size(Labels_te)), MASK)];
    PERFXY(end+1,:) = [m calculate_perf(X_te,Y_te, PX(1:m,:),PY(1:m,:), Labels_te, zeros(size(Labels_te)), MASK)];
end
PERFX(end+1,:) = [0 calculate_perf(X_te,X_te, [],[], Labels_te, zeros(size(Labels_te)), MASK)];
PERFY(end+1,:) = [0 calculate_perf(Y_te,Y_te, [],[], Labels_te, zeros(size(Labels_te)), MASK)];



h = plot([1 M], [1 1]*PERFX(end,2), ':r', 1:M,PERFX(1:end-1,2), 'r',  ...
     [1 M], [1 1]*PERFY(end,2), ':b', 1:M,PERFY(1:end-1,2), 'b', ...
     1:M,PERFXY(:,2), 'g' );
set(h(end),'LineWidth', 1.5); 
axis([1 M 0 1]);
xlabel('Number of bits (n)');
ylabel('Mean average precision (mAP)');
legend('unimodal X (Euclidean)', 'unimodal X (SSH)',...
       'unimodal Y (Euclidean)', 'unimodal Y (SSH)',... 
       'Cross-modality (MMSSH)', 'Location', 'SouthEast');

