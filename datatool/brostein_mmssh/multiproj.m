function q = multiproj(J,PY,p,n,s, i0,j0)

%p = Py;
%n = 12;
%s = 1;
if nargin<6, i0 = 1; end
if nargin<7, j0 = 1; end

[i,j] = ndgrid([0:s:n-p-1]+i0-1,[0:s:n-p-1]+j0-1);
o = i+j*n;

[k,l] = ndgrid(1:p,1:p);
idx = sub2ind([n n], k,l);

IDX = bsxfun(@plus, idx(:), o(:)');

q = bsxfun(@plus, PY(:,1:end-1)*J(IDX), PY(:,end));
q = double(q>=0);
%q = J(IDX);
%q = q(PY,:);