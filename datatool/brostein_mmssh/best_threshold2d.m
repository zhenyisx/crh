function [thx, thy, f] = best_threshold2d(xp,yp,xn,yn, wp,wn, alpha, loss)


% Loss function
switch lower(loss),
    
    case 'exp'
        Efp = exp(+alpha);
        Etp = exp(-alpha);
        Etn = exp(-alpha);
        Efn = exp(+alpha);
        
    case 'quad'
    
        Efp = 0;
        Etp = -1;
        Etn = -1;
        Efn = 0;
        
    otherwise
        
        throw(MException('ParseOptions:Unsupported', 'Loss function unsupported'));
        
end    

x0 = sum(bsxfun(@times, [xp xn], [wp(:)' wn(:)']),2) / (sum(wp)+sum(wn));
y0 = sum(bsxfun(@times, [yp yn], [wp(:)' wn(:)']),2) / (sum(wp)+sum(wn));


x2 = sqrt(sum(bsxfun(@times, ([xp xn]-x0).^2, [wp(:)' wn(:)']),2) / (sum(wp)+sum(wn)));
y2 = sqrt(sum(bsxfun(@times, ([yp yn]-y0).^2, [wp(:)' wn(:)']),2) / (sum(wp)+sum(wn)));


xmin = x0-3*x2;
xmax = x0+3*x2;
ymin = y0-3*y2;
ymax = y0+3*y2;

nbins = 100;

dx = (xmax-xmin)/(nbins-1);
dy = (ymax-ymin)/(nbins-1);

ixp = min(nbins, max(1, round((xp-xmin)/(xmax-xmin)*nbins)+1));
ixn = min(nbins, max(1, round((xn-xmin)/(xmax-xmin)*nbins)+1));
iyp = min(nbins, max(1, round((yp-ymin)/(ymax-ymin)*nbins)+1));
iyn = min(nbins, max(1, round((yn-ymin)/(ymax-ymin)*nbins)+1));

idxp = (ixp-1) + (iyp-1)*nbins;
WP = reshape(whistint(uint32(idxp(:)), wp(:)/sum(wp(:)), nbins*nbins), [nbins nbins]);
WP = cumsum(cumsum(WP,1),2);
idxn = (ixn-1) + (iyn-1)*nbins;
WN = reshape(whistint(uint32(idxn(:)), wn(:)/sum(wn(:)), nbins*nbins), [nbins nbins]);
WN = cumsum(cumsum(WN,1),2);

%[X,Y] = ndgrid(1:nbins, 1:nbins);

FP = bsxfun(@plus,WP(end,:),WP(:,end)) - 2*WP;
%FN = WN(end,end)-bsxfun(@plus,WN(end,:),WN(:,end)) + WN;
FN = 1 - (bsxfun(@plus,WN(end,:),WN(:,end)) - 2*WN);
%F = FP+FN;
%F = exp(alpha)*(FP+FN) + exp(-alpha)*(1-FP-FN);
F = Efp*FP + Etp*(1-FP) + Efn*FN + Etn*(1-FN);

[f,idx] = min(F(:));
[i,j] = ind2sub(size(F), idx);

thx = (i-1)/nbins*(xmax-xmin)+xmin;
thy = (j-1)/nbins*(ymax-ymin)+ymin;



% Refine
scale = 2;

idxp = find(abs(xp-thx) <= 1.01*scale*dx & abs(yp-thy) <= 1.01*scale*dy);
idxn = find(abs(xn-thx) <= 1.01*scale*dx & abs(yn-thy) <= 1.01*scale*dy);

xmin = thx-scale*dx;
xmax = thx+scale*dx;
ymin = thy-scale*dy;
ymax = thy+scale*dy;

dx = (xmax-xmin)/(nbins-1);
dy = (ymax-ymin)/(nbins-1);

xp_ = xp(idxp);
yp_ = yp(idxp);
xn_ = xn(idxn);
yn_ = yn(idxn);

ixp = min(nbins, max(1, round((xp_-xmin)/(xmax-xmin)*nbins)+1));
ixn = min(nbins, max(1, round((xn_-xmin)/(xmax-xmin)*nbins)+1));
iyp = min(nbins, max(1, round((yp_-ymin)/(ymax-ymin)*nbins)+1));
iyn = min(nbins, max(1, round((yn_-ymin)/(ymax-ymin)*nbins)+1));


idxp = (ixp-1) + (iyp-1)*nbins;
WP = reshape(whistint(uint32(idxp(:)), wp(:)/sum(wp(:)), nbins*nbins), [nbins nbins]);
WP = cumsum(cumsum(WP,1),2);
idxn = (ixn-1) + (iyn-1)*nbins;
WN = reshape(whistint(uint32(idxn(:)), wn(:)/sum(wn(:)), nbins*nbins), [nbins nbins]);
WN = cumsum(cumsum(WN,1),2);

FP = bsxfun(@plus,WP(end,:),WP(:,end)) - 2*WP;
FN = 1 - (bsxfun(@plus,WN(end,:),WN(:,end)) - 2*WN);
%F = FP+FN;
F = Efp*FP + Etp*(1-FP) + Efn*FN + Etn*(1-FN);

[f,idx] = min(F(:));
[i,j] = ind2sub(size(F), idx);

thx = (i-1)/nbins*(xmax-xmin)+xmin;
thy = (j-1)/nbins*(ymax-ymin)+ymin;


% Calculate cost function
sxp = sign(xp-thx);
sxn = sign(xn-thx);
syp = sign(yp-thy);
syn = sign(yn-thy);

dp = double(sxp.*syp);
dn = double(sxn.*syn);

%f = exp(-alpha*dp(:))'*wp(:) + exp(+alpha*dn(:))'*wn(:);
fn = sum(wp(dp<0));
tp = sum(wp(dp>=0));
fp = sum(wn(dn>=0));
tn = sum(wn(dn<0));

f = Efp*fp + Efn*fn + Etp*tp + Etn*tn;

%f = dp(:)'*wp(:) + dn(:)'*wn(:);
