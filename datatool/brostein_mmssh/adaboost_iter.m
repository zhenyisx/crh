function [proj, xp_best,yp_best,xn_best,yn_best, alpha] = adaboost_iter(Xp,Yp,Xn,Yn,wp,wn,J,K,L, alpha, loss)

    V = optimal_subspace(Xp,Yp,Xn,Yn,wp,wn, J, K);
    
    % Add random directions
    if L-K>0,
        V = [V , V*randn(size(V,2), L-K)];
    end
    
    str = mprintf('', '   Optimizing threshold...');    
    best_val = Inf;    
    proj = [];
    xp_best = [];
    yp_best = [];
    xn_best = [];
    yn_best = [];
    for i=1:size(V,2),
        % Do gap optimization to find best threshold 
        % for projection on the selected direction
        v = V(:,i);
        xp = v'*Xp;
        yp = v'*Yp;
        xn = v'*Xn;
        yn = v'*Yn;
        [th, val, alpha] = best_threshold(xp, yp, xn, yn, wp, wn, alpha, loss);
        if val < best_val,
            best_val = val;
            proj = [v', -th];
            xp_best = xp - th;
            yp_best = yp - th;
            xn_best = xn - th;
            yn_best = yn - th;            
        end
    end
    
    % Make sure that 0 vector always has 0 bits 
    % by flipping projection sign
    if proj(end) > 0, proj = -proj; end
    
    str = mprintf(str, '');    
  