function pairs = generate_negatives(Labels_te,K,posflag)

ul = unique(Labels_te);
pairs = [];

for n=1:4

    for k = 1:length(ul)
        ll = find(Labels_te == ul(k));
        ll = ll(randperm(length(ll)));
        ll = ll(1:min(K,length(ll)));

        ln = find(Labels_te ~= ul(k));
        ln = ln(randperm(length(ln)));
        ln = ln(1:min([K length(ln) length(ll)]));

        [i j] = generate_couples(length(ll),0);
        pairs = [pairs; ll(i) ln(j)];
    end

end

pairs = unique(pairs,'rows');

if posflag
   pairs = setdiff(pairs,pairs(:,[2 1]),'rows');  
end


function [i j] = generate_couples(N,rptflag)

i = [];
j = [];

if rptflag

    for k = 1:N
        for l = k+1:N
            i = [i; k];
            j = [j; l];
        end
    end

else

    for k = 1:N
        for l = 1:N
            i = [i; k];
            j = [j; l];
        end
    end
    
    
end