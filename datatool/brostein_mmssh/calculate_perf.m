function [map, ap] = calculate_perf(X,Y, PX,PY, id, strength, MASK)

if ~isempty(PX) & ~isempty(PY),
    x = double(bsxfun(@plus, PX(:,1:end-1)*X, PX(:,end))>=0);
    y = double(bsxfun(@plus, PY(:,1:end-1)*Y, PY(:,end))>=0);
    D = zeros(length(x), length(x));
    for k=1:size(x,1),
        D = D + bsxfun(@ne, x(k,:)', y(k,:));
    end
else
    D = squared_dist(X',Y');
end

%idxq = find(id<=15 & strength == 0);
%idxd = setdiff(1:length(id), idxq);

idxq = 1:length(id);
idxd = 1:length(id);


ap = zeros(length(idxq),1);
for k = 1:length(idxq),
    q = idxq(k);
    i = id(q);
    idxr = find(id == i);
%    idxr = setdiff(idxr, q);
    
    d = D(q,:);
    m = MASK(q,:);
    [d,idx] = sort(d, 'ascend');
    m = m(idx);

    n = cumsum(double(m~=0));
    r = cumsum(double(m>0));
    n(n==0 & r==0) = 1;
    pre = r./n; 
    rec = r./r(end);
    
    ap(k) = sum(double(m>0).*pre)/r(end);
end

map = mean(ap);

M = MASK(idxq,idxd);
MASK = MASK*0;
MASK(idxq,idxd) = M;

dp = D(find(MASK>0));
dn = D(find(MASK<0));

[eer,fpr1,fpr01] = calculate_rates(dp, dn, []);

map = [map eer fpr1 fpr01];

