function make_data_flickr1m


addpath utils;

change_format = 0;
split_tt = 0;
choose_train = 1;

if change_format
    feax = [];
    feay = [];

    % indatafile_tag = sprintf('../../MH_TMM11/Python-1/HashExp/data/flickr1m/tags/allimagetags_sparse.txt');
    % % gnd = 
    % gnd = spconvert(load(indatafile_tag));
    % size(gnd)
    % save(sprintf('../../MH_TMM11/Python-1/HashExp/data/flickr1m/tagmat.mat'),'gnd'); 

    for i=1:100
        indatafile_x = sprintf('../../MH_TMM11/Python-1/HashExp/data/flickr1m/eh_descriptors/eh%d.txt',i);
        indatafile_y = sprintf('../../MH_TMM11/Python-1/HashExp/data/flickr1m/ht_descriptors/ht%d.txt',i);
        ifeax = load(indatafile_x);
        ifeay = load(indatafile_y);
        size(ifeax)
        size(ifeay)
        feax(end+1:end+10000,:) = ifeax;
        feay(end+1:end+10000,:) = ifeay;

        fprintf('iteration: %d\t Dim X: %d\t Dim Y: %d', i, size(feax,2), size(feay,2));
    end

    size(feax)
    size(feay)

    save(sprintf('../../MH_TMM11/Python-1/HashExp/data/flickr1m/feax.mat'),'feax'); 
    save(sprintf('../../MH_TMM11/Python-1/HashExp/data/flickr1m/feay.mat'),'feay'); 

end

if split_tt
    % split database and query
    rdidx  = randperm(1000000);
    querysize = 1000;
    queryidx = rdidx(1:querysize);
    baseidx = rdidx(querysize+1:end);
    
    load(sprintf('../../MH_TMM11/Python-1/HashExp/data/flickr1m/feax.mat'));
    load(sprintf('../../MH_TMM11/Python-1/HashExp/data/flickr1m/feay.mat'));
    load(sprintf('../../MH_TMM11/Python-1/HashExp/data/flickr1m/tagmat100.mat'));
    feax_tr = feax(baseidx,:)';
    feay_tr = feay(baseidx,:)';
    gnd_tr = gnd(:,baseidx);
    feax_te = feax(queryidx,:)';
    feay_te = feay(queryidx,:)';
    gnd_te = gnd(:,queryidx);
%     size(feax_tr)
%     size(feay_tr)
%     size(gnd_tr)
%     size(feax_te)
%     size(feay_te)
%     size(gnd_te)
    clear feax feay gnd
    
    % centering the data
    x_mean = mean(feax_tr,2);
    y_mean = mean(feay_tr,2);
    feax_tr = double(bsxfun(@minus, feax_tr, x_mean)); %feax_tr = feax_tr-repmat(x_mean,1,Ntrain);
    feax_te = double(bsxfun(@minus, feax_te, x_mean)); %feax_te = feax_te-repmat(x_mean,1,Ntest);
    feay_tr = double(bsxfun(@minus, feay_tr, y_mean)); %feay_tr = feay_tr-repmat(y_mean,1,Ntrain);
    feay_te = double(bsxfun(@minus, feay_te, y_mean)); %feay_te = feay_te-repmat(y_mean,1,Ntest);
    
    % save databse and query
    save('../../MH_TMM11/Python-1/HashExp/data/flickr1m/FLICKR1M_TT.mat', 'feax_tr', 'feay_tr', 'gnd_tr', 'feax_te', 'feay_te', 'gnd_te');
end

if choose_train
    trsize = [4000 6000 8000 10000];
    load('../../MH_TMM11/Python-1/HashExp/data/flickr1m/FLICKR1M_TT.mat');
    obratio = 0.001;
    rdids = randperm(size(feax_tr,2));
    for r = 1:10
        rdids = randperm(size(feax_tr,2));
        for i=1:length(trsize)
            itrsize = trsize(i);
            trids = rdids(1:itrsize);
            sfeax = feax_tr(:,trids);
            sfeay = feay_tr(:,trids);
            sSXY = (gnd_tr(:,trids)'*gnd_tr(:,trids))>0;
            sSXY = 2*sSXY-1;

            sOXY = getobservations(itrsize, itrsize, obratio);

            % save data for CVH and CMSSH use
            save(sprintf('../../MH_TMM11/Python-1/HashExp/data/flickr1m/FLICKR1M_TT_TR%d_rep%d', itrsize, r),'sfeax','sfeay','sSXY','sOXY');

            % save data for CRH use
            outdatafile_x = sprintf('../pegasos/data/FLICKR1M_TT_TR%d_feax_rep%d.dat', itrsize, r);
            outdatafile_y = sprintf('../pegasos/data/FLICKR1M_TT_TR%d_feay_rep%d.dat', itrsize, r);
            outdatafile_cross = sprintf('../pegasos/data/FLICKR1M_TT_TR%d_crosssim_rep%d.dat', itrsize, r);
            label_vector = zeros(itrsize,1);
            libsvmwrite(outdatafile_x, label_vector, sparse(sfeax'));
            libsvmwrite(outdatafile_y, label_vector, sparse(sfeay'));
            sparseWrite(sparse(sSXY.*sOXY),outdatafile_cross);
        end
    end
end
 
