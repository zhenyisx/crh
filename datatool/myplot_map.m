function myplot_map(dataname, methodname, trsize, clength)

% firstly, smh-mlbe
% methodname = 'smh-mlbe';
% nclass = 10;
% lmperClass = 30;
topk = 10;
topr = 2;
final_map_xy = [];
final_map_yx = [];
for repid = 1:5
load(sprintf('./results/nips_res_%s_TT_%s_%dtr_%db_rank%d_radius%d_rep%d.mat',...
    dataname, methodname, trsize,clength,topk,topr,repid));

premat = Probresult{1};
recmat = Probresult{2};
retmat = Probresult{3};
rank_premat = Probresult{4};
rank_recmat = Probresult{5};
rank_mapmat = Probresult{6};

final_map_xy = [final_map_xy; mean(rank_mapmat.XY,1)];
final_map_yx = [final_map_yx; mean(rank_mapmat.YX,1)];

end


fprintf('MAP for XY: %.4f pm %.4f\n',mean(final_map_xy,1), std(final_map_xy,[],1));
fprintf('MAP for YX: %.4f pm %.4f\n',mean(final_map_yx,1), std(final_map_yx,[],1));


% myplot_prcurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3);
% myplot_reccurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3);

