function myplot_precision(dataname, methodname, trsize, clength)

% firstly, smh-mlbe
% methodname = 'smh-mlbe';
% nclass = 10;
% lmperClass = 30;
hradius = 2;
final_pre_xy = [];
final_pre_yx = [];

for repid = 1:5
    load(sprintf('./results/nips_res_%s_%s_%dtr_%db_rep%d.mat',...
        dataname, methodname, trsize,clength,repid));

    premat = Probresult{1};
    recmat = Probresult{2};
    retmat = Probresult{3};
    rank_premat = Probresult{4};
    rank_recmat = Probresult{5};
    rank_mapmat = Probresult{6};

    final_pre_xy = [final_pre_xy; mean(premat.XY,1)];
    final_pre_yx = [final_pre_yx; mean(premat.YX,1)];
end
final_pre_xy
final_pre_yx
mfinal_pre_xy = mean(final_pre_xy,1);
sfinal_pre_xy = std(final_pre_xy,[],1);
mfinal_pre_yx = mean(final_pre_yx,1);
sfinal_pre_yx = std(final_pre_yx,[],1);
fprintf('XY: %.4f pm %.4f\n',mfinal_pre_xy(hradius+1), sfinal_pre_xy(hradius+1));
fprintf('YX: %.4f pm %.4f\n',mfinal_pre_yx(hradius+1), sfinal_pre_yx(hradius+1));

% mean(final_pre_yx,1)
% std(final_pre_yx,[],1)
% myplot_prcurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3);
% myplot_reccurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3);

