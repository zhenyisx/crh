function mat2libformat(name)


nclass = 10;
obratio = 0.001;
if strcmp(name,'wiki')
    indatafile = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT%d',nclass);
    % indatafile_cross = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_OB%d_%.4f.mat', nclass, obratio);
    outdatafile_x = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT%d_feax.dat',nclass);
    outdatafile_y = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT%d_feay.dat',nclass);
    outdatafile_cross = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT%d_crosssim.dat',nclass);
% elseif strcmp(name,'flickr')
%     datafile = sprintf('../../MH_TMM11/Python-1/HashExp/data/nus/NUS_TT%d',nclass);
%     
% elseif strcmp(name,'flickr1m')
%     datafile = '../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT';
end

load(indatafile);

% mean-center the features
x_mean = mean(feax_tr,2);
y_mean = mean(feay_tr,2);
feax_tr = double(bsxfun(@minus, feax_tr, x_mean)); %feax_tr = feax_tr-repmat(x_mean,1,Ntrain);
% feax_te = double(bsxfun(@minus, feax_te, x_mean)); %feax_te = feax_te-repmat(x_mean,1,Ntest);
feay_tr = double(bsxfun(@minus, feay_tr, y_mean)); %feay_tr = feay_tr-repmat(y_mean,1,Ntrain);
% feay_te = double(bsxfun(@minus, feay_te, y_mean)); %feay_te = feay_te-repmat(y_mean,1,Ntest);



trsize = [300 500 1000 1500 2000];
obratio = 0.001;
rdids = randperm(size(feax_tr,2));
for i=1:length(trsize)
    itrsize = trsize(i);
    trids = rdids(1:itrsize);
    sfeax_tr = feax_tr(:,trids);
    sfeay_tr = feay_tr(:,trids);
    sSXY = gnd_tr(:,trids)'*gnd_tr(:,trids);
    sSXY = 2*sSXY-1;

    sOXYs = cell(5,1);% observations on (subset) training
    nX = size(feax_tr,2);
    nY = size(feay_tr,2);
    for k =1:5
        sOXYs{k} = getobservations(nX, nY,obratio);
    end
    save(sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT%d_tr%d',nclass, itrsize),'sfeax_tr','sfeay_tr','sSXY','sOXYs')
end
nx = size(feax_tr,2);
% ny = size(feay_tr,2);

label_vector = zeros(nx,1);
libsvmwrite(outdatafile_x, label_vector, sparse(feax_tr'));
libsvmwrite(outdatafile_y, label_vector, sparse(feay_tr'));

SXY = gnd_tr'*gnd_tr>0;
SXY = 2*SXY-1;

% load(indatafile_cross);
% OXY = OXYs{1};
sparseWrite(sparse(SXY.*OXY),outdatafile_cross);

% [label_vector trx_mat] = libsvmread(outdatafile_x);
% [label_vector try_mat] = libsvmread(outdatafile_y);