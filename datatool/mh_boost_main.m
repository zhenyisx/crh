function [ProbResults, hyper] = mh_boost_main(ProbParam)
% This function is the entry function of MLBE
% input:
%   ProbParam contains parameters need for experiments, they are:
%       ProbParam.datafile          : .mat data file
%       ProbParam.neighborfile      : .mat neighborhood file
%       ProbParam.indexfile         : .mat random index file
%       ProbParam.evaltype          : 'rank' or 'radius' or 'both'
%       ProbParam.hammingrank       : rank positions for evaluation
%       ProbParam.hammingradius     : radii for evaluation
%       ProbParam.codelength        : # of bits of binary codes
%       ProbParam.repeats           : # of experiments, each experiments corresponds to a random landmark set
%       ProbParam.lapregname        : 'none', 'label', 'feature', when ProbParam.methodname = 'ksmh' or 'rksmh'
%       ProbParam.methodname        : 'smhbase', 'ksmh' or 'rksmh'
%       ProbParam.threshname        : 'zero','mean','median','quater-mean','quater-median','learn'
%       ProbParam.landmarksize      : size of landmark set
%       ProbParam.gammalapreg       : gamma for laplacian regularization
%       ProbParam.expname           : 'normal','landmark','regularizer', varying different parameters
%       Probparam.subtrain          : use a subset for training, -1 indicate use all training set
% output:
%   ProbResults contains the results, they are

% add path
addpath utils;
% addpath lightspeed;
% addpath plots;

switch ProbParam.expname
    case 'normal'
        
        if strcmp(ProbParam.methodname,'crh') || strcmp(ProbParam.methodname,'bNPP')        
            
            load(ProbParam.datafile);
            Deig = sparse(gnd_tr'*gnd_te>0);% a sparse matrix
            
            % mean-center the features
            x_mean = mean(feax_tr,2);
            y_mean = mean(feay_tr,2);
            feax_tr = double(bsxfun(@minus, feax_tr, x_mean)); %feax_tr = feax_tr-repmat(x_mean,1,Ntrain);
            feax_te = double(bsxfun(@minus, feax_te, x_mean)); %feax_te = feax_te-repmat(x_mean,1,Ntest);
            feay_tr = double(bsxfun(@minus, feay_tr, y_mean)); %feay_tr = feay_tr-repmat(y_mean,1,Ntrain);
            feay_te = double(bsxfun(@minus, feay_te, y_mean)); %feay_te = feay_te-repmat(y_mean,1,Ntest);
            
            % learn and test
            if length(ProbParam.codelength)>1
                disp('wrong code length. Should be a scaler!');
                return;
            else
                curr_length = ProbParam.codelength;
            end
            
            premat.XY = zeros(1, curr_length+1);
            premat.YX = zeros(1, curr_length+1);
            recmat.XY = zeros(1, curr_length+1);
            recmat.YX = zeros(1, curr_length+1);
            retmat.XY = zeros(1, curr_length+1);
            retmat.YX = zeros(1, curr_length+1);
            rank_mapmat.XY = zeros(1, 1);
            rank_mapmat.YX = zeros(1, 1);
            rank_premat.XY = zeros(1, 1);
            rank_premat.YX = zeros(1, 1);
            rank_recmat.XY = zeros(1, 1);
            rank_recmat.YX = zeros(1, 1);
            
            % reading model parameters
            Wx = [];
            for k = 1:curr_length
                Wx(:,end+1) = load(sprintf('%s_x_%d.model', ProbParam.modelfilename, k-1))';
            end

            Wy = [];
            for k = 1:curr_length
                Wy(:,end+1) = load(sprintf('%s_y_%d.model', ProbParam.modelfilename, k-1))';
            end
            
            % Compress dataset: B = [nWords(ceil(Nbits/8)), Nsamples]
            [Bx_tr] = compressSMH(feax_tr, Wx, 'zero', 0);% Xtr
            [Bx_te] = compressSMH(feax_te, Wx, 'zero', 0);% Xte
            clear feax_tr feax_te Wx
            [By_tr] = compressSMH(feay_tr, Wy, 'zero', 0);% Ytr
            [By_te] = compressSMH(feay_te, Wy, 'zero', 0);% Yte
            clear feay_tr feay_te Wy

            % Compute hamming distance
            [Dhamm] = computeHdist(Bx_te, By_te, Bx_tr, By_tr);clear Bx_tr By_tr Bx_te By_te
            DN.XY = Deig; DN.YX = Deig;

            % Evaluation
%             [pre_results, rec_results, ret_results] = pr_evaluation2(DN, Dhamm, curr_length);
%             premat.XY(1,:) = pre_results.XY;premat.YX(1,:) = pre_results.YX;
%             recmat.XY(1,:) = rec_results.XY;recmat.YX(1,:) = rec_results.YX;            
%             retmat.XY(1,:) = ret_results.XY;retmat.YX(1,:) = ret_results.YX;            
%             [rank_premat.XY(1, 1),rank_recmat.XY(1, 1),rank_mapmat.XY(1, 1)]...
%                 = evaluation_hammingrank(DN.XY, Dhamm.XY, ProbParam.hammingrank);
%             [rank_premat.YX(1, 1),rank_recmat.YX(1, 1),rank_mapmat.YX(1, 1)]...
%                 = evaluation_hammingrank(DN.YX, Dhamm.YX, ProbParam.hammingrank);
            [premat.XY(1,:), recmat.XY(1,:), retmat.XY(1,:),...
                rank_premat.XY(1, 1),rank_recmat.XY(1, 1),rank_mapmat.XY(1, 1)]=...
                totalevaluationc(DN.XY, Dhamm.XY, curr_length, ProbParam.hammingrank);
            [premat.YX(1,:), recmat.YX(1,:), retmat.YX(1,:),...
                rank_premat.YX(1, 1),rank_recmat.YX(1, 1),rank_mapmat.YX(1, 1)]=...
                totalevaluationc(DN.YX, Dhamm.YX, curr_length, ProbParam.hammingrank);
            ProbResults = cell(6,1); 
            ProbResults{1} = premat; 
            ProbResults{2} = recmat;
            ProbResults{3} = retmat;
            ProbResults{4} = rank_premat; 
            ProbResults{5} = rank_recmat;
            ProbResults{6} = rank_mapmat;
            hyper = [];
        elseif strcmp(ProbParam.methodname,'npp_sh')            
            
            load(ProbParam.datafile);
            Deig = sparse(gnd_tr'*gnd_te>0);
            
            % mean-center the features
            x_mean = mean(feax_tr,2);
            y_mean = mean(feay_tr,2);
            feax_tr = double(bsxfun(@minus, feax_tr, x_mean)); %feax_tr = feax_tr-repmat(x_mean,1,Ntrain);
            feax_te = double(bsxfun(@minus, feax_te, x_mean)); %feax_te = feax_te-repmat(x_mean,1,Ntest);
            feay_tr = double(bsxfun(@minus, feay_tr, y_mean)); %feay_tr = feay_tr-repmat(y_mean,1,Ntrain);
            feay_te = double(bsxfun(@minus, feay_te, y_mean)); %feay_te = feay_te-repmat(y_mean,1,Ntest);
            
            % learn and test
            if length(ProbParam.codelength)>1
                disp('wrong w/ code length. Should be a scaler!');
                return;
            else
                curr_length = ProbParam.codelength;
            end
            
            premat.XY = zeros(1, curr_length+1);
            premat.YX = zeros(1, curr_length+1);
            recmat.XY = zeros(1, curr_length+1);
            recmat.YX = zeros(1, curr_length+1);
            retmat.XY = zeros(1, curr_length+1);
            retmat.YX = zeros(1, curr_length+1);
            rank_mapmat.XY = zeros(1, 1);
            rank_mapmat.YX = zeros(1, 1);
            rank_premat.XY = zeros(1, 1);
            rank_premat.YX = zeros(1, 1);
            rank_recmat.XY = zeros(1, 1);
            rank_recmat.YX = zeros(1, 1);
            
            % reading model parameters
            Wx = [];
            for k = 1:curr_length
                Wx(:,end+1) = load(sprintf('%s_x_%d.model', ProbParam.modelfilename, k-1))';
            end

            Wy = [];
            for k = 1:curr_length
                Wy(:,end+1) = load(sprintf('%s_y_%d.model', ProbParam.modelfilename, k-1))';
            end
            
            % new embedding
            nfeax_tr = feax_tr'*Wx;
            nfeax_te = feax_te'*Wx;
            nfeay_tr = feay_tr'*Wy;
            nfeay_te = feay_te'*Wy;
            
            % spectral hashing
            % Ttraining
            SHparamx.nbits = curr_length;
            SHparamx = trainSH(nfeax_tr, SHparamx);
            SHparamy.nbits = curr_length;
            SHparamy = trainSH(nfeay_tr, SHparamy);

            % Compress dataset: B = [ceil(Nbits/8), Nsamples]
            Bx_tr = compressSH(nfeax_tr, SHparamx); %Bx_tr = Bx_tr1'; clear Bx_tr1;
            Bx_te = compressSH(nfeax_te, SHparamx); %Bx_te = Bx_te1'; clear Bx_te1;
            By_tr = compressSH(nfeay_tr, SHparamy); %By_tr = By_tr1'; clear By_tr1;
            By_te = compressSH(nfeay_te, SHparamy); %By_te = By_te1'; clear By_te1;
            
%             % Compress dataset: B = [nWords(ceil(Nbits/8)), Nsamples]
%             [Bx_tr] = compressSMH(feax_tr, Wx, 'zero', 0);% Xtr
%             [Bx_te] = compressSMH(feax_te, Wx, 'zero', 0);% Xte
%             [By_tr] = compressSMH(feay_tr, Wy, 'zero', 0);% Ytr
%             [By_te] = compressSMH(feay_te, Wy, 'zero', 0);% Yte

            % Compute hamming distance
            [Dhamm] = computeHdist(Bx_te, By_te, Bx_tr, By_tr);clear Bx_tr By_tr Bx_te By_te
            DN.XY = Deig; DN.YX = Deig;

            % Evaluation
            [pre_results, rec_results, ret_results] = pr_evaluation2(DN, Dhamm, curr_length);
            premat.XY(1,:) = pre_results.XY;
            premat.YX(1,:) = pre_results.YX;
            recmat.XY(1,:) = rec_results.XY;
            recmat.YX(1,:) = rec_results.YX;
            retmat.XY(1,:) = ret_results.XY;
            retmat.YX(1,:) = ret_results.YX;
            [rank_premat.XY(1, 1),rank_recmat.XY(1, 1),rank_mapmat.XY(1, 1)]...
                = evaluation_hammingrank(DN.XY, Dhamm.XY, ProbParam.hammingrank);
            [rank_premat.YX(1, 1),rank_recmat.YX(1, 1),rank_mapmat.YX(1, 1)]...
                = evaluation_hammingrank(DN.YX, Dhamm.YX, ProbParam.hammingrank);

            ProbResults = cell(6,1); 
            ProbResults{1} = premat; 
            ProbResults{2} = recmat;
            ProbResults{3} = retmat;
            ProbResults{4} = rank_premat; 
            ProbResults{5} = rank_recmat;
            ProbResults{6} = rank_mapmat;
            hyper = [];
        elseif strcmp(ProbParam.methodname, 'bronstein')
            
%             load(ProbParam.simfile);
            load(ProbParam.trainfile);
%             clear SXY_tr SXY_te
%             SXY_tr = (gnd_tr'*gnd_tr)>0;
%             SXY_te = (gnd_tr'*gnd_te)>0;
            
                       
%             % load observation file
%             load(ProbParam.obfile);
%             OXY_tr = OXYs{1};clear OXYs;
            

            % make data for CMSSH
%             X = [feax_tr feax_te]; Y = [feay_tr feay_te];
            ids = find(sOXY>0);
            [nrows, ncols] = ind2sub(size(sSXY),ids);
            Xp_tr = []; Xn_tr = []; Yp_tr = []; Yn_tr = [];
            for i = 1:length(ids)
                if sSXY(nrows(i),ncols(i)) == 1
                    Xp_tr(:,end+1) = sfeax(:, nrows(i));
                    Yp_tr(:,end+1) = sfeay(:, ncols(i));
                else
                    Xn_tr(:,end+1) = sfeax(:, nrows(i));
                    Yn_tr(:,end+1) = sfeay(:, ncols(i));
                end
            end
            
            % learn and test
            if length(ProbParam.codelength)>1
                disp('wrong w/ code length. Should be a scaler!');
                return;
            else
                curr_length = ProbParam.codelength;
            end
            
            premat.XY = zeros(ProbParam.repeats, curr_length+1);
            premat.YX = zeros(ProbParam.repeats, curr_length+1);
            recmat.XY = zeros(ProbParam.repeats, curr_length+1);
            recmat.YX = zeros(ProbParam.repeats, curr_length+1);
            retmat.XY = zeros(ProbParam.repeats, curr_length+1);
            retmat.YX = zeros(ProbParam.repeats, curr_length+1);
            rank_mapmat.XY = zeros(ProbParam.repeats, 1);
            rank_mapmat.YX = zeros(ProbParam.repeats, 1);
            rank_premat.XY = zeros(ProbParam.repeats, 1);
            rank_premat.YX = zeros(ProbParam.repeats, 1);
            rank_recmat.XY = zeros(ProbParam.repeats, 1);
            rank_recmat.YX = zeros(ProbParam.repeats, 1);

            % load data for testing 
            load(ProbParam.datafile);
            % mean-center the features
            x_mean = mean(feax_tr,2);
            y_mean = mean(feay_tr,2);
            feax_tr = double(bsxfun(@minus, feax_tr, x_mean)); %feax_tr = feax_tr-repmat(x_mean,1,Ntrain);
            feax_te = double(bsxfun(@minus, feax_te, x_mean)); %feax_te = feax_te-repmat(x_mean,1,Ntest);
            feay_tr = double(bsxfun(@minus, feay_tr, y_mean)); %feay_tr = feay_tr-repmat(y_mean,1,Ntrain);
            feay_te = double(bsxfun(@minus, feay_te, y_mean)); %feay_te = feay_te-repmat(y_mean,1,Ntest);
 
            Deigh = (gnd_tr'*gnd_te) >0;
            for repeatid = 1:ProbParam.repeats
                [Wx, Wy] = trainBrostein(Xp_tr, Yp_tr, Xn_tr, Yn_tr, [5,10], curr_length);
                iWx = Wx(:,1:end-1)';
                iWy = Wy(:,1:end-1)';
                Tx = Wx(:,end);
                Ty = Wy(:,end);

                % Compress dataset: B = [nWords(ceil(Nbits/8)), Nsamples]
                [Bx_tr] = compressSMH(feax_tr, iWx, 'Given', Tx);% Xtr
                [Bx_te] = compressSMH(feax_te, iWx, 'Given', Tx);% Xte
                [By_tr] = compressSMH(feay_tr, iWy, 'Given', Ty);% Ytr
                [By_te] = compressSMH(feay_te, iWy, 'Given', Ty);% Yte

                % Compute hamming distance
                [Dhamm] = computeHdist(Bx_te, By_te, Bx_tr, By_tr);clear Bx_tr By_tr Bx_te By_te
                DN.XY = Deigh; DN.YX = Deigh;

                % Evaluation
%                 [pre_results, rec_results, ret_results] = pr_evaluation2(DN, Dhamm, curr_length);
%                 premat.XY(repeatid,:) = pre_results.XY;
%                 premat.YX(repeatid,:) = pre_results.YX;
%                 recmat.XY(repeatid,:) = rec_results.XY;
%                 recmat.YX(repeatid,:) = rec_results.YX;
%                 retmat.XY(repeatid,:) = ret_results.XY;
%                 retmat.YX(repeatid,:) = ret_results.YX;
%                 [rank_premat.XY(repeatid, 1),rank_recmat.XY(repeatid, 1),rank_mapmat.XY(repeatid, 1)]...
%                     = evaluation_hammingrank(DN.XY, Dhamm.XY, ProbParam.hammingrank);
%                 [rank_premat.YX(repeatid, 1),rank_recmat.YX(repeatid, 1),rank_mapmat.YX(repeatid, 1)]...
%                     = evaluation_hammingrank(DN.YX, Dhamm.YX, ProbParam.hammingrank);
                [premat.XY(1,:), recmat.XY(1,:), retmat.XY(1,:),...
                    rank_premat.XY(repeatid, 1),rank_recmat.XY(repeatid, 1),rank_mapmat.XY(repeatid, 1)]=...
                    totalevaluationc(DN.XY, Dhamm.XY, curr_length, ProbParam.hammingrank);
                [premat.YX(1,:), recmat.YX(1,:), retmat.YX(1,:),...
                    rank_premat.YX(repeatid, 1),rank_recmat.YX(repeatid, 1),rank_mapmat.YX(repeatid, 1)]=...
                    totalevaluationc(DN.YX, Dhamm.YX, curr_length, ProbParam.hammingrank);
            end
            
            % store the results and return
            ProbResults = cell(6,1); 
            ProbResults{1} = premat; 
            ProbResults{2} = recmat;
            ProbResults{3} = retmat;
            ProbResults{4} = rank_premat; 
            ProbResults{5} = rank_recmat;
            ProbResults{6} = rank_mapmat;
            hyper = [];
        elseif strcmp(ProbParam.methodname, 'cvh')
            
            if length(ProbParam.codelength)>1
                disp('wrong w/ code length. Should be a scaler!');
                return;
            else
                curr_length = ProbParam.codelength;
            end
            
            load(ProbParam.trainfile);
                        
            % covariance matrices
            lambda = 1e-6;
            Cxx = sfeax*sfeax'+lambda*eye(size(sfeax,1));
            Cyy = sfeay*sfeay'+lambda*eye(size(sfeay,1));
            Cxy = sfeax*sfeay';
            

            % train CCA
            [Wx, Wy] = trainCCA(Cxx, Cyy, Cxy, curr_length);
            % save parameters for crh use
            dummylabels = zeros(ProbParam.codelength,1);
            libsvmwrite(ProbParam.cvhfilex, dummylabels, sparse(Wx'));
            libsvmwrite(ProbParam.cvhfiley, dummylabels, sparse(Wy'));
            
            % load data for testing 
            load(ProbParam.datafile);
            % mean-center the features
            x_mean = mean(feax_tr,2);
            y_mean = mean(feay_tr,2);
            feax_tr = double(bsxfun(@minus, feax_tr, x_mean)); %feax_tr = feax_tr-repmat(x_mean,1,Ntrain);
            feax_te = double(bsxfun(@minus, feax_te, x_mean)); %feax_te = feax_te-repmat(x_mean,1,Ntest);
            feay_tr = double(bsxfun(@minus, feay_tr, y_mean)); %feay_tr = feay_tr-repmat(y_mean,1,Ntrain);
            feay_te = double(bsxfun(@minus, feay_te, y_mean)); %feay_te = feay_te-repmat(y_mean,1,Ntest);
            Deigh = full((gnd_tr'*gnd_te) >0);
            
            premat.XY = zeros(1, curr_length+1);
            premat.YX = zeros(1, curr_length+1);
            recmat.XY = zeros(1, curr_length+1);
            recmat.YX = zeros(1, curr_length+1);
            retmat.XY = zeros(1, curr_length+1);
            retmat.YX = zeros(1, curr_length+1);
            rank_mapmat.XY = zeros(1, 1);
            rank_mapmat.YX = zeros(1, 1);
            rank_premat.XY = zeros(1, 1);
            rank_premat.YX = zeros(1, 1);
            rank_recmat.XY = zeros(1, 1);
            rank_recmat.YX = zeros(1, 1);
   
            % Compress dataset: B = [nWords(ceil(Nbits/8)), Nsamples]
            [Bx_tr] = compressSMH(feax_tr, Wx, 'Given', 0);% Xtr
            [Bx_te] = compressSMH(feax_te, Wx, 'Given', 0);% Xte
            [By_tr] = compressSMH(feay_tr, Wy, 'Given', 0);% Ytr
            [By_te] = compressSMH(feay_te, Wy, 'Given', 0);% Yte

            % Compute hamming distance
            [Dhamm] = computeHdist(Bx_te, By_te, Bx_tr, By_tr);clear Bx_tr By_tr Bx_te By_te
            % DN.XY = Deigh; DN.YX = Deigh;

            % Evaluation
%             tic
%             [pre_results1, rec_results1, ret_results1] = pr_evaluation2(DN, Dhamm, curr_length);
%             toc
%             tic
%             [pre_results.XY, rec_results.XY, ret_results.XY] = pr_evaluationc(DN.XY, Dhamm.XY, curr_length);
%             [pre_results.YX, rec_results.YX, ret_results.YX] = pr_evaluationc(DN.YX, Dhamm.YX, curr_length);
%             toc
%             premat.XY(1,:) = pre_results.XY;
%             premat.YX(1,:) = pre_results.YX;
%             recmat.XY(1,:) = rec_results.XY;
%             recmat.YX(1,:) = rec_results.YX;
%             retmat.XY(1,:) = ret_results.XY;
%             retmat.YX(1,:) = ret_results.YX;
% %             tic
% %             [rank_premat.XY(1, 1),rank_recmat.XY(1, 1),rank_mapmat.XY(1, 1)]...
% %                 = evaluation_hammingrank((DN.XY)', (Dhamm.XY)', ProbParam.hammingrank);
% %             toc
            
% %             toc
% %             [rank_premat.YX(1, 1),rank_recmat.YX(1, 1),rank_mapmat.YX(1, 1)]...
% %                 = evaluation_hammingrank(DN.YX, Dhamm.YX, ProbParam.hammingrank);
% %             pause
% %             tic
%             [rank_premat.XY(1, 1),rank_recmat.XY(1, 1),rank_mapmat.XY(1, 1)]...
%                 = map_evaluationc(DN.XY, Dhamm.XY, ProbParam.hammingrank);
%             [rank_premat.YX(1, 1),rank_recmat.YX(1, 1),rank_mapmat.YX(1, 1)]...
%                 = map_evaluationc(DN.YX, Dhamm.YX, ProbParam.hammingrank);
%             [d1, e1, f1] = pr_evaluationc(Deigh, Dhamm.XY, curr_length);
%             [d2, e2, f2] = pr_evaluationc(Deigh, Dhamm.YX, curr_length);
            [a1 b1 c1] = evaluation_hammingrank(Deigh', Dhamm.XY', ProbParam.hammingrank);
            [a2 b2 c2] = evaluation_hammingrank(Deigh', Dhamm.YX', ProbParam.hammingrank);
            [premat.XY(1,:), recmat.XY(1,:), retmat.XY(1,:),...
                rank_premat.XY(1, 1),rank_recmat.XY(1, 1),rank_mapmat.XY(1, 1)]=...
                totalevaluationc(Deigh, Dhamm.XY, curr_length, ProbParam.hammingrank);
            [premat.YX(1,:), recmat.YX(1,:), retmat.YX(1,:),...
                rank_premat.YX(1, 1),rank_recmat.YX(1, 1),rank_mapmat.YX(1, 1)]=...
                totalevaluationc(Deigh, Dhamm.YX, curr_length, ProbParam.hammingrank);
            ProbResults = cell(6,1); 
            ProbResults{1} = premat; 
            ProbResults{2} = recmat; 
            ProbResults{3} = retmat; 
            ProbResults{4} = rank_premat; 
            ProbResults{5} = rank_recmat; 
            ProbResults{6} = rank_mapmat;
            hyper = [];
        else
            disp('Your method is not supported.');
        end
    otherwise
        disp('Exp name provided is not supported. Please use "normal".');
end

end

function [radius_premat, radius_recmat, rank_mapmat, rank_premat, rank_recmat] = makecontainer(nRow, nCol)
    radius_premat.XX = zeros(nRow, nCol);
    radius_premat.YY = zeros(nRow, nCol);
    radius_premat.XY = zeros(nRow, nCol);
    radius_premat.YX = zeros(nRow, nCol);
    radius_recmat.XX = zeros(nRow, nCol);
    radius_recmat.YY = zeros(nRow, nCol);
    radius_recmat.XY = zeros(nRow, nCol);
    radius_recmat.YX = zeros(nRow, nCol);
    rank_mapmat.XX = zeros(nRow, nCol);
    rank_mapmat.YY = zeros(nRow, nCol);
    rank_mapmat.XY = zeros(nRow, nCol);
    rank_mapmat.YX = zeros(nRow, nCol);
    rank_premat.XX = zeros(nRow, nCol);
    rank_premat.YY = zeros(nRow, nCol);
    rank_premat.XY = zeros(nRow, nCol);
    rank_premat.YX = zeros(nRow, nCol);
    rank_recmat.XX = zeros(nRow, nCol);
    rank_recmat.YY = zeros(nRow, nCol);
    rank_recmat.XY = zeros(nRow, nCol);
    rank_recmat.YX = zeros(nRow, nCol);
end
    
function [Klx_tr, Klx_te, Kly_tr, Kly_te, Kllx, Klly] = computeKernel(kernelname, feax_tr, feax_te, feay_tr, feay_te, feax_lm, feay_lm)
    switch kernelname
        case 'linear' % linear kernel
            Klx_tr = feax_lm'*feax_tr;  
            Kly_tr = feay_lm'*feay_tr;
            Klx_te = feax_lm'*feax_te;  
            Kly_te = feay_lm'*feay_te;
            Kllx = feax_lm'*feax_lm;    
            Klly = feay_lm'*feay_lm; 
        case 'rbf' % rbf kernel
            sc = 10;
            % gndmat_tr = (gnd_tr'*gnd_tr)>0; 
            % 
            % l2matx = distMat(feax_tr'); 
            % l2gndmatx = l2matx.*gndmat_tr; 
            % % sigmax = max(l2gndmatx(:)); % max
            % sigmax = mean(nonzeros(l2gndmatx))% mean
            Klx_tr = rbfkernel(feax_lm,feax_tr,sc,'fast'); 
            Klx_te = rbfkernel(feax_lm,feax_te,sc,'fast');
            Kllx = rbfkernel(feax_lm,feax_lm,sc,'fast');
            % 
            % l2maty = distMat(feay_tr');
            % l2gndmaty = l2maty.*gndmat_tr; 
            % % sigmay = max(l2gndmaty(:)); % max
            % sigmay = mean(nonzeros(l2gndmaty)) % mean
            Kly_tr = rbfkernel(feay_lm,feay_tr,sc,'fast'); 
            Kly_te = rbfkernel(feay_lm,feay_te,sc,'fast');
            Klly = rbfkernel(feay_lm,feay_lm,sc,'fast');
        otherwise
            disp('Kernel type not supported.')
    end
end

function [Rx, Ry] = computerLaplacian(lapregname, gamma, gnd_tr, feax_tr, feay_tr)
Ntrain = size(feax_tr,2);
switch lapregname
    case 'label'
    % % regularizer matrix with laplacian on labels
        W = sparse(gnd_tr'*gnd_tr); D = diag(sum(W,2)); Lx = D - W;% D2 = sqrt(D); Lx = inv(D2)*Lx*inv(D2); 
        Rx = eye(Ntrain)+gamma*Lx; Ry = Rx;
    case 'feature'
    % % regularizer matrix with laplacian on feature similarities (Lx and Ly will be different!)
        Rn = 20;
        l2Dmatx = dist2Mat(feax_tr', feax_tr');  [vx ix] = sort(l2Dmatx,1,'ascend');
        l2Dmaty = dist2Mat(feay_tr', feay_tr');  [vy iy] = sort(l2Dmaty,1,'ascend');
        l2Nmatx = zeros(Ntrain, Ntrain);  l2Nmaty = zeros(Ntrain, Ntrain);
        for i = 1:Ntrain
            l2Nmatx(ix(1:Rn,i),i) = 1;
            l2Nmaty(iy(1:Rn,i),i) = 1;
        end
        l2Nmatx = (l2Nmatx + l2Nmatx')>0;  l2Nmaty = (l2Nmaty + l2Nmaty')>0;
        l2Wmatx = exp(-l2Dmatx).*l2Nmatx;  l2Wmaty = exp(-l2Dmaty).*l2Nmaty;
        Lx = diag(sum(l2Wmatx,2)) - l2Wmatx;  Ly = diag(sum(l2Wmaty,2)) - l2Wmaty;
        Rx = eye(Ntrain)+gamma*Lx; Ry = eye(Ntrain)+gamma*Ly;
    otherwise
        disp('Laplacian is not supported.');
end
end

function [rank_premat, rank_recmat, rank_mapmat,radius_premat, radius_recmat] = perform_evaluation2(evaltype, iterationid, lengthid, DNeig, Dhamm, hammingrank, hammingradius, rank_premat, rank_recmat, rank_mapmat,radius_premat, radius_recmat)
    switch evaltype
        case 'rank'
%             [rank_premat.XX(iterationid, lengthid),rank_recmat.XX(iterationid, lengthid),rank_mapmat.XX(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.XX, hammingrank);
%             [rank_premat.YY(iterationid, lengthid),rank_recmat.YY(iterationid, lengthid),rank_mapmat.YY(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.YY, hammingrank);
            [rank_premat.XY(iterationid, lengthid),rank_recmat.XY(iterationid, lengthid),rank_mapmat.XY(iterationid, lengthid)] = evaluation_hammingrank(DNeig.XY, Dhamm.XY, hammingrank);
            [rank_premat.YX(iterationid, lengthid),rank_recmat.YX(iterationid, lengthid),rank_mapmat.YX(iterationid, lengthid)] = evaluation_hammingrank(DNeig.YX, Dhamm.YX, hammingrank);
        case 'radius'
%             [radius_premat.XX(iterationid, lengthid),radius_recmat.XX(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.XX, hammingradius, 'simple');
%             [radius_premat.YY(iterationid, lengthid),radius_recmat.YY(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.YY, hammingradius, 'simple');
            [radius_premat.XY(iterationid, lengthid),radius_recmat.XY(iterationid, lengthid)] = evaluation_hammingradius(DNeig.XY, Dhamm.XY, hammingradius, 'simple');
            [radius_premat.YX(iterationid, lengthid),radius_recmat.YX(iterationid, lengthid)] = evaluation_hammingradius(DNeig.YX, Dhamm.YX, hammingradius, 'simple');    
        case 'both'
%             [rank_premat.XX(iterationid, lengthid),rank_recmat.XX(iterationid, lengthid),rank_mapmat.XX(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.XX, hammingrank);
%             [rank_premat.YY(iterationid, lengthid),rank_recmat.YY(iterationid, lengthid),rank_mapmat.YY(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.YY, hammingrank);
            [rank_premat.XY(iterationid, lengthid),rank_recmat.XY(iterationid, lengthid),rank_mapmat.XY(iterationid, lengthid)] = evaluation_hammingrank(DNeig.XY, Dhamm.XY, hammingrank);
            [rank_premat.YX(iterationid, lengthid),rank_recmat.YX(iterationid, lengthid),rank_mapmat.YX(iterationid, lengthid)] = evaluation_hammingrank(DNeig.YX, Dhamm.YX, hammingrank);
%             [radius_premat.XX(iterationid, lengthid),radius_recmat.XX(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.XX, hammingradius, 'simple');
%             [radius_premat.YY(iterationid, lengthid),radius_recmat.YY(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.YY, hammingradius, 'simple');
            [radius_premat.XY(iterationid, lengthid),radius_recmat.XY(iterationid, lengthid)] = evaluation_hammingradius(DNeig.XY, Dhamm.XY, hammingradius, 'simple');
            [radius_premat.YX(iterationid, lengthid),radius_recmat.YX(iterationid, lengthid)] = evaluation_hammingradius(DNeig.YX, Dhamm.YX, hammingradius, 'simple');
        otherwise
            disp('Evaluation type not supported!');
    end
end

function [rank_premat, rank_recmat, rank_mapmat,radius_premat, radius_recmat] = perform_evaluation(evaltype, iterationid, lengthid, DNeig, Dhamm, hammingrank, hammingradius, rank_premat, rank_recmat, rank_mapmat,radius_premat, radius_recmat)
    switch evaltype
        case 'rank'
            [rank_premat.XX(iterationid, lengthid),rank_recmat.XX(iterationid, lengthid),rank_mapmat.XX(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.XX, hammingrank);
            [rank_premat.YY(iterationid, lengthid),rank_recmat.YY(iterationid, lengthid),rank_mapmat.YY(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.YY, hammingrank);
            [rank_premat.XY(iterationid, lengthid),rank_recmat.XY(iterationid, lengthid),rank_mapmat.XY(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.XY, hammingrank);
            [rank_premat.YX(iterationid, lengthid),rank_recmat.YX(iterationid, lengthid),rank_mapmat.YX(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.YX, hammingrank);
        case 'radius'
            [radius_premat.XX(iterationid, lengthid),radius_recmat.XX(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.XX, hammingradius, 'simple');
            [radius_premat.YY(iterationid, lengthid),radius_recmat.YY(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.YY, hammingradius, 'simple');
            [radius_premat.XY(iterationid, lengthid),radius_recmat.XY(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.XY, hammingradius, 'simple');
            [radius_premat.YX(iterationid, lengthid),radius_recmat.YX(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.YX, hammingradius, 'simple');    
        case 'both'
            [rank_premat.XX(iterationid, lengthid),rank_recmat.XX(iterationid, lengthid),rank_mapmat.XX(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.XX, hammingrank);
            [rank_premat.YY(iterationid, lengthid),rank_recmat.YY(iterationid, lengthid),rank_mapmat.YY(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.YY, hammingrank);
            [rank_premat.XY(iterationid, lengthid),rank_recmat.XY(iterationid, lengthid),rank_mapmat.XY(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.XY, hammingrank);
            [rank_premat.YX(iterationid, lengthid),rank_recmat.YX(iterationid, lengthid),rank_mapmat.YX(iterationid, lengthid)] = evaluation_hammingrank(DNeig, Dhamm.YX, hammingrank);
            [radius_premat.XX(iterationid, lengthid),radius_recmat.XX(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.XX, hammingradius, 'simple');
            [radius_premat.YY(iterationid, lengthid),radius_recmat.YY(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.YY, hammingradius, 'simple');
            [radius_premat.XY(iterationid, lengthid),radius_recmat.XY(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.XY, hammingradius, 'simple');
            [radius_premat.YX(iterationid, lengthid),radius_recmat.YX(iterationid, lengthid)] = evaluation_hammingradius(DNeig, Dhamm.YX, hammingradius, 'simple');
        otherwise
            disp('Evaluation type not supported!');
    end
end

function [pr_prevec, pr_recvec] = pr_evaluation(DN, Dhamm, codelength)
    pr_prevec.XX = [];pr_prevec.YY = [];pr_prevec.XY = [];pr_prevec.YX = [];
    pr_recvec.XX = [];pr_recvec.YY = [];pr_recvec.XY = [];pr_recvec.YX = [];
    for k = 1:codelength+1
        [pr_prevec.XX(end+1), pr_recvec.XX(end+1)] = evaluation_hammingradius(DN, Dhamm.XX, k-1, 'simple');
        [pr_prevec.YY(end+1), pr_recvec.YY(end+1)] = evaluation_hammingradius(DN, Dhamm.YY, k-1, 'simple');
        [pr_prevec.XY(end+1), pr_recvec.XY(end+1)] = evaluation_hammingradius(DN, Dhamm.XY, k-1, 'simple');
        [pr_prevec.YX(end+1), pr_recvec.YX(end+1)] = evaluation_hammingradius(DN, Dhamm.YX, k-1, 'simple');
    end
end

function [pr_prevec, pr_recvec, pr_retvec] = pr_evaluation2(DN, Dhamm, codelength)
%     pr_prevec.XX = [];pr_prevec.YY = [];
    pr_prevec.XY = [];pr_prevec.YX = [];
%     pr_recvec.XX = [];pr_recvec.YY = [];
    pr_recvec.XY = [];pr_recvec.YX = [];
    pr_retvec.XY = [];pr_retvec.YX = [];
    for k = 1:codelength+1
%         [pr_prevec.XX(end+1), pr_recvec.XX(end+1)] = evaluation_hammingradius(DN, Dhamm.XX, k-1, 'simple');
%         [pr_prevec.YY(end+1), pr_recvec.YY(end+1)] = evaluation_hammingradius(DN, Dhamm.YY, k-1, 'simple');
%         if k<=1 || pr_recvec.XY(end)<1
%         fprintf('current radius: %d\n', k);
            [pr_prevec.XY(end+1), pr_recvec.XY(end+1), pr_retvec.XY(end+1)] = evaluation_hammingradius(DN.XY, Dhamm.XY, k-1, 'simple');%pack;

%             [ta, tb, tc] = evaluation_hammingradius2(double(DN.XY), double(Dhamm.XY), k-1);%pack;

            
%         end
%         if k<=1 || pr_recvec.YX(end)<1
            [pr_prevec.YX(end+1), pr_recvec.YX(end+1), pr_retvec.YX(end+1)] = evaluation_hammingradius(DN.YX, Dhamm.YX, k-1, 'simple');%pack;
%         end
         
    end
end

function [pr_prevec, pr_recvec, pr_retvec] = pr_evaluation3(DN, Dhamm, codelength)
%     pr_prevec.XX = [];pr_prevec.YY = [];
    pr_prevec.XY = [];pr_prevec.YX = [];
%     pr_recvec.XX = [];pr_recvec.YY = [];
    pr_recvec.XY = [];pr_recvec.YX = [];
    pr_retvec.XY = [];pr_retvec.YX = [];
    for k = 1:codelength+1
%         [pr_prevec.XX(end+1), pr_recvec.XX(end+1)] = evaluation_hammingradius(DN, Dhamm.XX, k-1, 'simple');
%         [pr_prevec.YY(end+1), pr_recvec.YY(end+1)] = evaluation_hammingradius(DN, Dhamm.YY, k-1, 'simple');
%         if k<=1 || pr_recvec.XY(end)<1
        % fprintf('current radius: %d\n', k);
        [pr_prevec.XY(end+1), pr_recvec.XY(end+1), pr_retvec.XY(end+1)] = evaluation_hammingradius2(double(DN.XY), double(Dhamm.XY), k-1);%pack;
        [pr_prevec.YX(end+1), pr_recvec.YX(end+1), pr_retvec.YX(end+1)] = evaluation_hammingradius2(double(DN.YX), double(Dhamm.YX), k-1);%pack;
    end
end

% function [pr_prevec, pr_recvec, pr_retvec] = pr_evaluation3(DN, Dhamm, codelength)
%     pr_prevec.XX = [];pr_prevec.YY = [];
%     pr_prevec.XY = [];pr_prevec.YX = [];
%     pr_recvec.XX = [];pr_recvec.YY = [];
%     pr_recvec.XY = [];pr_recvec.YX = [];
%     pr_retvec.XY = [];pr_retvec.YX = [];
%     for k = 1:3%codelength+1
%         [pr_prevec.XX(end+1), pr_recvec.XX(end+1)] = evaluation_hammingradius(DN, Dhamm.XX, k-1, 'simple');
%         [pr_prevec.YY(end+1), pr_recvec.YY(end+1)] = evaluation_hammingradius(DN, Dhamm.YY, k-1, 'simple');
%         if k<=1 || pr_recvec.XY(end)<1
%         fprintf('current radius: %d\n', k);
%             [pr_prevec.XY(end+1), pr_recvec.XY(end+1), pr_retvec.XY(end+1)] = evaluation_hammingradius(DN.XY, Dhamm.XY, k-1, 'simple');%pack;
%         end
%         if k<=1 || pr_recvec.YX(end)<1
%             [pr_prevec.YX(end+1), pr_recvec.YX(end+1), pr_retvec.YX(end+1)] = evaluation_hammingradius(DN.YX, Dhamm.YX, k-1, 'simple');%pack;
%         end
%          
%     end
% end


function [Dhamm] = computeHdist(Bx_tr, By_tr, Bx_te, By_te)
%     Dhamm.XX = hammingDist2(Bx_te, Bx_tr);% image query - image database
%     Dhamm.YY = hammingDist2(By_te, By_tr);% text query  - text database
    Dhamm.XY = hammingDist2(Bx_te, By_tr);% image query  - text database
    Dhamm.YX = hammingDist2(By_te, Bx_tr);% text query - image database
end

function [Results] = copyresult(rank_premat, rank_recmat, rank_mapmat, radius_premat, radius_recmat)
    Results.rank_premat = rank_premat;
    Results.rank_recmat = rank_recmat;
    Results.rank_mapmat = rank_mapmat;
    Results.radius_premat = radius_premat;
    Results.radius_recmat = radius_recmat;
end           



% function [S] = compute_similarity(X,mode)
% % each column is a point
% switch mode
%     case 'euclidean'
%         X = perform_pca(X,40);
%         S = distMat(X);
%     case 'cosine'
%         S = squareform(pdist(X','cosine'));
%     otherwise
%         printf('similarity not supported.');
% end
% end

function [X] = perform_pca(X, npca)
    opts.disp = 0; 
    [pc, ~] = eigs(cov(X'), npca, 'LM', opts);
    X = pc'*X;
end

function  plotprcurve(pMat, rMat, titlestr)
    ps = mean(pMat,1); rs = mean(rMat,1);
    plot(rs,ps,'+-','LineWidth',2);set(gca, 'FontSize',20);
    xlabel('Recall');ylabel('Precision');title(titlestr);
end