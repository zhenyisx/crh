function myplot_train_curve(dataname, trsize, clength)

numtrain = length(trsize);

prevalues.XY = zeros(3, numtrain);
prevalues.YX = zeros(3, numtrain);
for i = 1:numtrain
    methodname = 'crh';
    load(sprintf('./results/res_%s_%s_%dtr_%db.mat',dataname, methodname, trsize(i), clength));
    prevalues.XY(1,i) = mean(Probresult{1}.XY(:,3),1);
    prevalues.YX(1,i) = mean(Probresult{1}.YX(:,3),1);
    methodname = 'cvh';
    load(sprintf('./results/res_%s_%s_%dtr_%db.mat',dataname, methodname, trsize(i), clength));
    prevalues.XY(2,i) = mean(Probresult{1}.XY(:,3),1);
    prevalues.YX(2,i) = mean(Probresult{1}.YX(:,3),1);
    methodname = 'bronstein';
    load(sprintf('./results/res_%s_%s_%dtr_%db.mat',dataname, methodname, trsize(i), clength));
    prevalues.XY(3,i) = mean(Probresult{1}.XY(:,3),1);
    prevalues.YX(3,i) = mean(Probresult{1}.YX(:,3),1);
    
end

figure; hold on;
plot(trsize,prevalues.XY(1,:), 'bo-', 'LineWidth',2,'MarkerSize',10);
plot(trsize,prevalues.XY(2,:), 'r*--', 'LineWidth',2,'MarkerSize',10);
plot(trsize,prevalues.XY(3,:), 'ks-.', 'LineWidth',2,'MarkerSize',10);
legend('CRH','CVH','CMSSH');set(gca, 'FontSize',20);%xlim([5,65]);
title('Image Query vs. Text Database');h = get(gca, 'title');set(h, 'FontSize', 20);
xlabel('Size of Training Set');h = get(gca, 'xlabel');set(h, 'FontSize', 20);
ylabel('Precision within Hamming Radius 2');h = get(gca, 'ylabel');set(h, 'FontSize', 20);

figure; hold on;
plot(trsize,prevalues.YX(1,:), 'bo-', 'LineWidth',2,'MarkerSize',10);
plot(trsize,prevalues.YX(2,:), 'r*--', 'LineWidth',2,'MarkerSize',10);
plot(trsize,prevalues.YX(3,:), 'ks-.', 'LineWidth',2,'MarkerSize',10);
legend('CRH','CVH','CMSSH');set(gca, 'FontSize',20);%xlim([5,65]);
title('Text Query vs. Image Database');h = get(gca, 'title');set(h, 'FontSize', 20);
xlabel('Size of Training Set');h = get(gca, 'xlabel');set(h, 'FontSize', 20);
ylabel('Precision within Hamming Radius 2');h = get(gca, 'ylabel');set(h, 'FontSize', 20);




