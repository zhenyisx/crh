% job script for experiments
% clear all;
% clc

function job_boost_wiki(methodname, trainsize, clength,repid)
%   Probparam contains parameters need for experiments, they are:
%       Probparam.datafile          : .mat data file
%       Probparam.neighborfile      : .mat neighborhood file
%       Probparam.indexfile         : .mat random index file
%       Probparam.evaltype          : 'rank' or 'radius' or 'both'
%       Probparam.hammingrank       : rank positions for evaluation
%       Probparam.hammingradius     : radii for evaluation
%       Probparam.codelength        : # of bits of binary codes
%       Probparam.repeats           : # of experiments, each experiments corresponds to a random landmark set
%       Probparam.lapregname        : 'none', 'label', 'feature', when Probparam.methodname = 'ksmh' or 'rksmh'
%       Probparam.methodname        : 'smhbase', 'ksmh' or 'rksmh'
%       Probparam.threshname        : 'zero','mean','median','quater-mean','quater-median','learn'
%       Probparam.landmarksize      : size of landmark set
%       Probparam.gammalapreg       : gamma for laplacian regularization
%       Probparam.expname           : 'normal','landmark','regularizer', varying different parameters
%       Probparam.subtrain          : use a subset for training, -1 indicate use all training set
nclass = 10;% although we have nclass here, but we do not use class labels.
% lmperClass = 30;
% obratio = 0.001;
% clength = 10;
% Setting problem parameters
% obratio = 0.001;
Probparam.datafile = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT%d',nclass);
Probparam.trainfile = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT%d_TR%d_rep%d',nclass,trainsize,repid);
Probparam.cvhfilex = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT%d_TR%d_%dbModel_x_rep%d.dat',nclass,trainsize,clength,repid);
Probparam.cvhfiley = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_TT%d_TR%d_%dbModel_y_rep%d.dat',nclass,trainsize,clength,repid);
% Probparam.neighborfile = '../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_NBM';
% Probparam.simfile = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_SIMEUC%d_RNNG',nclass);
% Probparam.rdidxfile = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_CTIDX%d_%d',nclass,lmperClass);% random index file
% Probparam.obfile = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_OB%d_%.4f.mat',nclass,obratio);% random index file
% Probparam.rdidxfile = sprintf('../../MH_TMM11/Python-1/HashExp/data/ucsd/UCSD_LMIDX%d_full',nclass);% random index file
% Probparam.evaltype = 'both';
Probparam.hammingrank = 10;% for evaluation
Probparam.hammingradius = 2;% for evaluation
Probparam.codelength = clength; %[2 10 20 30 40 50 60 70 80 90 100];%[24 48]
Probparam.repeats = 5; % repeats of landmark index
Probparam.methodname = methodname;  % 'bro-mlbe', 'smhbase', 'ksmh', 'rksmh','mmssh', 'brostein','spechash','mlbe','toy'
% Probparam.modelfilename = sprintf('../pegasos/models/nips_wiki_model_TR%d_64b_sgd1_sgd500_rep%d', trainsize,repid);% 'none', 'label', 'feature', when Probparam.methodname = 'ksmh' or 'rksmh'
if strcmp(methodname, 'crh')
    Probparam.modelfilename = sprintf('../pegasos/models/nips_wiki_model_TR%d_64b_sgd1_sgd500_g1000_rep%d', trainsize,repid);% 'none', 'label', 'feature', when Probparam.methodname = 'ksmh' or 'rksmh'
else strcmp(methodname, 'bNPP')
    Probparam.modelfilename = sprintf('../pegasos/models/nips_wiki_model_TR%d_64b_sgd0_sgd1_g100_rep%d', trainsize,repid);% 'none', 'label', 'feature', when Probparam.methodname = 'ksmh' or 'rksmh'
end
% Probparam.landmarksize = [20 40 60 80 100 500];%[50 100 200 300 400 500];%
% Probparam.gammalapreg = [0 1e-4 1e-3 1e-2 1e-1 1];
% Probparam.threshname = 'zero'; % 'zero','mean','median','quater-mean','quater-median','learn'
Probparam.expname = 'normal'; % 'normal','landmark','regularizer'
% Probparam.kernelname = 'linear'; % This is for the similarity metric adopted, 'linear','rbf'
% Probparam.subtrain = 200; % use a landmark set for training, -1 indicate use all training set
Probparam

tic
fprintf('Program starts running...');
[Probresult, hyper] = mh_boost_main(Probparam);
toc
% save the results
save(sprintf('./results/nips_res_wiki_%s_%dtr_%db_rep%d.mat', Probparam.methodname, trainsize, clength,repid),'Probresult','Probparam','hyper');

clear

fprintf('ends.\n');

