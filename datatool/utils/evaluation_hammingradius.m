function [mprecision, mrecall, mretrieved] = evaluation_hammingradius(Wtrue, Dhat, hammradius, mode)
%
% Input:
%    Wtrue : true neighbors [Ntest * Ntrain],
%    Dhat  : estimated distances
%    hammradius : upperbound (inclusive) of hamming distance, can be a
%                 vector of radii
%    mode  : (1) simple (2) average over test points
% Output:
%
%  mprecision = precision vector, each column is for one radius
%  mrecall = recall vector, each column is for one radius
%                          exp. # of total good pairs 

if size(Wtrue) == size(Dhat)
    Ntest = size(Wtrue,1);
    mprecision = zeros(1,length(hammradius));
    mrecall = zeros(1,length(hammradius));
    mretrieved = zeros(1,length(hammradius));

    % fprintf('\n');
    if strcmp(mode,'simple')
        total_good_pairs = sum(Wtrue(:));
%         size(Wtrue)
        for id = 1:length(hammradius)
%             tic
            j1 = find(Dhat(:)<((hammradius(id))+0.1));% find linear index of pairs with similar codes
            retrieved_good_pairs = sum(Wtrue(j1));% exp. # of good pairs retrieved 
            retrieved_all_pairs = length(j1);
%             toc
%             tic
%             retrieved_good_pairs2 = sum(Wtrue(Dhat(:)<((hammradius(id))+0.1)))% exp. # of good pairs retrieved 
%             retrieved_all_pairs2 = sum(Dhat(:)<((hammradius(id))+0.1))% exp. # of total pairs retrievevd
%             toc
%             pause
    %         fprintf('%d...',retrieved_all_pairs);
            if retrieved_all_pairs > eps
                mprecision(id) = retrieved_good_pairs/retrieved_all_pairs;
            else
                mprecision(id) = 0;
            end
            mrecall(id) = retrieved_good_pairs/total_good_pairs;
            mretrieved(id) = retrieved_all_pairs;
        end
    elseif strcmp(mode,'complex')
        Dhat = Dhat'; Wtrue = Wtrue';
        total_good_pairs = sum(Wtrue,1);% each column for each test query
        pmat = zeros(Ntest, length(hammradius));% for precision
        rmat = zeros(Ntest, length(hammradius));% for recall
        for id = 1:length(hammradius)
            for i = 1:Ntest
                if total_good_pairs(i) > eps 
                    ihammradius = hammradius(id);
                    jidx = find(Dhat(:,i)<=((ihammradius)+0.001));
    %                 while (length(jidx)<100)% at least retrieve 100 points
    %                     ihammradius = ihammradius + 1; jidx = find(Dhat(:,i)<=((ihammradius)+0.001));
    %                 end
                    retrieved_good_pairs = sum(Wtrue(jidx,i));
                    retrieved_all_pairs = length(jidx);         
                    if retrieved_all_pairs >eps
                        pmat(i,id) = double(retrieved_good_pairs)/double(retrieved_all_pairs);
                        rmat(i,id) = double(retrieved_good_pairs)/double(total_good_pairs(i));  
                    else
                        pmat(i,id) = 0;
                        rmat(i,id) = 0;
                    end                
                else% if no true neighbors for current query, precision,recall=1
                    pmat(i,id) = 1;
                    rmat(i,id) = 1;
                end
            end
        end
        mprecision = mean(pmat,1);
        mrecall = mean(rmat,1);        
    end
end
  




