//
//  evaluation_hammingradius2.c
//  
//
//  Created by Zhen Yi on 6/11/12.
//  Copyright 2012 HKUST. All rights reserved.
//

// #include <stdio.h>
#include <math.h>
#include "mex.h"

/* Input Arguments */

#define	Dhamm_IN	prhs[1]
#define	Dtrue_IN	prhs[0]
#define	r_IN	prhs[2]


/* Output Arguments */

#define	pre_OUT	plhs[0]
#define	rec_OUT	plhs[1]
#define	ret_OUT	plhs[2]

#if !defined(MAX)
#define	MAX(A, B)	((A) > (B) ? (A) : (B))
#endif

#if !defined(MIN)
#define	MIN(A, B)	((A) < (B) ? (A) : (B))
#endif

static	double	mu = 1/82.45;
static	double	mus = 1 - 1/82.45;


static void evaluation_hammingradius2(
                   double	*mpre,
                   double    *mrec,
                   double    *mret,
                   double	*W,
                   double    *D,
                   unsigned short	r,
                   int numel
                   )
{
    mwSize i,j,count=0;
    
    double retrieved_all_pairs = 0.0;
    double total_good_pairs = 0.0;
    double retrieved_good_pairs = 0.0;
    
    //mexPrintf("\nThe first element is %f.\n", (double)*(D+count));
    //mexPrintf("\nThe first element is %d.\n", *(D+count));
    //mexPrintf("\nThe first element is %d.\n", sizeof(unsigned short));
    //mexPrintf("\nThe first element is %d.\n", r);
    
    while (numel--) {
        double currentHamm = *D++;
        double currentLabel = *W++;
        // mexPrintf("\nThe element is %f\t%f.\n", currentHamm, currentLabel);
        // if (currentHamm > r) {
        //     mexPrintf("\nThe first element is %d\t%d.\n", currentHamm, r);
        // }
        ///*
        if (currentHamm <= r) {
            retrieved_all_pairs++;
            if (currentLabel > 0) {
                retrieved_good_pairs++ ;
            }
        }
        if (currentLabel > 0) {
            total_good_pairs++;
        }//*/
    }
    /*
    for (i=0; i<10; i++) {
        for (j=0; j<2; j++) {
           //
            unsigned short currentHamm = *(D+count);
            mexPrintf("\nThe first element is %d\t%d.\n", currentHamm, r);
            /*
            if (currentHamm > r) {
                mexPrintf("\nThe first element is %d\t%d.\n", currentHamm, r);
            }
            
            if (currentHamm <= r) {
                retrieved_all_pairs++;
                //if ((double)*(W+count) > 0) {
                //    retrieved_good_pairs++ ;
                //}
            }
            /*
            if ((double)*(W+count) > 0) {
                total_good_pairs++;
            }
            count++;
        }
    }//*/
    if (retrieved_all_pairs > 0.01)
        *mpre = retrieved_good_pairs/retrieved_all_pairs;
    else
        *mpre = 0.0;
    *mrec = retrieved_good_pairs/total_good_pairs;
    *mret = retrieved_all_pairs;
    return;
}

void mexFunction( int nlhs, mxArray *plhs[], 
                 int nrhs, const mxArray*prhs[] )

{ 
    double *Wmat, *Dmat;
    unsigned short r; 
    double *prevalue, *recvalue, *retvalue;
    int numel, numel2;
    //mwSize mrows, ncols; 
    
    /* Check for proper number of arguments */
    
    if (nrhs != 3) { 
        mexErrMsgTxt("Three input arguments required."); 
    } else if (nlhs != 3) {
        mexErrMsgTxt("Three output arguments required."); 
    } 
    
    /* Check the dimensions of Y.  Y can be 4 X 1 or 1 X 4. */ 
    
    numel = mxGetNumberOfElements(Dtrue_IN);
    // numel2 = mxGetNumberOfElements(Dhamm_IN);
    // mexPrintf("\nThe number of elements are %d and %d.\n", numel, numel2);
    //mrows = mxGetM(Dtrue_IN); 
    //ncols = mxGetN(Dtrue_IN);
    /*
    if (!mxIsDouble(Y_IN) || mxIsComplex(Y_IN) || 
        (MAX(m,n) != 4) || (MIN(m,n) != 1)) { 
        mexErrMsgTxt("YPRIME requires that Y be a 4 x 1 vector."); 
    } */
    
    /* Create a matrix for the return argument */ 
    // YP_OUT = mxCreateDoubleMatrix(m, n, mxREAL); 
    
    /* Assign pointers to the input parameters */ 
    
    Wmat = mxGetPr(Dtrue_IN); 
    Dmat = mxGetPr(Dhamm_IN); 
    r = mxGetScalar(r_IN);
    
    /* Assign pointers to the output parameters */ 
    
    pre_OUT = mxCreateDoubleMatrix(1,1,mxREAL);
    rec_OUT = mxCreateDoubleMatrix(1,1,mxREAL);
    ret_OUT = mxCreateDoubleMatrix(1,1,mxREAL);
    
    prevalue = mxGetPr(pre_OUT); 
    recvalue = mxGetPr(rec_OUT);
    retvalue = mxGetPr(ret_OUT);
    
    //*prevalue = 0; *recvalue = 0; *retvalue = 0;
    /* Do the actual computations in a subroutine */
    evaluation_hammingradius2(prevalue, recvalue, retvalue, Wmat, Dmat, r, numel); 
    return;
    
}


