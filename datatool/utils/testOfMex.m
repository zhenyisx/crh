function testOfMex

W = randn(24,1000);
data.Ntraining = 3000;
data.Ntest = 1000;
data.Xtraining = randn(1000,3000);
data.Xtest = randn(1000,3000);

B1 = W * [data.Xtraining] > 0;
B2 = W * [data.Xtest] > 0;

ndxtrain = 1:data.Ntraining;
ndxtest = data.Ntraining+1:data.Ntraining+data.Ntest;
code = [B1 B2];
code = compactbit(code);
code = uint8(code);

% P_code = zeros(numel(ndxtrain), numel(ndxtest));
% compute Hamming distance values
tic
D_code1 = hammingDist(code(:,ndxtest),code(:,ndxtrain));
toc
tic
D_code2 = hammingDist2(code(:,ndxtest),code(:,ndxtrain));		% a faster version of hammingDist
toc


if D_code1 == D_code2
    disp 'Equale'
else
    disp 'Different'
end
% for n = 1:length(ndxtest)
%   ndx = ndxtest(n);
%   
%   [foo, j_code] = sort(D_code(n, :), 'ascend'); % I assume that smaller distance means closer
%   j_code = ndxtrain(j_code);
%   
%   % get groundtruth sorting
%   D_truth = data.DtestTraining(ndx-data.Ntraining,:);
%   [foo, j_truth] = sort(D_truth);
%   j_truth = ndxtrain(j_truth);
%   
%   % evaluation
%   P_code(:,n) = neighbors_recall(j_truth, j_code);
% end
% 
% p_code = mean(P_code,2);
% ap = mean(p_code(1:data.max_care));
