/*
//  pr_evaluationc.c
//  
//
//  Created by Zhen Yi on 6/11/12.
//  Copyright 2012 HKUST. All rights reserved.
//
*/

#include <math.h>
#include "mex.h"

/* Input Arguments */

#define	Dhamm_IN	prhs[1]
#define	Dtrue_IN	prhs[0]
#define	r_IN	prhs[2]


/* Output Arguments */

#define	pre_OUT	plhs[0]
#define	rec_OUT	plhs[1]
#define	ret_OUT	plhs[2]

/*
#if !defined(MAX)
#define	MAX(A, B)	((A) > (B) ? (A) : (B))
#endif

#if !defined(MIN)
#define	MIN(A, B)	((A) < (B) ? (A) : (B))
#endif

static	double	mu = 1/82.45;
static	double	mus = 1 - 1/82.45;
*/

static void evaluationc(
                                      double	*mpre,
                                      double *mrec,
                                      double *mret,
                                      bool	*W,
                                      unsigned short *D,
                                      double	r,
                                      int numel
                                      )
{
    int rad = 0;
    
    double *retrieved_all_pairs;
    double total_good_pairs = 0;
    double *retrieved_good_pairs;
    
    retrieved_all_pairs = (double *) malloc(sizeof(double)*(r+1));
    retrieved_good_pairs = (double *) malloc(sizeof(double)*(r+1));
    
    /* initialize arrays */
    for (rad = 0; rad <= r; rad++) {
        retrieved_good_pairs[rad] = 0.0;
        retrieved_all_pairs[rad] = 0.0;
    }
    
    while (numel--) {
        double currentHamm = (double)*D++;
        double currentLabel = (double)*W++;
        /* mexPrintf("\nThe first element is %f\t%f.\n", currentHamm, currentLabel); */
        for (rad = 0; rad <= r; rad++) {
            if (currentHamm <= rad) {
                retrieved_all_pairs[rad]++;
                if (currentLabel > 0) {
                    retrieved_good_pairs[rad]++ ;
                }
            }
        }
        if (currentLabel > 0) {
            total_good_pairs++;
        }
    }
    /*
    for (rad = 0; rad <= r; rad++) {
        mexPrintf("\nretrieved pairs are %f for radius %d.\n", retrieved_all_pairs[rad], rad);
    }
    
     for (i=0; i<10; i++) {
     for (j=0; j<2; j++) {
     //
     unsigned short currentHamm = *(D+count);
     mexPrintf("\nThe first element is %d\t%d.\n", currentHamm, r);
     /*
     if (currentHamm > r) {
     mexPrintf("\nThe first element is %d\t%d.\n", currentHamm, r);
     }
     
     if (currentHamm <= r) {
     retrieved_all_pairs++;
     //if ((double)*(W+count) > 0) {
     //    retrieved_good_pairs++ ;
     //}
     }
     /*
     if ((double)*(W+count) > 0) {
     total_good_pairs++;
     }
     count++;
     }
     }//*/
    for (rad = 0; rad <= r; rad++) {
        if (retrieved_all_pairs[rad] > 0.01)
            *(mpre+rad) = retrieved_good_pairs[rad]/retrieved_all_pairs[rad];
        else
            *(mpre+rad) = 0.0;
        *(mrec+rad) = retrieved_good_pairs[rad]/total_good_pairs;
        *(mret+rad) = retrieved_all_pairs[rad];
    }
    return;
}

void mexFunction( int nlhs, mxArray *plhs[], 
                 int nrhs, const mxArray*prhs[] )

{ 
    bool *Wmat;
    unsigned short *Dmat;
    double r; 
    double *prevalue, *recvalue, *retvalue;
    int numel=0;
    
    /* Check for proper number of arguments */
    
    if (nrhs != 3) { 
        mexErrMsgTxt("Three input arguments required."); 
    } else if (nlhs != 3) {
        mexErrMsgTxt("Three output arguments required."); 
    } 
    
    /* Check the dimensions of Y.  Y can be 4 X 1 or 1 X 4. */ 
    
    numel = mxGetNumberOfElements(Dtrue_IN);
    /*
    // numel2 = mxGetNumberOfElements(Dhamm_IN);
    // mexPrintf("\nThe number of elements are %d and %d.\n", numel, numel2);
    //mrows = mxGetM(Dtrue_IN); 
    //ncols = mxGetN(Dtrue_IN);
    
     if (!mxIsDouble(Y_IN) || mxIsComplex(Y_IN) || 
     (MAX(m,n) != 4) || (MIN(m,n) != 1)) { 
     mexErrMsgTxt("YPRIME requires that Y be a 4 x 1 vector."); 
     } */
    
    /* Assign pointers to the input parameters */ 
    
    Wmat = mxGetLogicals(Dtrue_IN); 
    Dmat = mxGetData(Dhamm_IN); 
    r = mxGetScalar(r_IN);
    
    /* Assign pointers to the output parameters */ 
    
    pre_OUT = mxCreateDoubleMatrix(1,r+1,mxREAL);
    rec_OUT = mxCreateDoubleMatrix(1,r+1,mxREAL);
    ret_OUT = mxCreateDoubleMatrix(1,r+1,mxREAL);
    
    prevalue = mxGetPr(pre_OUT); 
    recvalue = mxGetPr(rec_OUT);
    retvalue = mxGetPr(ret_OUT);
    
    /* Do the actual computations in a subroutine */
    evaluationc(prevalue, recvalue, retvalue, Wmat, Dmat, r, numel); 
    return;
    
}


