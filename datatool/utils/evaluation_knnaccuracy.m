function [mprecision] = evaluation_knnaccuracy(Label_te, Label_tr, Dhat, hammrank)
%
% Input:
%    Label : labels vectors [length of label vector x Ntest]
%    Dhat  : estimated distances [Ntest x Ntrain]
%    hammrank : the upperbound number of retrived examples, can be a vector
%               of rank values
%
% Output:
%    mprecision: mean precision, each column is for one rank value
%    mrecall: mean recall, each column is for one rank value
%    map: mean average precision, each column is for one rank value

Ntest = size(Label_te, 2);
Dhat = Dhat'; %Label = Label';
% total_good_pairs = sum(Wtrue,1);% each column for each test query
pmat = zeros(1, length(hammrank));% for precision
% rmat = zeros(Ntest, length(hammrank));% for recall
% apmat = zeros(Ntest, length(hammrank));% for average precision (AP)

[Shammv Shammi] = sort(Dhat,1,'ascend'); 
clear Shammv;% Shammi contains the sorted index

% for j = 1:length(hammrank)
%     for i = 1:Ntest
%         iIDX = Shammi(1:hammrank(j), i);
%         retrieved_good_pairs = sum(Wtrue(iIDX, i));
%         pmat(i,j) = retrieved_good_pairs/hammrank(j);
%         rmat(i,j) = retrieved_good_pairs/total_good_pairs(i);
%         ps = [];
%         for r = 1:length(iIDX)
%             ps = [ps;sum(Wtrue(iIDX(1:r),i))/r];
%         end
%         apmat(i,j) = ps'*Wtrue(iIDX,i)/total_good_pairs(i);
%     end
% end

% improved version
for j = 1:length(hammrank)
    goodpoint = 0;
    for i = 1:Ntest
        iIDX = Shammi(1:hammrank(j), i);
        ilabel = sum(Label_tr(:,iIDX),2);
        [~, imax] = max(ilabel);
        plabel = zeros(size(ilabel)); plabel(imax) = 1;
        if plabel'*Label_te(:,i)>0
            goodpoint = goodpoint + 1;
        end
    end
    pmat(1,j) = goodpoint/Ntest;
end

%%%% This part is for illustration-----------------------------------%%
% [qval qidx] = sort(apmat(:,j),'descend');
% 
% for i = 1:10
%     fprintf('rank: %d\tquery id: %d\tMAP: %.4f\n',i, qidx(i), qval(i));
%     fprintf('Top retrieved id:');
%     for j = 1:8
%         fprintf('%d\t', Shammi(j,qidx(i)));
%     end
%     fprintf('\n');
% end
%%-------------------------------------------------------------------%%

mprecision = mean(pmat,1);
% mrecall = mean(rmat,1);
% map = mean(apmat,1);









