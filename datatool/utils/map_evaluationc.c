/*
//
//  map_evaluationc.c
//  
//
//  Created by Zhen Yi on 18/6/12.
//  Copyright 2012 HKUST. All rights reserved.
//
*/

#include <math.h>
#include "mex.h"

/* Input Arguments */

#define	Dhamm_IN	prhs[1]
#define	Dtrue_IN	prhs[0]
#define	r_IN	prhs[2]


/* Output Arguments */

#define	pre_OUT	plhs[0]
#define	rec_OUT	plhs[1]
#define	ret_OUT	plhs[2]

/*
 #if !defined(MAX)
 #define	MAX(A, B)	((A) > (B) ? (A) : (B))
 #endif
 
 #if !defined(MIN)
 #define	MIN(A, B)	((A) < (B) ? (A) : (B))
 #endif
 
 static	double	mu = 1/82.45;
 static	double	mus = 1 - 1/82.45;
 */

static void bubblesort (unsigned short *topKlabels, unsigned short *hammingvector, unsigned short *labelvector, int K, int totallength) {
    /* find top K labels with the smallest hamming distance */
    int i, j;
    unsigned short *tphammingvector = (unsigned short*) malloc(sizeof(unsigned short)*totallength);
    /* mexPrintf("\n number of elements: %d.\n", totallength); */
    for (i=0;i<totallength;i++) {
        *(tphammingvector+i) = *(hammingvector+i);
    }
    for (i=0; i<K; i++) {
        unsigned short iradius = 1000;
        int tpposision, iposition = -1;
        for (j=0; j<totallength;j++){
            if (*(tphammingvector+j)< iradius) {
                iposition = j;
                iradius = *(tphammingvector+iposition);
            }
        }        
        topKlabels[i] = *(labelvector+iposition);
        *(tphammingvector+iposition) = 1000;
    }

    return;
}

static double get_precision ( unsigned short	*labelvector, int labellength) {
    double iprecision = 0.0;
    int id=0;
    for (id = 0; id < labellength; id++) {
        if (*(labelvector+id)>0) {
            iprecision++;
        }
    }
    iprecision /= (double)labellength;
    return iprecision;
}

static double get_recall (unsigned short *returnedlabels, int rankposition, int allgoods) {
    double irecall = 0.0;
    int id=0;
    for (id = 0; id < rankposition; id++) {
        if (*(returnedlabels+id)>0) {
            irecall++;
        }
    }
    if (allgoods > 0) {
        irecall /= (double)allgoods;
    }
    else {
        irecall = 1;
    }
    
    return irecall;
}

static double get_ap( unsigned short	*labelvector, int labellength)
{
    /* labelvector is a vector of length rankposition */
    double ap = 0.0;
    double NumOfGoodpoint = 0.0;
    double CurrentPrecision = 0.0;
    int id=0;
    for (id = 0; id< labellength; id++) {
        if (*(labelvector+id)>0) {
            CurrentPrecision = (NumOfGoodpoint+1)/(id+1);
            ap = (ap*NumOfGoodpoint+CurrentPrecision)/(NumOfGoodpoint+1);
            NumOfGoodpoint++;
        }
    }
    return ap;
}

static void map_evaluationc(
                        double *mpre,
                        double *mrec,
                        double *map,
                        bool	*W,
                        unsigned short *D,
                        double	rankposition,
                        int mrows, int ncols
                        )
{
    int rad = 0, n=0, allgoods=0, numel, count = 0, trueneighbor=0, queryid=0;
    double totalprecision=0.0, totalrecall=0.0, totalap=0.0;
    numel = mrows*ncols;
    
    unsigned short *topKlabels = (unsigned short *) malloc(sizeof(unsigned short) * rankposition);
    unsigned short *ihammingvector = (unsigned short *) malloc(sizeof(unsigned short) * mrows);
    unsigned short *ilabelvector = (unsigned short *) malloc(sizeof(unsigned short) * mrows);
        
    double *precisions = (double *) malloc(sizeof(double)*(ncols));
    double *recalls = (double *) malloc(sizeof(double)*(ncols));
    double *aps = (double *) malloc(sizeof(double)*(ncols));
    
    /* initialize arrays */
    for (rad = 0; rad < ncols; rad++) {
        precisions[rad] = 0.0;
        recalls[rad] = 0.0;
        aps[rad] = 0.0;
    }

    for (rad=0; rad < mrows; rad++) {
        ihammingvector[rad] = 0;
        ilabelvector[rad] = 0;
    }
    
    for (rad = 0; rad < rankposition; rad++) {
        topKlabels[rad] = 0;
    }/**/
    
    /* Approach 1 */
    while (numel--) {
        unsigned short currentHamm = (unsigned short)*D++;
        unsigned short currentLabel = (unsigned short)*W++;
        
        if (count < mrows) {
            ihammingvector[count] = currentHamm;
            ilabelvector[count] = currentLabel;
            if (currentLabel>0) {
                trueneighbor++;
            }
            count++;
        } else {
            if (trueneighbor >0) {
                for (rad = 0; rad < rankposition; rad++) {
                    topKlabels[rad] = 0;
                }
                bubblesort(topKlabels, ihammingvector, ilabelvector, rankposition, mrows); 
                precisions[queryid] = get_precision(topKlabels, rankposition);
                recalls[queryid] = get_recall(topKlabels, rankposition, trueneighbor);
                aps[queryid] = get_ap(topKlabels, rankposition); 
                
            } else {
                precisions[queryid] = 1.0;
                recalls[queryid] = 1.0;
                aps[queryid] = 1.0; 
            }
            totalprecision += precisions[queryid];
            totalrecall += recalls[queryid];
            totalap += aps[queryid];
            count = 0;
            trueneighbor=0;
            queryid++;
        }
    }

    /*
    for (rad=0; rad<mrows*ncols; rad++) {
        bool ilabel = *(W+rad);
        mexPrintf("\n Dtrue(%d,%d):%d.\n", mrows, ncols, ilabel);
    }
    unsigned short ihammdis = *(D+mrows*(1000));
    bool ilabel = *(W+mrows*(10));
    mexPrintf("\n Dhamm(%d,%d):%d.\n", mrows, ncols, ihammdis);
    mexPrintf("\n Dtrue(%d,%d):%d.\n", mrows, ncols, ilabel);*/ 
    
      
    /* Approach 2
    for (n = 0; n<ncols; n++) {
        allgoods = 0;
        for (rad = 0; rad < rankposition; rad++) {
            topKlabels[rad] = 0;
        }
        
        for (rad = 0; rad < mrows; rad++) {
            if (*(W + n*mrows + rad)>0) {
                allgoods++;
            }
        }
        if (allgoods >0) {
            bubblesort(topKlabels, (D + n*mrows), (W + n*mrows), rankposition, mrows);
            
            precisions[n] = get_precision(topKlabels, rankposition);
            recalls[n] = get_recall(topKlabels, rankposition, allgoods);
            aps[n] = get_ap(topKlabels, rankposition); 
             
        } else {
            precisions[n] = 1.0;
            recalls[n] = 1.0;
            aps[n] = 1.0; 
        }
         
        totalprecision += precisions[n];
        totalrecall += recalls[n];
        totalap += aps[n];
    }  */
    
    *mpre = totalprecision/(double)ncols;
    *mrec = totalrecall/(double)ncols;
    *map = totalap/(double)ncols;
    return;
}


void mexFunction( int nlhs, mxArray *plhs[], 
                 int nrhs, const mxArray*prhs[] )

{ 
    bool *Wmat;
    unsigned short *Dmat;
    double r; 
    double *prevalue, *recvalue, *mapvalue;
    int mrows, ncols;
    
    /* Check for proper number of arguments */
    
    if (nrhs != 3) { 
        mexErrMsgTxt("Three input arguments required."); 
    } else if (nlhs != 3) {
        mexErrMsgTxt("Three output arguments required."); 
    } 
    
    /* Each column is for a test query 
    
    numel = mxGetNumberOfElements(Dtrue_IN);*/ 
    mrows = mxGetM(Dtrue_IN); 
    ncols = mxGetN(Dtrue_IN);
    
    mexPrintf("\n rows: %d\tcols: %d.\n", mxGetM(Dtrue_IN), mxGetN(Dtrue_IN));
    mexPrintf("\n rows: %d\tcols: %d.\n", mxGetM(Dhamm_IN), mxGetN(Dhamm_IN));
    /*mexPrintf("\nThe number of elements are %d and %d.\n", mxGetNumberOfElements(Dtrue_IN), mxGetNumberOfElements(Dhamm_IN));
    
     // numel2 = mxGetNumberOfElements(Dhamm_IN);
     // mexPrintf("\nThe number of elements are %d and %d.\n", numel, numel2);
     //
     //     
     if (!mxIsDouble(Y_IN) || mxIsComplex(Y_IN) || 
     (MAX(m,n) != 4) || (MIN(m,n) != 1)) { 
     mexErrMsgTxt("YPRIME requires that Y be a 4 x 1 vector."); 
     } */
    
    /* Assign pointers to the input parameters */ 
    
    Wmat = mxGetLogicals(Dtrue_IN); 
    Dmat = mxGetData(Dhamm_IN); 
    r = mxGetScalar(r_IN);
    
    /* Assign pointers to the output parameters */ 
    
    pre_OUT = mxCreateDoubleMatrix(1,1,mxREAL);
    rec_OUT = mxCreateDoubleMatrix(1,1,mxREAL);
    ret_OUT = mxCreateDoubleMatrix(1,1,mxREAL);
    
    prevalue = mxGetPr(pre_OUT); 
    recvalue = mxGetPr(rec_OUT);
    mapvalue = mxGetPr(ret_OUT);
    
    /* Do the actual computations in a subroutine */
    map_evaluationc(prevalue, recvalue, mapvalue, Wmat, Dmat, r, mrows, ncols); 
    return;
    
}


