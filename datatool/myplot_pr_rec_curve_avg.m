function myplot_pr_rec_curve_avg(dataname, trsize, clength)


methodname = 'crh';
premat1.XY = [];premat1.YX = [];
premat2.XY = [];premat2.YX = [];
premat3.XY = [];premat3.YX = [];

recmat1.XY = [];recmat1.YX = [];
recmat2.XY = [];recmat2.YX = [];
recmat3.XY = [];recmat3.YX = [];

retmat1.XY = [];retmat1.YX = [];
retmat2.XY = [];retmat2.YX = [];
retmat3.XY = [];retmat3.YX = [];


for repid = 1:5
    load(sprintf('./results/nips_res_%s_%s_%dtr_%db_rep%d.mat',...
        dataname, methodname, trsize,clength, repid));

    premat1.XY = [premat1.XY;Probresult{1}.XY];
    premat1.YX = [premat1.YX;Probresult{1}.YX];
    recmat1.XY = [recmat1.XY;Probresult{2}.XY];
    recmat1.YX = [recmat1.YX;Probresult{2}.YX];
    retmat1.XY = [retmat1.XY;Probresult{3}.XY];
    retmat1.YX = [retmat1.YX;Probresult{3}.YX];

    methodname = 'cvh';
    load(sprintf('./results/nips_res_%s_%s_%dtr_%db_rep%d.mat',...
        dataname, methodname, trsize,clength, repid));

    premat2.XY = [premat2.XY;Probresult{1}.XY];
    premat2.YX = [premat2.YX;Probresult{1}.YX];
    recmat2.XY = [recmat2.XY;Probresult{2}.XY];
    recmat2.YX = [recmat2.YX;Probresult{2}.YX];
    retmat2.XY = [retmat2.XY;Probresult{3}.XY];
    retmat2.YX = [retmat2.YX;Probresult{3}.YX];


    methodname = 'bronstein';
    load(sprintf('./results/nips_res_%s_%s_%dtr_%db_rep%d.mat',...
        dataname, methodname, trsize,clength, repid));

    premat3.XY = [premat3.XY;Probresult{1}.XY];
    premat3.YX = [premat3.YX;Probresult{1}.YX];
    recmat3.XY = [recmat3.XY;Probresult{2}.XY];
    recmat3.YX = [recmat3.YX;Probresult{2}.YX];
    retmat3.XY = [retmat3.XY;Probresult{3}.XY];
    retmat3.YX = [retmat3.YX;Probresult{3}.YX];

end

premat1.XY = mean(premat1.XY,1);premat1.YX = mean(premat1.YX,1);
premat2.XY = mean(premat2.XY,1);premat2.YX = mean(premat2.YX,1);
premat3.XY = mean(premat3.XY,1);premat3.YX = mean(premat3.YX,1);
recmat1.XY = mean(recmat1.XY,1);recmat1.YX = mean(recmat1.YX,1);
recmat2.XY = mean(recmat2.XY,1);recmat2.YX = mean(recmat2.YX,1);
recmat3.XY = mean(recmat3.XY,1);recmat3.YX = mean(recmat3.YX,1);
retmat1.XY = mean(retmat1.XY,1);retmat1.YX = mean(retmat1.YX,1);
retmat2.XY = mean(retmat2.XY,1);retmat2.YX = mean(retmat2.YX,1);
retmat3.XY = mean(retmat3.XY,1);retmat3.YX = mean(retmat3.YX,1);

myplot_prcurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3);
myplot_reccurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3);

