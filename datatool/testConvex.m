function testConvex

w = 10;
x = -100:0.1:100;
y = 1-abs(w*x);

if y < 0
    y = 0;
end

plot(x,y,'k-');