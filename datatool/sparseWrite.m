function sparseWrite(matrix, filename)

[i,j,val] = find(matrix);
save_data = [i,j,val];
fid = fopen(filename,'w');
fprintf(fid,'%d %d %.4f\n',save_data');
fclose(fid);