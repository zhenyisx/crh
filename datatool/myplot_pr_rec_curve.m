function myplot_pr_rec_curve(dataname, trsize, clength)


methodname = 'crh';
load(sprintf('./results/res_%s_%s_%dtr_%db.mat',...
    dataname, methodname, trsize,clength));

premat1 = Probresult{1};
recmat1 = Probresult{2};
retmat1 = Probresult{3};

methodname = 'cvh';
load(sprintf('./results/res_%s_%s_%dtr_%db.mat',...
    dataname, methodname, trsize,clength));

premat2 = Probresult{1};
recmat2 = Probresult{2};
retmat2 = Probresult{3};


methodname = 'bronstein';
load(sprintf('./results/res_%s_%s_%dtr_%db.mat',...
    dataname, methodname, trsize,clength));

premat3 = Probresult{1};
recmat3 = Probresult{2};
retmat3 = Probresult{3};

% rank_premat = Probresult{4};
% rank_recmat = Probresult{5};
% rank_mapmat = Probresult{6};

% mean(rank_mapmat.XY,1)
% mean(rank_mapmat.YX,1)

myplot_prcurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3);
myplot_reccurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3);

